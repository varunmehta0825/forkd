@component('mail::message')
<h2>Welcome to the site {{$user['name']}}</h2>
<br/>
Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account
<br/>
<a href="{{url('user/confirm', $user->verifyUser->token)}}">Verify Email</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
