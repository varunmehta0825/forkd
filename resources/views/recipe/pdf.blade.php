<!DOCTYPE html>
<html>
    <head>
        <title>Recipe</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .clearfix::after {
                content: "";
                clear: both;
                display: table;
            }
            body {
                font-family: 'Roboto', sans-serif;
                color: rgba(0, 0, 0, 0.87);
            }

            header {
                text-align: center;
            }

            header b {
                  margin: 10px;
            }

            img {
                border-width: 0;
                box-shadow: 0;
            }

            .row {
                page-break-after: always;
            }

            .recipe__lifestyle {
                margin: 0;
                padding: 0;
            }

            .recipe__lifestyle ul li,
            .recipe__lifestyle ul {
                padding: 0;
                margin: 0;
            }

            .recipe__lifestyle li {
                display: inline-block;
                height: 20px;
                margin-right: 8px;
                overflow: hidden;
                width: 20px;
            }

            .recipe h3 {
                font-size: 21px;
                letter-spacing: .66px;
                line-height: 24px;
                margin: 5px 0;
            }

            .recipe__servings {
                margin-bottom: 20px;
            }

            .recipe__servings,
            .recipe__cooking-time {
                font-size: 13px;
            }

            span {
                display: inline-block;
                vertical-align: middle;
            }

            .recipe__servings ul {
                display: inline-block;
                margin-left: 0;
                margin-top: 4px;
                vertical-align: middle;
            }

            .recipe__servings ul li {
                padding: 0;
                display: inline-block;
                height: 22px;
                text-align: center;
                vertical-align: middle;
                width: 22px;
                background-size: 100%;
            }

            .recipe__servings ul li i {
                color: #99A1B1;
                line-height: 1.8;
                font-style: normal;
                display: block;
                height: 22px;
                text-align: center;
                vertical-align: middle;
                width: 22px;
            }

            .recipe__servings ul li.active i {
                background: url('{{url('/')}}/images/oval.png') no-repeat center;
                color: #fff;
            }

            h5 {
                text-transform: uppercase;
                font-size: 16px;
            }

            .recipe__ingredients > div {
                display: inline-block;
                vertical-align: top;
                width: 50%;
            }

            .recipe__ingredients > div:nth-child(2) {
                margin-left: -10px;
            }

            .recipe__ingredients ul {
                list-style: none;
                margin: 0;
                padding: 0;
            }

            .recipe__ingredients ul li {
                margin-bottom: 10px;
                height: 20px;
            }

            .recipe__cooking-time > div {
                display: inline-block;
                margin-right: 20px;
                vertical-align: top;
            }

            .recipe__cooking-time > div img {
                margin-right: 10px;
            }

            .recipe__cooking-time > div span,
            .recipe__cooking-time > div strong {
                display: inline-block;
                vertical-align: middle;
            }

            .recipe__ingredients ul {
                margin: 0;
            }

            .recipe__preparation {
                margin-top: 0;
                white-space: pre-line;
                line-height: 1.8;
            }

            .recipe__ingredients i {
                border-radius: 50%;
                margin-right: 0;
                display: inline-block;
                height: 15px;
                padding: 0 7.5px;
                margin-top: 1px;
                width: 15px;
            }

            .recipe__ingredients--red {
                background-color: #D91917;
            }

            .recipe__ingredients--green {
                background-color: #00B659;
            }

            .recipe__ingredients--purple {
                background-color: #8233CA;
            }

            .recipe__ingredients--yellow {
                background-color: #FFD300;
            }

            .recipe__ingredients--blue {
                background-color: #0080E8;
            }

            .recipe__ingredients--orange {
                background-color: #F38903;
            }

            .recipe__ingredients--grey {
                background-color: #808080;
            }
        </style>
    </head>
    <body>
        @if(!empty($plan))
            @foreach($plan['daily_plan'][0]['meals_plan'] as $recipe)
            <header class="clearfix">
                <img src="{{url('/')}}/images/logo.png" width="192"/>
                <br/>
                <br/>
                <b>{{ $user->name }}</b>
                <br/>
                <p>Week: {{ date('d M.', strtotime($plan['from'])) .' - '. date('d M. Y', strtotime($plan['to'])) }}</p>
            </header>
            <div class="row recipe">
                <div class="col-right">
                    <div class="recipe__content">
                        @if($recipe['recipes']['dietary_lifestyles'])
                        <ul class="recipe__lifestyle clearfix">
                            @foreach($recipe['recipes']['dietary_lifestyles'] as $lifestyle)
                            <li>
                                <img src="{{url('/')}}/images/{{strtolower(str_replace(' ', '-', $lifestyle['name'])) }}.png" height="20"/>
                            </li>
                            @endforeach
                        </ul>
                        @endif

                        <h3>
                            {{ $recipe['recipes']['name'] }}
                        </h3>

                        <div class="recipe__servings clearfix">
                            <span>Servings: </span>
                            <ul class="clearfix">
                                @for($i = 1; $i < 11; ++$i)
                                <li @if($i == $recipe['recipes']['servings']) class='active' @endif>
                                    <i>{{ $i }}</i>
                                </li>
                                @endfor
                            </ul>
                        </div><!-- //.servings -->

                        <div class="recipe__cooking-time clearfix">
                            <div>
                                <img src="{{url('/')}}/images/prep.png" height="15"/>
                                <span>Prep.</span>
                                <strong>{{ $recipe['recipes']['display_prep_time'] }}</strong>
                            </div>
                            <div>
                                <img src="{{url('/')}}/images/prep.png" height="15"/>
                                <span>Cook.</span>
                                <strong>{{ $recipe['recipes']['display_cook_time'] }}</strong>
                            </div>
                        </div><!-- //.cooking-time -->
                        @if(isset($recipe['recipes']['recipe_ingredients']))
                        <div class="recipe__ingredients clearfix">
                            <div>
                                <h5>Ingredients</h5>
                                <ul class="clearfix">
                                @foreach($recipe['recipes']['recipe_ingredients'] as $ingredient)
                                    <li>
                                        <span>{{ $ingredient['qty'] }}</span>
                                        <span>{{ $ingredient['measurement']['name'] }}</span>
                                        <span>{{ $ingredient['ingredients']['name'] }}</span>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                            <div>
                                <h5>Container Count</h5>
                                <ul class="clearfix">
                                    @foreach($recipe['recipes']['recipe_ingredients'] as $ingredient)
                                    <li>
                                        <div>
                                            @if($ingredient['ingredients']['red_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--red"></i>
                                                <span>{{ $ingredient['ingredients']['red_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['green_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--green"></i>
                                                <span>{{ $ingredient['ingredients']['green_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['purple_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--purple"></i>
                                                <span>{{ $ingredient['ingredients']['purple_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['yellow_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--yellow"></i>
                                                <span>{{ $ingredient['ingredients']['yellow_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['blue_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--blue"></i>
                                                <span>{{ $ingredient['ingredients']['blue_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['orange_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--orange"></i>
                                                <span>{{ $ingredient['ingredients']['orange_containers'] }}</span>
                                            </span>
                                            @endif
                                            @if($ingredient['ingredients']['spoon_containers'] != 0)
                                            <span>
                                                <i class="recipe__ingredients--grey"></i>
                                                <span>{{ $ingredient['ingredients']['spoon_containers'] }}</span>
                                            </span>
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div><!-- //.recipe__imgredients -->
                        @endif

                        @if(isset($recipe['recipes']['preparation']))
                        <div class="recipe__preparation clearfix">
                            <h5>Recipe</h5>
                            {{ $recipe['recipes']['preparation'] }}
                        </div><!-- //.recipe__preparation -->
                        @endif

                    </div><!-- //.recipe__content -->
                </div><!-- //.col-right -->
            </div><!-- //.row -->
            @endforeach
        @endif
    </body>
</html>
