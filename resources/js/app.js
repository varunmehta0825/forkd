
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import App from "./App.vue"
import axios from 'axios'
import router from  './routes'
import store from  './store'
import Vuetify from 'vuetify'
import VueSweetalert2 from 'vue-sweetalert2'
import VueMouseParallax from 'vue-mouse-parallax'
import VueResource from 'vue-resource'
import VueSVGCustomIcon from 'vue-svg-custom-icon'

import { MdButton, MdContent, MdCheckbox, MdRadio, MdField, MdMenu, MdList, MdDialog, MdCard} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdCheckbox)
Vue.use(MdRadio)
Vue.use(MdMenu)
Vue.use(MdField)
Vue.use(MdList)
Vue.use(MdDialog)
Vue.use(MdCard)

Vue.use(VueSweetalert2);
Vue.use(VueMouseParallax)
Vue.use(VueResource)
Vue.use(VueSVGCustomIcon, { basePath: '../svg' })

Vue.use(Vuetify, {
    theme: {
        primary: '#C7DAD1',
        secondary: '#FFFFFF',
        accent: '#3ECFFE',
        // error: '#b71c1c'
    }
})

Vue.config.devtools = true

// axios.defaults.baseURL = 'http://forkd.varunme.com/api/'
axios.defaults.baseURL = 'http://forkd.codecraftsolutions.com/api/'
// axios.defaults.baseURL = 'http://forkd.loc/api/'
axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.auth.token

axios.interceptors.response.use(undefined, function(err) {
    return new Promise(function (resolve, reject) {
        if(err.response.status === 401 && err.response.statusText === 'Unauthorized') {
            store.dispatch('logout')
        }

        throw err
    })
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");

// window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app.constructor;
