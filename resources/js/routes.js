import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from './components/pages/HomePage'
import UserRegister from './components/pages/UserRegister'
import ResetPassword from './components/pages/ResetPassword'
import Billing from './components/pages/Billing'
import Step2 from './components/pages/Step2'
import Step3 from './components/pages/Step3'
import Step4 from './components/pages/Step4'
import Step5 from './components/pages/Step5'
import Confirmation from './components/pages/Confirmation'
import CreatePlan from './components/pages/CreatePlan'
import MealPlan from './components/pages/MealPlan'
import PlanNextWeek from './components/pages/PlanNextWeek'
import ResendConfirmation from './components/pages/ResendConfirmation'
import CustomizeShake from './components/pages/CustomizeShake'
import ShoppingList from './components/pages/ShoppingList'
import MyAccount from './components/pages/MyAccount'
import multiguard from 'vue-router-multiguard';
import store from './store/index'

Vue.use(VueRouter)

const getUser = function(to, from, next) {
    store.dispatch('getUser').then((response) => {
        if(response.data.status == 'success') {
            next()
        }
    })
}

const isConfirmed = function(to, from, next) {
    if(store.getters.getUser.length < 1) {
        store.dispatch('getUser').then((response) => {
            if(response.data.status == 'success') {
                let user = response.data.data
                // if(user.verify_request && !user.confirmed) {
                //     router.push('/resend-confirmation')
                // } else if(!user.subscribed) {
                //     router.push('/billing')
                // } else

                if(user.passed_steps != 5) {
                    router.push('/step/' + user.passed_steps)
                } else if(user.confirmed) { //subscribed
                    next()
                }
            }
        })
    } else {
        let user = store.getters.getUser
        //
        // if(user.verify_request && !user.confirmed) {
        //     router.push('/resend-confirmation')
        // } else if(!user.subscribed) {
        //     router.push('/billing')
        // } else

        if(user.passed_steps != 5) {
            router.push('/step/' + user.passed_steps)
        } else if(user.confirmed) { //subscribed
            next()
        }
    }
}

const customizeSteps = function(to, from, next) {
    if(store.getters.getSteps.length < 1) {
        store.dispatch('getSteps').then((response) => {
            if(response.data.status == 'success') {
                let steps = response.data.data
                let week = ''

                if(router.currentRoute.params.week)
                    week = router.currentRoute.params.week

                if(steps.length > 0) {
                    router.push('/customize/' + steps[0] + week)
                    store.dispatch('getCurrent')
                } else {
                    router.push('/create-plan')
                }
            }
        })
    }
    else {
        store.dispatch('getNext').then((response) => {
            next();
        })
    }
}

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomePage, //require('./components/pages/HomePage')
            name: 'home',
        },
        {
            path: '/register',
            component: UserRegister, //require('./components/pages/UserRegister'),
            name: 'register',
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/reset/password/:token',
            component: ResetPassword, //require('./components/pages/ResetPassword'),
            name: 'reset-password',
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/billing',
            component: Billing, //require('./components/pages/Billing'),
            name: 'billing',
            meta: {
                forAuth: true
            }
        },
        {
            path: '/step/2',
            component: Step2, //require('./components/pages/CalorieTarget'),
            name: 'step2',
            // beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/step/3',
            component: Step3, //require('./components/pages/CalorieTarget'),
            name: 'step3',
            // beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/step/4',
            component: Step4, //require('./components/pages/CalorieTarget'),
            name: 'step4',
            // beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/step/5',
            component: Step5, //require('./components/pages/CalorieTarget'),
            name: 'step5',
            // beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/user/confirm/:token',
            component: Confirmation, //require('./components/pages/Confirmation'),
            name: 'confirmation',
            meta: {
                forAuth: true
            }
        },
        {
            path: '/create-plan',
            component: CreatePlan, //require('./components/pages/CreatePlan'),
            name: 'create-plan',
            beforeEnter: multiguard([getUser]),
            meta: {
                forAuth: true
            },
        },
        {
            path: '/meal-plan/show/:week?',
            component: MealPlan, //require('./components/pages/MealPlan'),
            name: 'meal-plan',
            beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/meal-plan/next-week',
            component: PlanNextWeek, //require('./components/pages/PlanNextWeek'),
            name: 'next-week',
            // beforeEnter: multiguard([isConfirmed]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/resend-confirmation',
            component: ResendConfirmation, //require('./components/pages/PlanNextWeek'),
            name: 'resend-confirmation',
            meta: {
                forAuth: true
            }
        },
        {
            path: '/customize/shake/:week?',
            component: CustomizeShake, //require('./components/pages/PlanNextWeek'),
            name: 'customize-shake',
            beforeEnter: multiguard([customizeSteps]),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/shopping-list',
            component: ShoppingList, //require('./components/pages/ShoppingList'),
            name: 'shopping-list',
            meta: {
                forAuth: true
            }
        },
        {
            path: '/my-account',
            component: MyAccount, //require('./components/pages/ShoppingList'),
            name: 'my-account',
            meta: {
                forAuth: true
            }
        }
    ]
})

router.beforeEach(
    (to, from, next) => {
        if(to.matched.some(record => record.meta.forVisitors)) {
            if(store.getters.isAuthenticated) {
                next({
                    path: '/'
                })
            } else next()
        }
        else if(to.matched.some(record => record.meta.forAuth)) {
            if(!store.getters.isAuthenticated) {
                next({
                    path: '/login'
                })
            } else next()
        } else next()
    }
)

export default router
