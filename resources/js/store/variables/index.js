export const ranges = {
    set1From: 1200,
    set1To:   1499,
    set2From: 1500,
    set2To:   1799,
    set3From: 1800,
    set3To:   2099,
    set4From: 2100,
    set4To:   2299
}
