import router from '../../routes'
import axios from 'axios'

export default {
    state: {
        user: [],
        token: localStorage.getItem('token') || null,
        status: '',
    },
    getters: {
        getUser: state => state.user,
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
    },
    mutations: {
        GET_USER: (state, user) => {
            state.user = user
        },
        AUTH_SUCCESS: (state, token) => {
            state.token = token
            state.status = 'success'
        },
        AUTH_ERRROR: (state, response) => {
            state.token = ''
            state.status = 'error'
        },
        AUTH_LOGOUT: (state) => {
            state.token = ''
            state.user = []
            axios.defaults.headers.common['Authorization'] = ''
        }

    },
    actions: {
        auth(context, payload) {
            let action = payload.action
            delete payload.action;

            return new Promise((resolve, reject) => {
                axios.post(action, payload)
                    .then(response => {
                        let accessToken = response.data.data.access_token
                        let user = response.data.data.user
                        let status = response.data.status

                        context.commit('AUTH_SUCCESS', accessToken, status)
                        context.commit('GET_USER', user)
                        localStorage.setItem('token', accessToken)
                        axios.defaults.headers.common['Authorization'] = "Bearer " + accessToken

                        resolve(response)
                    })
                    .catch(({response}) => {
                        localStorage.removeItem('token')
                        context.commit('AUTH_ERRROR', response)

                        reject(response)
                    })
            })
        },

        getUser(context) {
            if(context.getters.isAuthenticated) {
                return new Promise((resolve, reject) => {
                    axios.get('user')
                        .then(response => {
                            let user = response.data.data
                            context.commit('GET_USER', user)

                            resolve(response)
                        })
                        .catch(({response}) => {
                            console.log(response)
                            reject(response)
                        })
                })
            }
        },

        logout(context) {
            return new Promise((resolve, reject) => {
                context.commit('AUTH_LOGOUT')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                router.push('/')

                resolve()
            })
        }
    }
};
