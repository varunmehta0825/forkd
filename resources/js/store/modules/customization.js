import router from  '../../routes'

export default {
    state: {
        current: 0,
        steps: []
    },
    getters: {
        getSteps: state => state.steps,
        getCurrent: state => state.current,
    },
    mutations: {
        GET_STEPS: (state, steps) => {
            state.steps = steps
        },
        GET_CURRENT: (state, current) => {
            state.current += current
        }
    },
    actions: {
        addSteps(context, payload) {
            return new Promise((resolve, reject) => {
                context.commit('GET_STEPS', payload)

                resolve(context.getters.getSteps)
            })
        },
        getSteps(context, payload) {
            return new Promise((resolve, reject) => {
                axios.get('get-steps')
                    .then(response => {
                        let steps = response.data.data

                        context.commit('GET_STEPS', steps)
                        resolve(response)
                        // console.log(context.getters.getSteps)
                    })
                    .catch(({response}) => {
                        console.log(response)

                        reject(response)
                    })
            })
        },
        getCurrent(context, payload) {
            return new Promise((resolve, reject) => {
                context.commit('GET_CURRENT', 0)

                resolve(context.getters.getCurrent)
            })
        },
        getNext(context, payload) {
            return new Promise((resolve, reject) => {
                context.commit('GET_CURRENT', 1)

                resolve(context.getters.getCurrent)
            })
        },
        getNextStep(context, payload) {
            return new Promise((resolve, reject) => {
                axios.post("/customize/shake", payload)
                    .then(response => {
                        let status = response.data.status
                        let data = response.data.data

                        let week = ''

                        if(router.currentRoute.params.week)
                            week = router.currentRoute.params.week

                        // router.push('/customize/' + this.steps[this.currentStep])
                        if(status == 'success') {
                            context.dispatch('createMealPlan', week)
                            context.commit('GET_CURRENT', -context.getters.getCurrent)
                        }

                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error.response)
                        // self.status = error.response.data.status
                        // self.message = error.response.data.errors.message
                        // self.submitted = false
                        reject(response)
                    })
            })


        }
    }
}
