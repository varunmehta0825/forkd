import router from  '../../routes'

export default {
    state: {
        recipes: []
    },
    getters: {
        getRecipe: state => state.recipes,
    },
    mutations: {
        GET_RECIPE: (state, recipe) => {
            state.recipes.push(recipe)
        }
    },
    actions: {
        getRecipe(context, payload) {
            return new Promise((resolve, reject) => {
                axios.get('recipe/'+payload.action)
                    .then(response => {
                        let recipe = response.data.data
                        context.commit('GET_RECIPE', recipe)
                    })
                    .catch(({response}) => {
                        console.log(response)

                        reject(response)
                    })
            })
        },
        createMealPlan(context, payload) {
            return new Promise((resolve, reject) => {
                Vue.swal({
                    title: 'Wait a bit...',
                    text: 'We\'re looking for the best meal plan.',
                    padding: '20px',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                })
                var url = '';

                if(payload.length > 0)
                    url = "/meal-plan/store/" + payload
                else
                    url = "/meal-plan/store"

                axios.post(url)
                    .then(response => {
                        // let status = response.data.status
                        // let message = response.data.data.message
                        Vue.swal.close();

                        router.push('/meal-plan/show/' + payload)

                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)

                        // self.status = error.response.data.status
                        // self.message = error.response.data.errors.message
                        // self.submitted = false

                        Vue.swal.close();

                        if(error.response.status == 500)
                            Vue.swal({
                                type: 'error',
                                title: 'Can\'t create meal plan.',
                                text: 'Please change your plan restrictions and try again.',
                                padding: '20px'
                            })

                        reject(error)
                    })
            })
        }
    }
}
