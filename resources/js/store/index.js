import Vue from "vue"
import Vuex from "vuex"
import auth from "./modules/auth"
import recipe from "./modules/recipe"
import customization from "./modules/customization"


Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        recipe,
        customization
    }
})
