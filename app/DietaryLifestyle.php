<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DietaryLifestyle extends Model
{
    protected $fillable = [
        'dietary_lifestyle_id'
    ];

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'recipe_dietary_lifestyle', 'dietary_lifestyle_id', 'recipe_id');
    }

    public function user()
    {
        return $this->belongsToMany('App\User', 'user_lifestyles', 'dietary_lifestyle_id', 'user_id');
    }
}
