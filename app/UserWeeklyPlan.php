<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWeeklyPlan extends Model
{
    protected $table = 'user_weekly_plan';

    protected $fillable = [
        'user_id',
        'from',
        'to',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function daily_plan()
    {
        return $this->hasMany('App\UserDailyPlan', 'user_weekly_plan_id');
    }
}
