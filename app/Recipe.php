<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{

    public function meal_types()
    {
        return $this->belongsToMany('App\MealType', 'recipe_meal_type', 'recipe_id', 'meal_type_id');
    }

    public function dietary_lifestyles()
    {
        return $this->belongsToMany('App\DietaryLifestyle', 'recipe_dietary_lifestyle', 'recipe_id', 'dietary_lifestyle_id');
    }

    public function recipe_types()
    {
        return $this->belongsToMany('App\RecipeType', 'recipe_recipe_type', 'recipe_id', 'recipe_type_id');
    }

    public function cuisine_types()
    {
        return $this->belongsToMany('App\CuisineType', 'recipe_cuisine_type', 'recipe_id', 'cuisine_type_id');
    }

    public function related_recipes()
    {
        return $this->belongsToMany('App\Recipe', 'related_recipes', 'recipe_id', 'related_recipe_id');
    }

    public function recipe_ingredients()
    {
        return $this->hasMany('App\RecipeIngredient', 'recipe_id');
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient', 'recipe_ingredients', 'recipe_id', 'ingredient_id');
    }

    public function meal_plan()
    {
        return $this->hasMany('App\UserMealPlan', 'recipe_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'excluded_recipes', 'recipe_id', 'user_id');
    }

}
