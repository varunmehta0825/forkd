<?php

namespace App;

use Storage;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable, SoftDeletes, HasApiTokens, Notifiable;

    protected $appends = ['avatar_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'age',
        'gender',
        'nutrition_lifestyle',
        'nutrition_goal',
        'avatar',
        'active',
        'password',
        'confirmed',
    ];


    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'last_active'
    ];

    public function getAvatarUrlAttribute()
    {
        return Storage::url('public/avatars/'.$this->id.'/'.$this->avatar);
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function parameters()
    {
        return $this->hasOne('App\UserParameter');
    }

    public function customization()
    {
        return $this->hasOne('App\UserCustomization');
    }

    public function customization_shake()
    {
        return $this->hasOne('App\UserCustomizeShake');
    }

    public function weekly_plans()
    {
        return $this->hasMany('App\UserWeeklyPlan');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription', 'user_id');
    }

    public function excluded_recipes()
    {
        return $this->belongsToMany('App\Recipe', 'excluded_recipes', 'user_id', 'recipe_id');
    }

    public function lifestyles()
    {
        return $this->belongsToMany('App\DietaryLifestyle', 'user_lifestyles', 'user_id', 'dietary_lifestyle_id');
    }
}
