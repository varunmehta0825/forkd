<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserParameter extends Model
{
    protected $table = 'user_meal_parameters';

    protected $fillable = [
        'user_id',
        'calorie_bracket_id',
        'passed_steps',
        'nursing',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function calorie_brackets()
    {
        return $this->belongsTo('App\CalorieBracket', 'calorie_bracket_id')
                    ->select(
                        'red',
                        'green',
                        'purple',
                        'yellow',
                        'blue',
                        'orange'
                        // 'grey'
                     );
    }

    public function calories()
    {
        return $this->belongsTo('App\CalorieBracket', 'calorie_bracket_id')
                    ->select('id', 'range_calories');
    }

    public function meal_template()
    {
        return $this->belongsTo('App\MealTemplate', 'meal_template_id');
    }
}
