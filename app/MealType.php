<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealType extends Model
{
    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'recipe_meal_type', 'meal_type_id', 'recipe_id');
    }
}
