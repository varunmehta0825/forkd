<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealTemplate extends Model
{
    public function user()
    {
        return $this->hasOne('App\UserParameter');
    }
}
