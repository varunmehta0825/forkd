<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeIngredient extends Model
{
    protected $table = 'recipe_ingredients';

    public function recipe()
    {
        return $this->belongsTo('App\Recipe');
    }

    public function ingredients()
    {
        return $this->belongsTo('App\Ingredient', 'ingredient_id');
    }

    public function preparation()
    {
        return $this->belongsTo('App\Preparation', 'preparation_id');
    }

    public function measurement()
    {
        return $this->belongsTo('App\Measurement', 'measurement_id');
    }
}
