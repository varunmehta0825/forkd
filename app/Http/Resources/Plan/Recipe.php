<?php

namespace App\Http\Resources\Plan;

use Illuminate\Http\Resources\Json\JsonResource;

class Recipe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => '/recipe/'.$this->id,
            'containers' => [
                'red'      => ($this->red_containers != 0) ? (int) $this->red_containers : null,
                'green'    => ($this->green_containers != 0) ? (int) $this->green_containers : null,
                'purple'   => ($this->purple_containers != 0) ? (int) $this->purple_containers : null,
                'yellow'   => ($this->yellow_containers != 0) ? (int) $this->yellow_containers : null,
                'blue'     => ($this->blue_containers != 0) ? (int) $this->blue_containers : null,
                'orange'   => ($this->orange_containers != 0) ? (int) $this->orange_containers : null,
                'grey'     => ($this->spoon_containers != 0) ? (int) $this->spoon_containers : null
            ]
        ];
    }
}
