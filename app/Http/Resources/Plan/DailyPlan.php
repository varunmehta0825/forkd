<?php

namespace App\Http\Resources\Plan;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DailyPlanResource;

class DailyPlan extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $plan = $this->meals_plan->groupBy('meal_type_id');
  
        return [
            'id' => $this->id,
            'day' => substr($this->day, 0, 3),
            'meals' => new MealPlanCollection($plan)
        ];
    }
}
