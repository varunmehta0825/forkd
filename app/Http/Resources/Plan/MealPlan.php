<?php

namespace App\Http\Resources\Plan;

use Illuminate\Http\Resources\Json\JsonResource;

class MealPlan extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arr = [];
        foreach ($this->resource as $key => $recipe) {
            $arr[$key] = [
                'id' => $recipe->meal_type->id,
                'meal_type' => $recipe->meal_type->name,
                'recipe' => new Recipe($recipe->recipes),
            ];
        }

        return $arr;
    }
}
