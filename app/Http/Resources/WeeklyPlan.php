<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Plan\DailyPlanCollection;
use App\UserWeeklyPlan;
use Carbon\Carbon;

class WeeklyPlan extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->from = strtotime($this->from);
        $this->to = strtotime($this->to);

        $date = Carbon::now()->addDays(7);

        $weekStartDate = $date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $date->endOfWeek()->format('Y-m-d H:i');

        $next_week = (bool) UserWeeklyPlan::where('user_id', auth()->user()->id)
                              ->where('from', $weekStartDate)
                              ->where('to', $weekEndDate)
                              ->first();

        return [
            'id' => $this->id,
            'date' => date('d M.', $this->from) .' - '. date('d M. Y', $this->to),
            'next_week' => $next_week,
            'daily_plan' => new DailyPlanCollection($this->daily_plan)
        ];
    }
}
