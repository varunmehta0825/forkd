<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCustomization extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cheat_day' => $this->cheat_day,
            'eating_out' => $this->eating_out,
            'travel' => $this->travel,
            'shake' => $this->shake,
            'alcohol_intake' => $this->alcohol_intake,
            'ingredients' => $this->ingredients
        ];
    }
}
