<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\UserMealPlan;
use Carbon\Carbon;
use App\UserWeeklyPlan;

class ShoppingList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ingredients = [];

        if(!empty($this->ingredients)) {
            foreach ($this->ingredients as $key => $ingredient) {

                foreach($ingredient->recipe_ingredients as $k => $recipe_ingredient) {
                    $qty = $this->getIngredientQTY($recipe_ingredient->recipe_id, $recipe_ingredient->qty);

                    if(isset($recipe_ingredient->measurement->name)) {
                        $measurement = trim($recipe_ingredient->measurement->name);
                        $ingredients[$key][$measurement]['ingredient_name'] = $ingredient->name;
                        $ingredients[$key][$measurement]['unit'] = $measurement;

                        if(isset($ingredients[$key][$measurement]['qty'])) {
                            $ingredients[$key][$measurement]['qty'] += $qty;
                        } else {
                            $ingredients[$key][$measurement]['qty'] = $qty;
                        }
                    }
                }
            }
        }

        return [
            'category_name' => $this->name,
            'ingredients' => $ingredients
        ];
    }

    public function getIngredientQTY($recipe_id, $ingredient_qty)
    {
        $count = UserMealPlan::where('recipe_id', $recipe_id)
                             ->whereHas('daily_plan', function($q) {
                                $q->whereHas('week', function($q) {
                                    $date = Carbon::now();
                                    $weekStartDate = $date->startOfWeek()->format('Y-m-d H:i');
                                    $weekEndDate = $date->endOfWeek()->format('Y-m-d H:i');

                                    $current_week_id = $this->getCurrentWeek($weekStartDate, $weekEndDate);

                                    $q->where('id', $current_week_id);
                                  });
                              })
                              ->get();

        return $ingredient_qty * count($count);
    }

    public function getCurrentWeek($weekStartDate, $weekEndDate) {
        $current_week = UserWeeklyPlan::select('id')
                                      ->where('user_id', auth()->user()->id)
                                      ->where('from', $weekStartDate)
                                      ->where('to', $weekEndDate)
                                      ->orderByDesc('created_at')
                                      ->first();

        return $current_week->id;
    }
}
