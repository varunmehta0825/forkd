<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

// use Storage;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = new Carbon($this->last_active);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'age' => $this->age,
            'gender' => $this->gender,
            'active' => (bool) $this->active,
            'confirmed' => (bool) $this->confirmed,
            'subscribed' => (bool) $this->subscription,
            'verify_request' => (bool) $this->verifyUser,
            'passed_steps' =>  isset($this->parameters->passed_steps) ? $this->parameters->passed_steps : false,
            'measurement' => $this->measurement,
            'last_active' => $dt->diffForHumans(),
            'created_at' =>  Carbon::parse($this->created_at)->format('M d Y'),
            'avatar' => '/user/avatar/'.$this->avatar.'/avatar.png',
        ];
    }
}
