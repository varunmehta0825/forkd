<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class AppController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = \DB::table('oauth_clients')->where('id', 2)->first();
    }

    protected function getOauthToken($grant_type, $p1, $p2)
    {
        $http = new Client;
        try {
            if($grant_type != 'social') {
                $response = $http->post(url('oauth/token'), [
                    'form_params' => [
                        'grant_type'         => $grant_type,
                        'client_id'          => $this->client->id,
                        'client_secret'      => $this->client->secret,
                        'username'           => $p1,
                        'password'           => $p2,
                        'scope'              => '',
                    ],
                    'http_errors' => false
                ]);
            } else {
                $response = $http->post(url('oauth/token'), [
                    'form_params' => [
                        'grant_type'         => $grant_type,
                        'client_id'          => $this->client->id,
                        'client_secret'      => $this->client->secret,
                        'accessToken'        => $p1,
                        'provider'           => $p2,
                    ],
                    'http_errors' => false
                ]);
            }

            $response = json_decode((string) $response->getBody(), true);
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            $response = response()->json('Something went wrong on the server.', $e->getCode());
        }

        return $response;
    }

    public function json_response($status, $code, $data)
    {
        if(isset($data->collection)) {
            $data->additional([
                'status' => $status,
            ]);

            return $data;
        }

        if($status == 'error') {
            return response()->json([
                'status' => $status,
                'errors' => $data
            ], $code);
        }

        return response()->json([
            'status' => $status,
            'data' => $data
        ], $code);
    }

}
