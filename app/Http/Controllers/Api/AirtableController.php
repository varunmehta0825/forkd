<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ingredient;
use App\Recipe;

class AirtableController extends Controller
{
    /**
     * TEMPORARY CONTROLLER
     * Importing data from airtable
     */

    public function getIngredients()
    {
        $airtable = new \DavidZadrazil\AirtableApi\Airtable('keyj7jwfLddkMv8Xr', 'appPKLVnSOLV9UaOo');
        $request = new \DavidZadrazil\AirtableApi\Request($airtable, 'Ingredients');
        $tableRequest = $request->getTable(['sortField' => 'Name']);

        do {
          foreach ($tableRequest->getRecords() as $record) {
              $row = (array) $record->getFields();

              $ingredient = new Ingredient;
              $ingredient->ingredient_id = $record->getId();
              $categories_id = null;
              $measurements_id = null;

              if(isset($row['Category']))
                $categories_id = \DB::table('categories')->where('name', $row['Category'])->pluck('id')->first();

              if(isset($row['Measurement']))
                $measurements_id = \DB::table('measurements')->where('name', $row['Measurement'])->pluck('id')->first();

              if($categories_id)
                $ingredient->category_id = $categories_id;

              if($measurements_id)
                $ingredient->measurement_id = $measurements_id;

              if(isset($row['Base QTY']))
                $ingredient->base_qty = $row['Base QTY'];

              if(isset($row['Name']))
                $ingredient->name = $row['Name'];

              if(isset($row['Calories']))
                $ingredient->calories = $row['Calories'];

              if(isset($row['Red Containers (Protien)']))
                $ingredient->red_containers = $row['Red Containers (Protien)'];

              if(isset($row['Green Containers (Veggies)']))
                $ingredient->green_containers = $row['Green Containers (Veggies)'];

              if(isset($row['Purple Containers (Fruit)']))
                $ingredient->purple_containers = $row['Purple Containers (Fruit)'];

              if(isset($row['Yellow A Containers (Carbs)']))
                $ingredient->yellow_containers = $row['Yellow A Containers (Carbs)'];

              if(isset($row['Blue Containers (Fats)']))
                $ingredient->blue_containers = $row['Blue Containers (Fats)'];

              if(isset($row['Orange Containers (Dressing/Seeds)']))
                $ingredient->orange_containers = $row['Orange Containers (Dressing/Seeds)'];

              if(isset($row['Spoon (Oils & Butters)']))
                $ingredient->spoon_containers = $row['Spoon (Oils & Butters)'];

              $ingredient->save();

          }
        } while ($tableRequest = $tableRequest->nextPage());

        dd('success');
    }

    public function getRecipes()
    {
        $airtable = new \DavidZadrazil\AirtableApi\Airtable('keyj7jwfLddkMv8Xr', 'appPKLVnSOLV9UaOo');
        $request = new \DavidZadrazil\AirtableApi\Request($airtable, 'Recipes');

        $tableRequest = $request->getTable(['sortField' => 'Recipe Name']);
        $i = 0;

        $exclude = array(
          'Baked Spaghetti Squash Fritters',
          'Coconut Curry Shrimp',
          'Slow Cooker Sweet Chops and Brown Rice',
          'Roasted Cauliflower Casserole',
          'Turmeric Rice Bowl with Root Vegetables & Chickpeas',
          'Spicy Chicken Bean Soup',
          'Shredded Mexican Chicken',
          'Roasted Garlic, Spinach, and Chicken White Pizza {Paleo, GF, DF}',
          'Parmesan Crusted Chicken',
          'Healthy Instant Pot Airfryer Chicken Taquitos',
          'Crispy Turkey Tostadas',
          'BBQ Chicken Pita Pizza',
          'Beef and Broccoli Bowl'
        );

        do {
          foreach ($tableRequest->getRecords() as $record) {
              $row = (array) $record->getFields();
              // dump($row);
              // Get related recipes
              // if(isset($row['Related Recipes'])) {
              //     $recipe_id = \DB::table('recipes')->where('recipe_id', $record->getId())->pluck('id')->first();
              //     $related_recipe_id = \DB::table('recipes')->where('recipe_id', $row['Related Recipes'][0])->pluck('id')->first();
              //     \DB::table('related_recipes')->insert(['recipe_id' => $recipe_id, 'related_recipe_id' => $related_recipe_id]);
              // }

              if(isset($row['Recipe Name']) && !empty($row['Recipe Name']) && !in_array($row['Recipe Name'], $exclude)) {
                  $recipe = new Recipe;
                  $recipe->recipe_id = $record->getId();
                  $recipe->name = (string) $row['Recipe Name'];

                  if(isset($row['Preparation']))
                      $recipe->preparation = (string) $row['Preparation'];

                  if(isset($row['Cook Time (Minutes)']))
                      $recipe->cook_time_m = (string) $row['Cook Time (Minutes)'];

                  if(isset($row['Ready Time (Minutes)']))
                      $recipe->ready_time_m = (string) $row['Ready Time (Minutes)'];

                  if(isset($row['Prep Time (Minutes)']))
                      $recipe->prep_time_m = (string) $row['Prep Time (Minutes)'];

                  if(isset($row['Prep Time (Minutes)']))
                      $recipe->prep_time_m = (string) $row['Prep Time (Minutes)'];

                  if(isset($row['Servings']))
                      $recipe->servings = (string) $row['Servings'];

                  if(isset($row['Secondary Ingredients Title']))
                      $recipe->secondary_ingredients_title = (string) $row['Secondary Ingredients Title'];

                  if(isset($row['Prep Time (Hours)']))
                      $recipe->prep_time_h = round($row['Prep Time (Hours)'], 2);

                  if(isset($row['Cook Time (Hours)']))
                      $recipe->cook_time_h = round($row['Cook Time (Hours)'], 2);

                  if(isset($row['Ready Time (Hours)']))
                      $recipe->ready_time_h = round($row['Ready Time (Hours)'], 2);

                  if(isset($row['Display Prep Time']))
                      $recipe->display_prep_time = (string) $row['Display Prep Time'];

                  if(isset($row['Display Cook Time']))
                      $recipe->display_cook_time = (string) $row['Display Cook Time'];

                  if(isset($row['Display Ready Time']))
                      $recipe->display_ready_time = (string) $row['Display Ready Time'];

                  if(isset($row['Proofed?']))
                      $recipe->proofed = $row['Proofed?'];

                  if(isset($row['Count per Serving']))
                      $recipe->count_per_serving = (string) $row['Count per Serving'];

                  if(isset($row['Tested']))
                      $recipe->tested = (string) $row['Tested'];

                  if(isset($row['Total Red Containers Per Serving']))
                      $recipe->red_containers = $row['Total Red Containers Per Serving'];

                  if(isset($row['Total Green Containers Per Serving']))
                      $recipe->green_containers = $row['Total Green Containers Per Serving'];

                  if(isset($row['Total Purple Containers Per Serving']))
                      $recipe->purple_containers = $row['Total Purple Containers Per Serving'];

                  if(isset($row['Total Yellow Containers Per Serving']))
                      $recipe->yellow_containers = $row['Total Yellow Containers Per Serving'];

                  if(isset($row['Total Blue Containers Per Serving']))
                      $recipe->blue_containers = $row['Total Blue Containers Per Serving'];

                  if(isset($row['Total Orange Containers Per Serving']))
                      $recipe->orange_containers = $row['Total Orange Containers Per Serving'];

                  if(isset($row['Total Spoons Per Serving']))
                      $recipe->spoon_containers = $row['Total Spoons Per Serving'];


                  $recipe->save();

                  if(isset($row['Meal Type'])) {
                      foreach ($row['Meal Type'] as $key => $value) {
                          $id = \DB::table('meal_types')->where('name', $value)->pluck('id')->first();
                          if($id == null)
                            dd($value);
                          \DB::table('recipe_meal_type')->insert(['recipe_id' => $recipe->id, 'meal_type_id' => $id]);
                      }
                  }

                  if(isset($row['Dietary Lifestyle'])) {
                      foreach ($row['Dietary Lifestyle'] as $key => $value) {
                          $id = \DB::table('dietary_lifestyles')->where('name', $value)->pluck('id')->first();
                          \DB::table('recipe_dietary_lifestyle')->insert(['recipe_id' => $recipe->id, 'dietary_lifestyle_id' => $id]);
                      }
                  }

                  if(isset($row['Recipe Types'])) {
                      foreach ($row['Recipe Types'] as $key => $value) {
                          $id = \DB::table('recipe_types')->where('name', $value)->pluck('id')->first();
                          \DB::table('recipe_recipe_type')->insert(['recipe_id' => $recipe->id, 'recipe_type_id' => $id]);
                      }
                  }

                  if(isset($row['Cuisine Type'])) {
                      foreach ($row['Cuisine Type'] as $key => $value) {
                          $id = \DB::table('cuisine_types')->where('name', $value)->pluck('id')->first();
                          \DB::table('recipe_cuisine_type')->insert(['recipe_id' => $recipe->id, 'cuisine_type_id' => $id]);
                      }
                  }

              }
          }
        } while ($tableRequest = $tableRequest->nextPage());

        dd('success');
    }

    public function getRecipeIngredients()
    {
        $airtable = new \DavidZadrazil\AirtableApi\Airtable('keyj7jwfLddkMv8Xr', 'appPKLVnSOLV9UaOo');
        $request = new \DavidZadrazil\AirtableApi\Request($airtable, 'RecipeIngredients');

        $tableRequest = $request->getTable(['sortField' => 'Recipe']);
        $i = 0;
        do {
          foreach ($tableRequest->getRecords() as $record) {
              $row = (array) $record->getFields();

              $preparation_id = null;
              $recipe_qty = null;
              $qty = null;
              $measurement_id = null;
              $secondary_recipe = null;

              if(isset($row['Recipe'][0])) {
                  $recipe_id = \DB::table('recipes')->where('recipe_id', $row['Recipe'][0])->pluck('id')->first();

                  if(isset($row['Ingredient'][0])) {
                      $ingredient_id = \DB::table('ingredients')->where('ingredient_id', $row['Ingredient'][0])->pluck('id')->first();
                  }

                  if(isset($row['Preparation'][0])) {
                      $preparation_id = \DB::table('preparations')->where('name', $row['Preparation'][0])->pluck('id')->first();
                  }

                  if(isset($row['Recipe QTY'])) {
                      $recipe_qty = $row['Recipe QTY'];
                  }

                  if(isset($row['QTY'])) {
                      $qty = $row['QTY'];
                  }

                  if(isset($row['Unit'][0]) && is_array($row['Unit'][0])) {
                      $measurement_id = \DB::table('measurements')->where('name', $row['Unit'][0])->pluck('id')->first();
                  } elseif(isset($row['Unit'])) {
                      $measurement_id = \DB::table('measurements')->where('name', $row['Unit'])->pluck('id')->first();
                  }
                  //
                  // if($row['Recipe'][0] == 'recFSbYxdtFR3uhH1') {
                  //     // if($row['QTY'] == 0.25) {
                  //         dump($row['Unit']);
                  //         dd($row);
                  //     // }
                  // }

                  if(isset($row['Secondary Ingredients'])) {
                      $secondary_recipe = 1;
                  }

                  \DB::table('recipe_ingredients')->insert([
                      'recipe_id' => $recipe_id,
                      'ingredient_id' => $ingredient_id,
                      'preparation_id' => $preparation_id,
                      'recipe_qty' => $recipe_qty,
                      'qty' => $qty,
                      'measurement_id' => $measurement_id,
                      'secondary_recipe' => $secondary_recipe,
                  ]);
              }

          }
        } while ($tableRequest = $tableRequest->nextPage());

        dd('success');
    }

    public function getAllRecipes()
    {
        $recipes = Recipe::all();
        $i = 0;
        foreach ($recipes as $recipe) {
            echo '<pre>';
            echo $recipe->id . ' ' . $recipe->name;

            $red = (int)$recipe->ingredients->sum('red_containers');
            $green = (int)$recipe->ingredients->sum('green_containers');
            $purple = (int)$recipe->ingredients->sum('purple_containers');
            $yellow = (int)$recipe->ingredients->sum('yellow_containers');
            $blue = (int)$recipe->ingredients->sum('blue_containers');
            $orange = (int) $recipe->ingredients->sum('orange_containers');
            $total = $red + $green + $purple + $yellow + $blue + $orange;

            echo '<br>red: ' . $recipe->ingredients->sum('red_containers');
            echo '<br>green: ' . $recipe->ingredients->sum('green_containers');
            echo '<br>purple: ' . $recipe->ingredients->sum('purple_containers');
            echo '<br>yellow: ' . $recipe->ingredients->sum('yellow_containers');
            echo '<br>blue: ' . $recipe->ingredients->sum('blue_containers');
            echo '<br>orange: ' . $recipe->ingredients->sum('orange_containers');
            echo '<br>TOTAL: ' . $total;

            if($total >= 4)
              $i++;
            dump('========================');
            echo '</pre>';
        }
        dd($i);

    }
}
