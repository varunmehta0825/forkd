<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use Carbon\Carbon;
use App\Category;
use App\UserWeeklyPlan;
use App\Http\Resources\ShoppingListCollection;

class ShoppingListController extends AppController
{
    public function show()
    {
        $date = Carbon::now();
        $params = [];

        $weekStartDate = $date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $date->endOfWeek()->format('Y-m-d H:i');

        $current_week = UserWeeklyPlan::where('user_id', auth()->user()->id)
                                      ->where('from', $weekStartDate)
                                      ->where('to', $weekEndDate)
                                      ->orderBy('created_at', 'DESC')
                                      ->pluck('id')
                                      ->first();

        if(!$current_week)
            return $this->json_response('error', 404, [
                'message' => 'Meal plan not found.'
            ]);

        $params['week_start_date'] = $weekStartDate;
        $params['week_end_date'] = $weekEndDate;
        $params['current_week'] = $current_week;

        $ingredients = Category::select('id', 'name')
                                ->with(['ingredients' => function($q) use ($params) {
                                    $q->select('id', 'name', 'category_id')
                                      ->with(['recipe_ingredients' => function($q) use ($params) {
                                          $q->select('recipe_id', 'qty', 'ingredient_id', 'measurement_id')
                                            ->with(['measurement'])
                                            ->whereHas('recipe', function($q) use ($params) {
                                                $q->whereHas('meal_plan', function($q) use ($params) {
                                                   $q->whereHas('daily_plan', function($q) use ($params) {
                                                      $q->whereHas('week', function($q) use ($params) {
                                                          $q->where('user_id', auth()->user()->id)
                                                            ->where('id', $params['current_week'])
                                                            ->where('from', $params['week_start_date'])
                                                            ->where('to', $params['week_end_date']);
                                                      });
                                                  });
                                              });
                                          });
                                      }])
                                      ->whereHas('recipe_ingredients', function($q) use ($params) {
                                          $q->whereHas('recipe', function($q) use ($params) {
                                              $q->whereHas('meal_plan', function($q) use ($params) {
                                                 $q->whereHas('daily_plan', function($q) use ($params) {
                                                     $q->whereHas('week', function($q) use ($params) {
                                                         $q->where('user_id', auth()->user()->id)
                                                           ->where('id', $params['current_week'])
                                                           ->where('from', $params['week_start_date'])
                                                           ->where('to', $params['week_end_date']);
                                                     });
                                                });
                                            });
                                        });
                                    });
                                }])
                                ->get();

        return $this->json_response('success', 200,
            new ShoppingListCollection($ingredients)
        );
    }
}
