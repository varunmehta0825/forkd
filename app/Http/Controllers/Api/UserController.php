<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\Http\Resources\User as UserResource;
use App\User;

class UserController extends AppController
{
    public function show(Request $request, $id = null)
    {
        if($id !== null)
            $user = User::where('id', $id)->first();
        else
            $user = $request->user();

        if(!$user)
            return $this->json_response('success', 200, []);

        return $this->json_response('success', 200, new UserResource($user));
    }

    public function avatar($folder, $image = null)
    {
         $filePath = storage_path('app/public/avatars/'.$folder.'/avatar.png');

         if (! file_exists($filePath)) {
            return $this->json_response('error', 404, [
                'message' => 'Media file doesn\'t exist'
            ]);
         }

         return response()->file($filePath);
    }
}
