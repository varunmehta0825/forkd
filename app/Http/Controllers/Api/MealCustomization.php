<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\Http\Resources\UserCustomization as UserCustomizationResource;
use App\UserCustomization;

class MealCustomization extends AppController
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }

    public function selectCustomizationSteps(Request $request)
    {
        $steps = $request->all();

        if(!$steps)
            return $this->json_response('error', 400, [
                'message' => 'You aren\'t select any step.'
            ]);

        if(!$this->user->customization)
            $this->user->customization()
                       ->create($steps);
        else
            $this->user->customization()
                       ->update($steps);

        $step_links = [];

        foreach ($steps as $key => $step) {
            if($step == 1)
                array_push($step_links, str_replace('_', '-', $key));
        }

        return $this->json_response('success', 201, $step_links);
    }

    public function getCustomizationSteps()
    {
        $step_links = [];
        $steps = $this->user
                      ->customization()
                      ->select('cheat_day', 'eating_out', 'travel', 'shake', 'alcohol_intake', 'ingredients')
                      ->first();

        foreach ($steps->toArray() as $key => $step) {
            if($step == 1)
                array_push($step_links, str_replace('_', '-', $key));
        }

        return $this->json_response('success', 201, $step_links);
    }

    public function customizeShake(Request $request)
    {
        $data = $request->all();

        if(!$this->user->customization_shake)
            $this->user->customization_shake()
                       ->create($data);
        else
            $this->user->customization_shake()
                       ->update($data);

        return $this->json_response('success', 201, []);
    }

    public function getPreviousSettings() {
        $settings = [];

        if(!empty($this->user->customization_shake)) {
            if($this->user->customization_shake->shake)
                array_push($settings, 'Making Shakes');
        }

        return $this->json_response('success', 200, $settings);
    }
}
