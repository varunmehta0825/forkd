<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\Http\Resources\Recipe as RecipeResource;
use Illuminate\Support\Facades\Storage;
use App\UserWeeklyPlan;
use Carbon\Carbon;
use App\UserMealPlan;
use App\Recipe;
use PDF;

class RecipeController extends AppController
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            $this->lifestyle_id = $this->user->parameters->dietary_lifestyle_id;
            $this->excluded_recipes = $this->user->excluded_recipes->pluck('id');

            return $next($request);
        });
    }

    public function show($recipe_id)
    {
        $recipe = Recipe::with('dietary_lifestyles')
                        ->with(['recipe_ingredients' => function($q) {
                            $q->with(['ingredients', 'measurement']);
                        }])
                        ->where('id', $recipe_id)
                        ->first();

        if($recipe == null)
            return $this->json_response('error', 404, [
                'message' => 'Nothing found.'
            ]);

        return $this->json_response('success', 200, new RecipeResource($recipe));
    }

    public function getCurrentWeekRecipes($week)
    {
        $date = Carbon::now();

        if($week == 'next')
            $date->addDays(7);

        $weekStartDate = $date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $date->endOfWeek()->format('Y-m-d H:i');

        $plan = UserWeeklyPlan::where('user_id', auth()->user()->id)
                    ->where('from', $weekStartDate)
                    ->where('to', $weekEndDate)
                    ->select('id', 'user_id', 'from', 'to')
                    ->with([
                        'daily_plan.meals_plan.meal_type',
                        'daily_plan.meals_plan.recipes.ingredients',
                    ])
                    ->with(['daily_plan.meals_plan' => function($q) {
                        $q->select('user_daily_plan_id', 'meal_type_id', 'recipe_id');
                    }])
                    ->with(['daily_plan.meals_plan.recipes' => function($q) {
                        $q->with('dietary_lifestyles')
                          ->with(['recipe_ingredients' => function($q) {
                            $q->with(['ingredients', 'measurement']);
                        }]);
                    }])
                    ->with(['daily_plan' => function($q) {
                        $q->where('day', 'Sunday');
                    }])
                    ->orderBy('created_at', 'desc')
                    ->first()
                    ->toArray();

        return $plan;
    }

    public function createPDF($week = null)
    {
        $plan = $this->getCurrentWeekRecipes($week);
        $pdf = PDF::loadView('recipe.pdf', [
            'plan' => $plan,
            'user' => auth()->user()
        ])
        ->setPaper('a4', 'portrait');

        if($plan == null)
            return $this->json_response('error', 400, [
                'message' => 'Something went wrong. Please refresh page and try again.'
            ]);

        // return $pdf->stream('pdfview.pdf');
        return $pdf->download('pdfview.pdf');
    }

    public function swap($week_id, $meal_id, $recipe_id)
    {
        $shake = $this->user->customization_shake->meal ?? null;
        $lifestyle_id = $this->lifestyle_id;
        $containers = [];
        $recipe = Recipe::where('id', $recipe_id)
                        ->with('ingredients')
                        ->with(['meal_types' => function($q) use ($meal_id) {
                            $q->where('meal_types.id', $meal_id);
                        }])
                        ->first();
        $containers['red']     = $recipe->ingredients->sum('red_containers');
        $containers['green']   = $recipe->ingredients->sum('green_containers');
        $containers['purple']  = $recipe->ingredients->sum('purple_containers');
        $containers['yellow']  = $recipe->ingredients->sum('yellow_containers');
        $containers['blue']    = $recipe->ingredients->sum('blue_containers');
        $containers['orange']  = $recipe->ingredients->sum('orange_containers');

        // $meal = $recipe->meal_types[0]->name;

        $new_recipe = Recipe::inRandomOrder()
                            ->whereNotIn('id', $this->excluded_recipes)
                            ->whereNotIn('id', [$recipe_id])
                            ->whereHas('meal_types', function($q) use ($meal_id) {
                                $q->where('meal_types.id', $meal_id);
                            })
                            ->whereHas('dietary_lifestyles', function($q) use ($lifestyle_id) {
                                if($lifestyle_id !== NULL)
                                    $q->where('dietary_lifestyle_id', $lifestyle_id);
                            })
                            // ->whereHas('recipe_types', function($q) use ($shake) {
                            //     if($shake !== NULL)
                            //         $q->where('name', 'Shakeology');
                            // })
                            ->with('ingredients')
                            ->whereHas('ingredients', function($q) use ($containers) {
                                $q->groupBy('recipe_ingredients.recipe_id')
                                  ->havingRaw('SUM(red_containers) = ' . $containers['red'])
                                  ->havingRaw('SUM(green_containers) = ' . $containers['green'])
                                  ->havingRaw('SUM(purple_containers) = ' . $containers['purple'])
                                  ->havingRaw('SUM(yellow_containers) = ' . $containers['yellow'])
                                  ->havingRaw('SUM(blue_containers) = ' . $containers['blue'])
                                  ->havingRaw('SUM(orange_containers) = ' . $containers['orange']);
                                })
                            ->first();

        if($new_recipe == null)
            return $this->json_response('error', 404, [
                'message' => 'Can\'t find any recipe.'
            ]);

        $plan = UserMealPlan::where('meal_type_id', $meal_id)
                            ->where('recipe_id', $recipe_id)
                            ->whereHas('daily_plan', function($q) use ($week_id) {
                                $q->where('user_weekly_plan_id', $week_id)
                                  ->whereHas('week', function($q) {
                                      $q->where('user_id', auth()->user()->id);
                                  });
                            })
                            ->get();

        if($plan == null)
            return $this->json_response('error', 404, [
                'message' => 'Plan Not Found.'
            ]);

        foreach ($plan as $item) {
            $item->recipe_id = $new_recipe->id;
            $item->save();
        }

        return $this->json_response('success', 200, []);
    }
}
