<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\Http\Resources\SearchIngredientsCollection as SearchIngredients;
use App\DietaryLifestyle;
use App\CalorieBracket;
use App\UserParameter;
use App\Ingredient;
use App\Recipe;

class StoreAttributesController extends AppController
{

    public function step2(Request $request)
    {
        $user = auth()->user();
        $calorie_bracket_id = $request->input('calorie_brackets');
        $target = CalorieBracket::where('id', $calorie_bracket_id)->first();

        if(!$target)
            return $this->json_response('error', 400, [
                'message' => 'Calorie target doesn\'t exist.'
            ]);

        $user->parameters()->update([
            'calorie_bracket_id' => $calorie_bracket_id,
            'nursing' => $request->input('nursing'),
            'passed_steps' => 2
        ]);

        return $this->json_response('success', 201, [
            'message' => 'Your parameters successfully was saved.'
        ]);
    }

    public function step3(Request $request)
    {
        $user = auth()->user();
        $lifestyles = $request->all();
        $lifestyles = array_filter($lifestyles);

        if(empty($lifestyles))
            return $this->json_response('error', 400, []);

        $user->parameters()->update([
            'passed_steps' => 3
        ]);

        if(isset($lifestyles['no_restrictions']) && $lifestyles['no_restrictions'] === true) {
            $user->lifestyles()->detach();
            return $this->json_response('success', 200, []);
        }

        $user->lifestyles()->sync($lifestyles);

        return $this->json_response('success', 201, [
            'message' => 'Your parameters successfully was saved.'
        ]);
    }

    public function step4(Request $request)
    {
        $ids = $request->input('ids');
        $excluded_recipes = Recipe::with('recipe_ingredients')
                                  ->has('recipe_ingredients')
                                  ->whereHas('recipe_ingredients', function($query) use ($ids) {
                                      $query->whereIn('ingredient_id', $ids);
                                  })->pluck('id');

        auth()->user()->parameters()->update([
            'passed_steps' => 5,
        ]);

        auth()->user()->excluded_recipes()->sync($excluded_recipes, false);

        return $this->json_response('success', 200, []);
    }

    public function step5(Request $request)
    {
        $meal_template_id = $request->input('meal-template');

        auth()->user()->parameters()->update([
            'meal_template_id' => $meal_template_id,
            'passed_steps' => 5,
        ]);

        return $this->json_response('success', 200, []);
    }

    public function searchIngredients()
    {
        $ingredients = [];
        $ingredients = Ingredient::get();

        return $this->json_response('success', 200, new SearchIngredients($ingredients));
    }

}
