<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirm;
use App\VerifyUser;
use Stripe\Stripe;

class CheckoutController extends AppController
{

    public function subscribe(Request $request)
    {
        $v = validator($request->all(), [
            'plan'               => 'required',
            'card_name'          => 'required|string|min:3|max:36',
            'card_number'        => 'required|integer',
            'card_exp_m'         => 'required|digits:2',
            'card_exp_y'         => 'required|digits:2',
            'card_cvc'           => 'required|digits:3',
            'card_address_zip'   => 'required',
        ], [
            'card_address_zip.required' => 'The zip code field is required.'
        ]);

        if($v->fails())
            return $this->json_response('error', 400, $v->errors());

        try {
            Stripe::setApiKey(env('STRIPE_SECRET'));

            $card = \Stripe\Token::create([
                      "card" => [
                          'name'        => $request->card_name,
                          "number"      => $request->card_number,
                          "exp_month"   => $request->card_exp_m,
                          'exp_year'    => $request->card_exp_y,
                          'cvc'         => $request->card_cvc,
                          'address_zip' => $request->card_address_zip
                      ]
                ]);

            $user = auth()->user();

            if($user->confirmed) {
                $user->newSubscription('main', $request->plan)->create($card['id']);
            } else {
                VerifyUser::create([
                   'user_id' => $user->id,
                   'stripe_token' => $card['id'],
                   'stripe_plan' => $request->plan,
                   'token' => str_random(40)
                ]);

                Mail::to($user->email)->send(new RegistrationConfirm($user));

                return $this->json_response('success', 201, [
                    'message' => 'Check your email for confirmation to finalize registration and subscription.'
                ]);
            }

            return $this->json_response('success', 201, [
                'message' => 'You have been successfully subscribed.'
            ]);
        } catch (\Exception $e) {
            return $this->json_response('error', 400, [
                'message' => $e->getMessage()
            ]);
        }

    }

}
