<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\CalorieBracket;

class CalorieCalculatorController extends AppController
{
    public function index(Request $request)
    {
        $goal = $request->input('goal');
        $weight = $request->input('weight');
        $calorie_target = 0;
        $target = null;

        if(!isset($goal) || !isset($weight))
            return $this->json_response('error', 400, []);

        $brackets = $this->getCalorieBrackets();

        // For example my weight are 165 lbs.
        $caloric_baseline = $weight * 11;                 // 165 * 11 = 1815

        if($goal == 'not_working') {
            $calorie_target = $caloric_baseline - 400;    // 1415 calories
        } elseif($goal == 'moderately_maintain') {
            $calorie_target = $caloric_baseline + 400;    // 2215 calories
        } elseif($goal == 'moderately_lose') {
            $calorie_target = $caloric_baseline - 750;    // 1065 calories - does not match with any calorie target
        } elseif($goal == 'extremely_maintain') {
            $calorie_target = $caloric_baseline + 600;    // 2415 calories - does not match with any calorie target
        } elseif($goal == 'extremely_lose') {
            $calorie_target = $caloric_baseline - 750;    // 1065 calories - does not match with any calorie target
        }
        // And in final for weight 75kg/165lbs we have 3 unavailable options

        foreach ($brackets as $key => $value) {
            if($calorie_target >= $value['from'] && $calorie_target <= $value['to'])
                $target = $key;
        }

        if(!$target)
            return $this->json_response('error', 400, []);

        return $this->json_response('success', 200, [
            'target' => $target,
            'calorie_from' => $brackets[$target]['from'],
            'calorie_to' => $brackets[$target]['to'],
        ]);
    }

    public function getCalorieBrackets()
    {
        $array = [];
        $brackets = CalorieBracket::get();

        foreach ($brackets as $key => $value) {
            $calories = explode(' - ', $value->range_calories);
            $from = $calories[0];
            $to = $calories[1];
            $array[$value->id]['from'] = (int) $from;
            $array[$value->id]['to'] = (int) $to;
        }

        return $array;
    }
}
