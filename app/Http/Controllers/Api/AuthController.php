<?php

namespace App\Http\Controllers\Api;

use Avatar;
use Storage;
use App\User;
use Stripe\Stripe;
use App\VerifyUser;
use App\UserParameter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\AppController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Resources\User as UserResource;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Mail\RegistrationConfirm;
use GuzzleHttp\Client;
use App\PasswordReset;

class AuthController extends AppController
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request))
        {
            $this->fireLockoutEvent($request);
            $seconds = $this->limiter()->availableIn($this->throttleKey($request));

            return $this->json_response('error', Response::HTTP_BAD_REQUEST, [
                'message' => 'Too many login attempts. Please try again in '.$seconds.' seconds.'
            ]);
        }

        $v = validator($request->all(), [
            'email'         => 'required|string|email|max:255',
            'password'      => 'required|string|min:6',
        ]);

        if($v->fails())
            return $this->json_response('error', Response::HTTP_BAD_REQUEST, $v->errors());

        $credentials = $request->only('email', 'password');
        if(!auth()->attempt($credentials)) {
            $this->incrementLoginAttempts($request);
        }

        $user = User::where('email', $request->email)->first();

        if(!$user)
            return $this->json_response('error', Response::HTTP_BAD_REQUEST, [
                'email' => ['Account with this email does not exist. Please check it and try again.']
            ]);

        if(Hash::check($request->password, $user->password)) {
            $response = $this->getOauthToken('password', $request->email, $request->password);

            if(isset($response['error']))
                return $this->json_response('error', Response::HTTP_BAD_REQUEST, $response);

            $user->last_active = date('Y-m-d H:i:s');
            $user->save();

            $response['user'] = new UserResource($user);
            return $this->json_response('success', Response::HTTP_OK, $response);
        }

        return $this->json_response('error', Response::HTTP_BAD_REQUEST, [
            'password' => ['Your password is incorrect.']
        ]);
    }

    public function register(Request $request)
    {
        $v = validator($request->all(), [
            'name'                  => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => '',
            'terms'                 => 'in:1'
        ],[
            'terms.in'        => 'You should agree with terms and conditions.'
        ]);

        if($v->fails())
            return $this->json_response('error', Response::HTTP_BAD_REQUEST, $v->errors());

        $folder = time().mt_rand(1, 999999);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->avatar = $folder;
        $user->confirmed = 1;

        $user->last_active = date('Y-m-d H:i:s');
        $user->save();

        // Temporary
        UserParameter::create([
           'user_id' => $user->id
        ]);

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('public/avatars/'.$folder.'/avatar.png', (string) $avatar);

        $response = $this->getOauthToken('password', $request->email, $request->password);
        $response['user'] = new UserResource($user);

        return $this->json_response('success', 201, $response);
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();

        if(isset($verifyUser)) {
            $user = $verifyUser->user;

            if(!$user->confirmed) {
                $verifyUser->user->confirmed = 1;
                $verifyUser->user->save();

                try {
                    Stripe::setApiKey(env('STRIPE_SECRET'));
                    $user->newSubscription('main', $verifyUser->stripe_plan)
                                        ->create($verifyUser->stripe_token);

                    UserParameter::create([
                       'user_id' => $user->id
                    ]);
                } catch (\Exception $e) {
                    return $this->json_response('error', 400, [
                        'message' => $e->getMessage()
                    ]);
                }
            }

            $verifyUser->delete();
            return $this->json_response('success', 201, [
                'message' => 'Your account successfully was confirmed.'
            ]);
        } else {
            return $this->json_response('error', 400, [
                'message' => 'Sorry your email cannot be identified.'
            ]);
        }

        return $this->json_response('success', 201, []);
    }

    public function logout()
    {
        if (\Auth::check()) {
            \Auth::user()->AauthAcessToken()->delete();
            return $this->json_response('success', 200, [
                'message' => 'You was logout.'
            ]);
        }

        return $this->json_response('error', 400, [
            'message' => 'You\'re loggout.'
        ]);
    }

    public function resendConfirmation()
    {
        $user = auth()->user();

        $user->verifyUser()->update(array(
            'token' => str_random(40)
        ));

        Mail::to($user->email)->send(new RegistrationConfirm($user));

        return $this->json_response('success', 200, [
            'message' => 'Confirmation email successfuly sent.'
        ]);
    }

    public function changePassword(Request $request)
    {
        $v = validator($request->all(), [
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => ''
        ]);

        if($v->fails())
            return $this->json_response('error', 400, $v->errors());

        $user = auth()->user();
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->json_response('success', 200, [
            'message' => 'Password was changed.'
        ]);
    }

    public function changeAvatar(Request $request)
    {
        $v = validator($request->all(), [
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if($v->fails())
            return $this->json_response('error', 400, $v->errors());

        $avatar = $request->file('avatar');

        $user = auth()->user();
        Storage::deleteDirectory('public/avatars/'.$user->avatar);

        $folder = time().mt_rand(1, 999999);
        $user->avatar = $folder;
        $user->save();

        Storage::disk('local')->putFileAs(
            'public/avatars/'.$folder,
            $avatar,
            'avatar.png'
        );

        return $this->json_response('success', 200, []);
    }

    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user)
            return $this->json_response('error', 400, [
                'email' => ['We can\'t find a user with that e-mail address.']
            ]);

        $passwordReset = PasswordReset::updateOrCreate([
                                          'email' => $user->email,
                                          'token' => str_random(60)
                                      ]);

        if($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );

        return $this->json_response('success', 200, [
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    public function reset(Request $request)
    {
        $response = [];
        $this->validate($request, [
            'token'                 => 'required|string',
            'email'                 => 'required|string|email|max:255',
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => '',
        ]);

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if(!$passwordReset)
            return $this->json_response('error', 400, [
                'message' => 'Password reset token is invalid.'
            ]);

        $user = User::where('email', $passwordReset->email)->first();

        if(!$user)
            return $this->json_response('error', 400, [
                'message' => 'User with that e-mail address doesn\'t exist.'
            ]);

        $user->password = bcrypt($request->password);
        $user->save();

        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));

        return $this->json_response('success', 200, [
            'message' => 'Your password has been changed.'
        ]);
    }
}
