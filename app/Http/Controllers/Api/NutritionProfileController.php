<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\DietaryLifestyle;
use App\CalorieBracket;


class NutritionProfileController extends AppController
{
    public function show()
    {
        $calories = CalorieBracket::select('id', 'range_calories')
                                  ->get()
                                  ->toArray();

        $lifestyles = DietaryLifestyle::get()
                                      ->toArray();

        $user = auth()->user();

        $data = array(
            'calories'            => $calories,
            'lifestyles'          => $lifestyles,
            'selected_lifestyles' => $user->lifestyles->toArray(),
            'user'                => array(
                                            'age'         => $user->age,
                                            'gender'      => $user->gender,
                                            'measurement' => $user->measurement,
                                            'calorie'     => $user->parameters->calories
                                          )
            );

        return $this->json_response('success', 200, $data);
    }

    public function update(Request $request)
    {
        $fields = $request->all();

        $v = validator($request->all(), [
            'age'          => 'nullable|integer|digits_between:1,2',
            'gender'       => 'nullable|in:Female,Male',
            'measurement'  => 'nullable|in:Metric,US',
            'calories'     => 'required|integer'
        ]);

        if($v->fails())
            return $this->json_response('error', 400, $v->errors());

        $user = auth()->user();
        $user->measurement = $fields['measurement'];
        $user->gender = $fields['gender'];
        $user->age = $fields['age'];
        $user->save();
        $user->parameters()
             ->update(
                 array(
                    'calorie_bracket_id' => $fields['calories']
                 )
             );

        if($fields['lifestyles'] !== 0)
            $user->lifestyles()->sync($fields['lifestyles'], true);

        return $this->json_response('success', 200, []);
    }
}
