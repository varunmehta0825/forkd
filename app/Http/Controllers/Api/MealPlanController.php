<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\Http\Resources\WeeklyPlan;
use App\RecipeIngredient;
use App\UserParameter;
use App\UserWeeklyPlan;
use Carbon\Carbon;
use App\Recipe;
use App\RecipeType;

class MealPlanController extends AppController
{
    /*
    *  The most complicated part of app.
    *  Generate meal plan for one week including all requirements.
    *  FOR TESTING PURPOSES. Not final algorithm. Need optimizations.
    */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            $this->cont = $this->user->parameters->calorie_brackets->toArray();
            $this->calorie_bracket_id = $this->user->parameters->calorie_bracket_id ?: NULL;
            $this->lifestyle_ids = $this->user->lifestyles->pluck('id');
            $this->excluded_recipes = $this->user->excluded_recipes->pluck('id');
            $this->meal_template = $this->user->parameters->meal_template_id;
            $this->date = Carbon::now();
            $this->meals = [];
            $this->leftOver = false;

            if($this->calorie_bracket_id == 1)
                $this->meals = ['Lunch', 'Dinner', 'Breakfast', 'Snack 1', 'Lunch', 'Snack 2'];

            if($this->calorie_bracket_id == 2)
                $this->meals = ['Lunch', 'Dinner', 'Breakfast', 'Breakfast', 'Snack 1', 'Lunch', 'Snack 2', 'Dinner'];

            if($this->calorie_bracket_id == 3)
                $this->meals = ['Lunch', 'Dinner', 'Breakfast', 'Snack 1',  'Snack 1', 'Lunch', 'Snack 2', 'Snack 2', 'Dinner', 'Dinner'];

            if($this->calorie_bracket_id == 4)
                $this->meals = ['Lunch', 'Dinner', 'Breakfast', 'Breakfast', 'Snack 1', 'Snack 1', 'Lunch', 'Snack 2', 'Snack 2', 'Dinner', 'Dinner'];

            return $next($request);
        });
    }

    public function show($week = 'current')
    {
        $current_day = $this->date->dayOfWeek;
        $weekStartDate = $this->date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $this->date->endOfWeek()->format('Y-m-d H:i');

        $plan_current_week = UserWeeklyPlan::where('user_id', auth()->user()->id)
                                            ->where('from', $weekStartDate)
                                            ->where('to', $weekEndDate)
                                            ->first();

        if(!$plan_current_week && $current_day == \Carbon\Carbon::SUNDAY)
            $week = 'next';

        if($week == 'next')
            $this->date->addDays(7);

        $weekStartDate = $this->date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $this->date->endOfWeek()->format('Y-m-d H:i');

        $plan = UserWeeklyPlan::where('user_id', auth()->user()->id)
                    ->where('from', $weekStartDate)
                    ->where('to', $weekEndDate)
                    ->select('id', 'user_id', 'from', 'to', 'created_at')
                    ->with([
                        'daily_plan.meals_plan.meal_type',
                        'daily_plan.meals_plan.recipes.ingredients',
                    ])
                    ->with(['daily_plan' => function($q) {
                        $q->select('user_weekly_plan_id', 'day');
                    }])
                    ->with(['daily_plan.meals_plan' => function($q) {
                        $q->select('user_daily_plan_id', 'meal_type_id', 'recipe_id');
                    }])
                    ->orderBy('created_at', 'desc')
                    ->first();

        if($plan == NULL)
            return $this->json_response('error', 404, [
                'message' => 'Nothing found.'
            ]);

        return $this->json_response('success', 200, new WeeklyPlan($plan));
    }

    public function store($week = 'current')
    {
        // Exclude shakeology recipes
        $this->excludeShakeologyRecipes();

        $recipes = $this->getRecipes();
        $recipes = $this->reorderMeals($recipes);
        $alterate_recipes = NULL;

        // Add recipes for alterate meal plan
        if($this->meal_template == 3) {
            // Exclude recipes from even days
            $excluded_recipes = array_column($recipes, 'id');

            foreach ($excluded_recipes as $excluded)
                $this->excluded_recipes->push($excluded);

            // Add recipes for odd days
            $alterate_recipes = $this->getRecipes();
            $alterate_recipes = $this->reorderMeals($alterate_recipes);
        }

        if($this->meal_template == 2) {
            $this->createLeftOver($recipes);
            return $this->json_response('success', 201, [
                'message' => 'Your meal plan was created.'
            ]);
        }

        if($this->date->dayOfWeek == \Carbon\Carbon::SUNDAY)
            $week = 'next';

        $this->addMealPlan($week, $recipes, $alterate_recipes);

        return $this->json_response('success', 201, [
            'message' => 'Your meal plan was created.'
        ]);
    }

    public function getRecipes()
    {
        $lifestyle_ids = $this->lifestyle_ids;
        $shake_meal = $this->user->customization_shake->meal ?? null;
        $shake = $this->user->customization_shake->shake ?? null;

        do {
            $containers = $this->cont;

            $containers_sum = array_sum($containers);
            $collected = [];
            $recipes = [];
            $sum = 0;
            $test = true;
            $rand_key = rand(0, 1);
            $dinner = true;
            $lunch = true;
            $input = array('Breakfast', 'Snack 1');

            // Add random shake recipe for Breakfast or Snack 1
            if($shake_meal) {
                if($shake_meal == 'Breakfast')
                    $rand_key = 1;
                else
                    $rand_key = 0;
            }

            // Add recipe for each meal.
            // Not better solution. Need optimization

            foreach ($this->meals as $key => $meal) {
                if($shake_meal && $shake_meal == $meal && $test) {
                    $test = false;
                    $shake = 0;

                    $recipe = Recipe::inRandomOrder()
                                    ->whereNotIn('id', [133])
                                    // ->whereNotIn('id', $collected)
                                    // ->whereNotIn('id', $this->excluded_recipes)
                                    ->with(['meal_types' => function($q) use ($meal) {
                                        $q->where('name', $meal);
                                    }])
                                    ->whereHas('meal_types', function($q) use ($meal) {
                                        $q->where('name', $meal);
                                    })
                                    ->whereHas('recipe_types', function($q) {
                                        $q->where('name', 'Shakeology');
                                    })
                                    ->where('red_containers', '<=', $containers['red'])
                                    ->where('green_containers', '<=', $containers['green'])
                                    ->where('purple_containers', '<=', $containers['purple'])
                                    ->where('yellow_containers', '<=', $containers['yellow'])
                                    ->where('blue_containers', '<=', $containers['blue'])
                                    ->where('orange_containers', '<=', $containers['orange'])
                                    // ->where('spoon_containers', '<=', $containers['grey'])
                                    ->first();
                } else {
                    $recipe = Recipe::inRandomOrder()
                        // ->whereNotIn('id', $collected)
                        // ->whereNotIn('id', $this->excluded_recipes)
                        ->with([
                            'meal_types'
                        ])
                        ->with(['meal_types' => function($q) use ($meal) {
                            $q->where('name', $meal);
                        }])
                        ->whereHas('meal_types', function($q) use ($meal) {
                            $q->where('name', $meal);
                        })
                        ->whereHas('recipe_types', function($q) use ($shake, $recipes) {
                            if($shake === 0)
                                $q->where('name', '<>', 'Shakeology');
                        })
                        ->whereHas('dietary_lifestyles', function($q) use ($lifestyle_ids) {
                            if(count($lifestyle_ids) > 0) {
                                $q->whereIn('dietary_lifestyle_id', $lifestyle_ids);
                            }
                        })
                        ->where(function($q) use ($containers, $meal, $input, $rand_key, $dinner, $lunch) {
                            if($this->leftOver) {
                                $q->where('red_containers', '=', $containers['red'])
                                  ->where('green_containers', '=', $containers['green'])
                                  ->where('purple_containers', '=', $containers['purple'])
                                  ->where('yellow_containers', '=', $containers['yellow'])
                                  ->where('blue_containers', '=', $containers['blue'])
                                  ->where('orange_containers', '=', $containers['orange']);
                                  // ->where('spoon_containers', '=', $containers['grey']);
                            } elseif(($meal == 'Dinner' && $dinner == true) ||
                                     ($meal == 'Lunch' && $lunch == true)
                             ) {
                                $q->whereBetween('red_containers', [1, $containers['red']])
                                  ->whereBetween('green_containers', [1, $containers['green']])
                                  ->where('purple_containers', '<=', $containers['purple'])
                                  ->where('yellow_containers', '<=', $containers['yellow'])
                                  ->where('blue_containers', '<=', $containers['blue'])
                                  ->where('orange_containers', '<=', $containers['orange']);
                                  // ->where('spoon_containers', '<=', $containers['grey']);
                             } elseif($input[$rand_key] == $meal) {
                                $q->whereBetween('red_containers', [1, $containers['red']])
                                  ->where('green_containers', '<=', $containers['green'])
                                  ->where('purple_containers', '<=', $containers['purple'])
                                  ->where('yellow_containers', '<=', $containers['yellow'])
                                  ->where('blue_containers', '<=', $containers['blue'])
                                  ->where('orange_containers', '<=', $containers['orange']);
                                  // ->where('spoon_containers', '<=', $containers['grey']);
                             } else {
                                $q->where('red_containers', '<=', $containers['red'])
                                  ->where('green_containers', '<=', $containers['green'])
                                  ->where('purple_containers', '<=', $containers['purple'])
                                  ->where('yellow_containers', '<=', $containers['yellow'])
                                  ->where('blue_containers', '<=', $containers['blue'])
                                  ->where('orange_containers', '<=', $containers['orange']);
                                  // ->where('spoon_containers', '<= ', $containers['grey']);
                             }
                        })
                        ->whereHas('recipe_ingredients', function($q) use ($containers) {
                             // $q->where('secondary_recipe', NULL);
                        })
                        ->first();

                        if($meal == 'Dinner')
                            $dinner = false;

                        if($meal == 'Lunch')
                            $lunch = false;

                        if($input[$rand_key] == $meal)
                            $input[$rand_key] = null;
                }

                if($recipe === NULL) {
                    return $this->getRecipes();
                }

                // Check if current recipe is Shakeology do not reapeat it again.
                $shake_exist = $recipe->recipe_types()
                                      ->where('recipe_types.name', 'Shakeology')
                                      ->exists();
                if($shake_exist)
                    $shake = 0;

                array_push($collected, $recipe->id);

                $sum += $recipe->red_containers;
                $sum += $recipe->green_containers;
                $sum += $recipe->purple_containers;
                $sum += $recipe->yellow_containers;
                $sum += $recipe->blue_containers;
                $sum += $recipe->orange_containers;
                // $sum += $recipe->spoon_containers;
                $containers['red'] -= $recipe->red_containers;
                $containers['green'] -= $recipe->green_containers;
                $containers['purple'] -= $recipe->purple_containers;
                $containers['yellow'] -= $recipe->yellow_containers;
                $containers['blue'] -= $recipe->blue_containers;
                $containers['orange'] -= $recipe->orange_containers;
                // $containers['grey'] -= $recipe->spoon_containers;

                if(isset($recipes[$meal]))
                    $recipes[$meal . time() . rand(1000, 9999)][$key] = $recipe;
                else
                    $recipes[$meal][$key] = $recipe;

            }

        } while ($sum != $containers_sum);

        return $recipes;
    }

    public function addMealPlan($week, $recipes, $alterate_recipes = NULL)
    {
        if($week == 'next')
            $this->date->addDays(7);

        $weekStartDate = $this->date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $this->date->endOfWeek()->format('Y-m-d H:i');

        $week = $this->user->weekly_plans()->create(array(
            'user_id'    => $this->user->id,
            'from'       => $weekStartDate,
            'to'         => $weekEndDate,
        ));

        $days = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];

        // Need optimization
        foreach ($days as $key => $day) {
            $daily_plan = $week->daily_plan()->create(array(
                'user_weekly_plan_id' => $week->id,
                'day' => $day,
            ));

            if($alterate_recipes !== NULL && $key % 2 == 0) {
                $current_recipes = $alterate_recipes;
            } else {
                $current_recipes = $recipes;
            }

            foreach ($current_recipes as $meal) {
                foreach ($meal as $recipe) {
                    $daily_plan->meals_plan()->create(array(
                        'user_daily_plan_id' => $daily_plan->id,
                        'meal_type_id' => $recipe->meal_types[0]->id,
                        'recipe_id' => $recipe->id,
                    ));
                }
            }
        }
    }

    public function excludeShakeologyRecipes()
    {
        if(!empty($this->user->customization_shake)) {
            if(!$this->user->customization_shake->shake) {
                $shakeology_recipes = Recipe::inRandomOrder()
                                              ->whereHas('recipe_types', function($q) {
                                                  $q->where('name', 'Shakeology');
                                              })
                                              ->get()
                                              ->toArray();

                $excluded_recipes = array_column($shakeology_recipes, 'id');

                foreach ($excluded_recipes as $excluded)
                    $this->excluded_recipes->push($excluded);
            }
        }
    }

    public function createLeftOver($recipes)
    {
        $this->leftOver = true;
        $days = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];

        $plan = [];
        $nextLunch = [];
        $mondayLunch = [];
        $nextDinner = [];

        // Need optimization
        foreach ($days as $day) {
            foreach ($recipes as $key => $meal) {
                foreach ($meal as $k => $recipe) {
                    $plan[$day][$key][0]['id'] = $recipe->id;
                    $plan[$day][$key][0]['meal_type_id'] = $recipe->meal_types[0]->id;
                    $left = null;

                    if($day == 'Monday' && $key == 'Lunch') {
                        $mondayLunch['id'] = $recipes['Lunch'][0]->id;
                        $mondayLunch['meal_type_id'] = $recipes['Lunch'][0]->meal_types[0]->id;
                    }

                    if($nextLunch && $key == 'Lunch') {
                        $plan[$day]['Lunch'][0] = $nextLunch;
                    }

                    if($nextDinner && $key == 'Dinner') {
                        $plan[$day]['Dinner'][0]['id'] = $nextDinner->id;
                        $plan[$day]['Dinner'][0]['meal_type_id'] = 3;
                    }

                    if($key == 'Dinner') {
                        if(!$nextLunch) {
                            $nextLunch['id'] = $recipe->id;
                            $nextLunch['meal_type_id'] = 6;
                            // $this->excluded_recipes->push($recipe->id);
                        } else {
                            $nextLunch['id'] = $nextDinner->id;
                            $nextLunch['meal_type_id'] = 6;
                        }

                        $this->meals = ['Dinner'];
                        $recipe = Recipe::where('id', $plan[$day]['Lunch'][0]['id'])->first();
                        $this->cont['red'] = $recipe->red_containers;
                        $this->cont['green'] = $recipe->green_containers;
                        $this->cont['purple'] = $recipe->purple_containers;
                        $this->cont['yellow'] = $recipe->yellow_containers;
                        $this->cont['blue'] = $recipe->blue_containers;
                        $this->cont['orange'] = $recipe->orange_containers;

                        $dinner = $this->getRecipes();
                        $nextDinner = $dinner['Dinner'][0];

                        // $this->excluded_recipes->push($nextDinner->id);
                    }

                    if($day == 'Sunday' && $key == 'Dinner') {
                        $plan[$day][$key][0]['id'] = $mondayLunch['id'];
                        $plan[$day][$key][0]['meal_type_id'] = 3;
                    }
                }
            }
        }

        $weekStartDate = $this->date->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = $this->date->endOfWeek()->format('Y-m-d H:i');

        $week = $this->user->weekly_plans()->create(array(
            'user_id'    => $this->user->id,
            'from'       => $weekStartDate,
            'to'         => $weekEndDate,
        ));

        // Need optimization
        foreach ($plan as $key => $day) {
            $daily_plan = $week->daily_plan()->create(array(
                'user_weekly_plan_id' => $week->id,
                'day' => $key,
            ));

            foreach ($day as $meal) {
                foreach ($meal as $recipe) {
                    $daily_plan->meals_plan()->create(array(
                        'user_daily_plan_id' => $daily_plan->id,
                        'meal_type_id' => $recipe['meal_type_id'],
                        'recipe_id' => $recipe['id'],
                    ));
                }
            }
        }

    }

    public function reorderMeals($arr)
    {
        $breakfast = $arr['Breakfast'];
        $snack = $arr['Snack 1'];
        $dinner = $arr['Dinner'];
        $lunch = $arr['Lunch'];

        unset($arr['Dinner']);
        unset($arr['Snack 1']);
        unset($arr['Breakfast']);

        $arr = $arr + array('Dinner' => $dinner);
        $arr = array('Snack 1' => $snack) + $arr;

        $arr = array('Breakfast' => $breakfast) + $arr;

        return $arr;
    }
}
