<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\AppController;
use App\CalorieBracket;

class GetAttributesController extends AppController
{
    public function calories()
    {
        $calories = CalorieBracket::get()->toArray();

        return $this->json_response('success', 200, $calories);
    }
}
