<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\AppController as App;


class IsConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->confirmed) {
            $app = new App();
            return $app->json_response('error', 400, [
                'message' => 'Your account wasn\'t confirmed. Check your e-mail and submit confirmation link or try to send confirmation email again.'
            ]);
        }

        return $next($request);
    }
}
