<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;
use App\Http\Controllers\Api\AppController as App;

class UserActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            $user = \Auth::user();
            $user->last_active = date('Y-m-d H:i:s');
            $user->save();
        }

        return $next($request);
    }
}
