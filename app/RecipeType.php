<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeType extends Model
{
    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'recipe_recipe_type', 'recipe_type_id', 'recipe_id');
    }
}
