<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function measurement()
    {
        return $this->belongsTo('App\Measurement');
    }

    public function recipe_ingredients()
    {
        return $this->hasMany('App\RecipeIngredient');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'recipe_ingredients', 'ingredient_id', 'recipe_id');
    }

}
