<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDailyPlan extends Model
{
    public $timestamps = false;

    protected $table = 'user_daily_plan';

    protected $fillable = [
        'user_weekly_plan_id',
        'day',
    ];

    public function week()
    {
        return $this->belongsTo('App\UserWeeklyPlan', 'user_weekly_plan_id');
    }

    public function meals_plan()
    {
        return $this->hasMany('App\UserMealPlan');
    }
}
