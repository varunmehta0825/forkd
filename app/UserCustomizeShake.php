<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCustomizeShake extends Model
{
    protected $fillable = [
        'shake',
        'meal'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
