<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuisineType extends Model
{
    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'recipe_cuisine_type', 'cuisine_type_id', 'recipe_id');
    }
}
