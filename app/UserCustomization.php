<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCustomization extends Model
{
    protected $table = 'user_meal_customization';

    protected $fillable = [
        'cheat_day',
        'eating_out',
        'travel',
        'shake',
        'alcohol_intake',
        'ingredients'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
