<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preparation extends Model
{
    public function recipe_ingredients()
    {
        return $this->hasMany('App\RecipeIngredient');
    }
}
