<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMealPlan extends Model
{
    public $timestamps = false;

    protected $table = 'user_meals_plan';

    protected $fillable = [
        'user_daily_plan_id',
        'meal_type_id',
        'recipe_id',
    ];

    public function recipes()
    {
        return $this->belongsTo('App\Recipe', 'recipe_id');
    }

    public function meal_type()
    {
        return $this->belongsTo('App\MealType', 'meal_type_id');
    }

    public function daily_plan()
    {
        return $this->belongsTo('App\UserDailyPlan', 'user_daily_plan_id');
    }
}
