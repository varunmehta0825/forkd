<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    public function ingredients()
    {
        return $this->hasMany('App\Ingredient');
    }

    public function recipe_ingredients()
    {
        return $this->hasMany('App\RecipeIngredient');
    }
}
