<?php

use Illuminate\Database\Seeder;

class OauthAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_access_tokens')->delete();
        
        \DB::table('oauth_access_tokens')->insert(array (
            0 => 
            array (
                'id' => 'b2c4f446a1985dc23235409729d06eb35afa4afc58e724b8bf58d142d1e7552b87dfba779c1f7e8c',
                'user_id' => 1,
                'client_id' => 2,
                'name' => NULL,
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2019-02-03 19:43:20',
                'updated_at' => '2019-02-03 19:43:20',
                'expires_at' => '2020-02-03 19:43:20',
            ),
        ));
        
        
    }
}