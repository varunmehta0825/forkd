<?php

use Illuminate\Database\Seeder;

class CalorieBracketsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('calorie_brackets')->delete();
        
        \DB::table('calorie_brackets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'range_calories' => '1200 - 1499',
                'red' => 4,
                'green' => 3,
                'purple' => 2,
                'yellow' => 2,
                'blue' => 1,
                'orange' => 1,
                'grey' => 2,
                'n_red' => 4,
                'n_green' => 4,
                'n_purple' => 3,
                'n_yellow' => 3,
                'n_blue' => 1,
                'n_orange' => 1,
                'n_grey' => 5,
            ),
            1 => 
            array (
                'id' => 2,
                'range_calories' => '1500 - 1799',
                'red' => 4,
                'green' => 4,
                'purple' => 3,
                'yellow' => 3,
                'blue' => 1,
                'orange' => 1,
                'grey' => 4,
                'n_red' => 5,
                'n_green' => 5,
                'n_purple' => 3,
                'n_yellow' => 4,
                'n_blue' => 1,
                'n_orange' => 1,
                'n_grey' => 5,
            ),
            2 => 
            array (
                'id' => 3,
                'range_calories' => '1800 - 2099',
                'red' => 5,
                'green' => 5,
                'purple' => 3,
                'yellow' => 4,
                'blue' => 1,
                'orange' => 1,
                'grey' => 5,
                'n_red' => 5,
                'n_green' => 5,
                'n_purple' => 3,
                'n_yellow' => 4,
                'n_blue' => 1,
                'n_orange' => 1,
                'n_grey' => 5,
            ),
            3 => 
            array (
                'id' => 4,
                'range_calories' => '2100 - 2299',
                'red' => 6,
                'green' => 6,
                'purple' => 4,
                'yellow' => 4,
                'blue' => 1,
                'orange' => 1,
                'grey' => 6,
                'n_red' => 0,
                'n_green' => 5,
                'n_purple' => 0,
                'n_yellow' => 0,
                'n_blue' => 0,
                'n_orange' => 0,
                'n_grey' => 0,
            ),
        ));
        
        
    }
}