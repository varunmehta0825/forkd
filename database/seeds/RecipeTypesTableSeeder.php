<?php

use Illuminate\Database\Seeder;

class RecipeTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_types')->delete();
        
        \DB::table('recipe_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '30 Minutes or Less',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'No Cook',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Travel by Air',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Travel by Car',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Nutrient Dense Shake',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Crock-pot',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Shakeology',
            ),
        ));
        
        
    }
}