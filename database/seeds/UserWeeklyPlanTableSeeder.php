<?php

use Illuminate\Database\Seeder;

class UserWeeklyPlanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_weekly_plan')->delete();
        
        \DB::table('user_weekly_plan')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'from' => '2019-01-21 00:00:00',
                'to' => '2019-01-27 23:59:00',
                'created_at' => '2019-01-25 12:12:39',
                'updated_at' => '2019-01-25 12:12:39',
            ),
        ));
        
        
    }
}