<?php

use Illuminate\Database\Seeder;

class UserMealsPlanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_meals_plan')->delete();
        
        \DB::table('user_meals_plan')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            1 => 
            array (
                'id' => 2,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            2 => 
            array (
                'id' => 3,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            3 => 
            array (
                'id' => 4,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            4 => 
            array (
                'id' => 5,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            5 => 
            array (
                'id' => 6,
                'user_daily_plan_id' => 1,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            6 => 
            array (
                'id' => 7,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            7 => 
            array (
                'id' => 8,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            8 => 
            array (
                'id' => 9,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            9 => 
            array (
                'id' => 10,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            10 => 
            array (
                'id' => 11,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            11 => 
            array (
                'id' => 12,
                'user_daily_plan_id' => 2,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            12 => 
            array (
                'id' => 13,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            13 => 
            array (
                'id' => 14,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            14 => 
            array (
                'id' => 15,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            15 => 
            array (
                'id' => 16,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            16 => 
            array (
                'id' => 17,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            17 => 
            array (
                'id' => 18,
                'user_daily_plan_id' => 3,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            18 => 
            array (
                'id' => 19,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            19 => 
            array (
                'id' => 20,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            20 => 
            array (
                'id' => 21,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            21 => 
            array (
                'id' => 22,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            22 => 
            array (
                'id' => 23,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            23 => 
            array (
                'id' => 24,
                'user_daily_plan_id' => 4,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            24 => 
            array (
                'id' => 25,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            25 => 
            array (
                'id' => 26,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            26 => 
            array (
                'id' => 27,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            27 => 
            array (
                'id' => 28,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            28 => 
            array (
                'id' => 29,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            29 => 
            array (
                'id' => 30,
                'user_daily_plan_id' => 5,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            30 => 
            array (
                'id' => 31,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            31 => 
            array (
                'id' => 32,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            32 => 
            array (
                'id' => 33,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            33 => 
            array (
                'id' => 34,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            34 => 
            array (
                'id' => 35,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            35 => 
            array (
                'id' => 36,
                'user_daily_plan_id' => 6,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
            36 => 
            array (
                'id' => 37,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 1,
                'recipe_id' => 16,
            ),
            37 => 
            array (
                'id' => 38,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 4,
                'recipe_id' => 75,
            ),
            38 => 
            array (
                'id' => 39,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 2,
                'recipe_id' => 136,
            ),
            39 => 
            array (
                'id' => 40,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 2,
                'recipe_id' => 137,
            ),
            40 => 
            array (
                'id' => 41,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 4,
                'recipe_id' => 115,
            ),
            41 => 
            array (
                'id' => 42,
                'user_daily_plan_id' => 7,
                'meal_type_id' => 3,
                'recipe_id' => 124,
            ),
        ));
        
        
    }
}