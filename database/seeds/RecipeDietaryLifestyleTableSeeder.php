<?php

use Illuminate\Database\Seeder;

class RecipeDietaryLifestyleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_dietary_lifestyle')->delete();
        
        \DB::table('recipe_dietary_lifestyle')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'dietary_lifestyle_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 1,
                'dietary_lifestyle_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 1,
                'dietary_lifestyle_id' => 3,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 1,
                'dietary_lifestyle_id' => 4,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 1,
                'dietary_lifestyle_id' => 5,
            ),
            5 => 
            array (
                'id' => 6,
                'recipe_id' => 2,
                'dietary_lifestyle_id' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'recipe_id' => 2,
                'dietary_lifestyle_id' => 2,
            ),
            7 => 
            array (
                'id' => 8,
                'recipe_id' => 2,
                'dietary_lifestyle_id' => 4,
            ),
            8 => 
            array (
                'id' => 9,
                'recipe_id' => 2,
                'dietary_lifestyle_id' => 3,
            ),
            9 => 
            array (
                'id' => 10,
                'recipe_id' => 2,
                'dietary_lifestyle_id' => 5,
            ),
            10 => 
            array (
                'id' => 11,
                'recipe_id' => 3,
                'dietary_lifestyle_id' => 3,
            ),
            11 => 
            array (
                'id' => 12,
                'recipe_id' => 3,
                'dietary_lifestyle_id' => 5,
            ),
            12 => 
            array (
                'id' => 13,
                'recipe_id' => 4,
                'dietary_lifestyle_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'recipe_id' => 4,
                'dietary_lifestyle_id' => 2,
            ),
            14 => 
            array (
                'id' => 15,
                'recipe_id' => 4,
                'dietary_lifestyle_id' => 5,
            ),
            15 => 
            array (
                'id' => 16,
                'recipe_id' => 4,
                'dietary_lifestyle_id' => 3,
            ),
            16 => 
            array (
                'id' => 17,
                'recipe_id' => 4,
                'dietary_lifestyle_id' => 4,
            ),
            17 => 
            array (
                'id' => 18,
                'recipe_id' => 5,
                'dietary_lifestyle_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'recipe_id' => 5,
                'dietary_lifestyle_id' => 3,
            ),
            19 => 
            array (
                'id' => 20,
                'recipe_id' => 5,
                'dietary_lifestyle_id' => 5,
            ),
            20 => 
            array (
                'id' => 21,
                'recipe_id' => 5,
                'dietary_lifestyle_id' => 2,
            ),
            21 => 
            array (
                'id' => 22,
                'recipe_id' => 5,
                'dietary_lifestyle_id' => 4,
            ),
            22 => 
            array (
                'id' => 23,
                'recipe_id' => 6,
                'dietary_lifestyle_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'recipe_id' => 6,
                'dietary_lifestyle_id' => 2,
            ),
            24 => 
            array (
                'id' => 25,
                'recipe_id' => 7,
                'dietary_lifestyle_id' => 2,
            ),
            25 => 
            array (
                'id' => 26,
                'recipe_id' => 7,
                'dietary_lifestyle_id' => 3,
            ),
            26 => 
            array (
                'id' => 27,
                'recipe_id' => 8,
                'dietary_lifestyle_id' => 2,
            ),
            27 => 
            array (
                'id' => 28,
                'recipe_id' => 9,
                'dietary_lifestyle_id' => 2,
            ),
            28 => 
            array (
                'id' => 29,
                'recipe_id' => 9,
                'dietary_lifestyle_id' => 3,
            ),
            29 => 
            array (
                'id' => 30,
                'recipe_id' => 9,
                'dietary_lifestyle_id' => 5,
            ),
            30 => 
            array (
                'id' => 31,
                'recipe_id' => 10,
                'dietary_lifestyle_id' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'recipe_id' => 10,
                'dietary_lifestyle_id' => 2,
            ),
            32 => 
            array (
                'id' => 33,
                'recipe_id' => 10,
                'dietary_lifestyle_id' => 3,
            ),
            33 => 
            array (
                'id' => 34,
                'recipe_id' => 10,
                'dietary_lifestyle_id' => 4,
            ),
            34 => 
            array (
                'id' => 35,
                'recipe_id' => 10,
                'dietary_lifestyle_id' => 5,
            ),
            35 => 
            array (
                'id' => 36,
                'recipe_id' => 11,
                'dietary_lifestyle_id' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'recipe_id' => 11,
                'dietary_lifestyle_id' => 3,
            ),
            37 => 
            array (
                'id' => 38,
                'recipe_id' => 11,
                'dietary_lifestyle_id' => 4,
            ),
            38 => 
            array (
                'id' => 39,
                'recipe_id' => 11,
                'dietary_lifestyle_id' => 5,
            ),
            39 => 
            array (
                'id' => 40,
                'recipe_id' => 12,
                'dietary_lifestyle_id' => 5,
            ),
            40 => 
            array (
                'id' => 41,
                'recipe_id' => 12,
                'dietary_lifestyle_id' => 3,
            ),
            41 => 
            array (
                'id' => 42,
                'recipe_id' => 13,
                'dietary_lifestyle_id' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'recipe_id' => 13,
                'dietary_lifestyle_id' => 2,
            ),
            43 => 
            array (
                'id' => 44,
                'recipe_id' => 13,
                'dietary_lifestyle_id' => 3,
            ),
            44 => 
            array (
                'id' => 45,
                'recipe_id' => 13,
                'dietary_lifestyle_id' => 4,
            ),
            45 => 
            array (
                'id' => 46,
                'recipe_id' => 13,
                'dietary_lifestyle_id' => 5,
            ),
            46 => 
            array (
                'id' => 47,
                'recipe_id' => 14,
                'dietary_lifestyle_id' => 2,
            ),
            47 => 
            array (
                'id' => 48,
                'recipe_id' => 14,
                'dietary_lifestyle_id' => 3,
            ),
            48 => 
            array (
                'id' => 49,
                'recipe_id' => 14,
                'dietary_lifestyle_id' => 4,
            ),
            49 => 
            array (
                'id' => 50,
                'recipe_id' => 14,
                'dietary_lifestyle_id' => 5,
            ),
            50 => 
            array (
                'id' => 51,
                'recipe_id' => 15,
                'dietary_lifestyle_id' => 2,
            ),
            51 => 
            array (
                'id' => 52,
                'recipe_id' => 15,
                'dietary_lifestyle_id' => 3,
            ),
            52 => 
            array (
                'id' => 53,
                'recipe_id' => 15,
                'dietary_lifestyle_id' => 4,
            ),
            53 => 
            array (
                'id' => 54,
                'recipe_id' => 15,
                'dietary_lifestyle_id' => 5,
            ),
            54 => 
            array (
                'id' => 55,
                'recipe_id' => 16,
                'dietary_lifestyle_id' => 1,
            ),
            55 => 
            array (
                'id' => 56,
                'recipe_id' => 16,
                'dietary_lifestyle_id' => 2,
            ),
            56 => 
            array (
                'id' => 57,
                'recipe_id' => 16,
                'dietary_lifestyle_id' => 3,
            ),
            57 => 
            array (
                'id' => 58,
                'recipe_id' => 17,
                'dietary_lifestyle_id' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'recipe_id' => 17,
                'dietary_lifestyle_id' => 2,
            ),
            59 => 
            array (
                'id' => 60,
                'recipe_id' => 17,
                'dietary_lifestyle_id' => 3,
            ),
            60 => 
            array (
                'id' => 61,
                'recipe_id' => 17,
                'dietary_lifestyle_id' => 4,
            ),
            61 => 
            array (
                'id' => 62,
                'recipe_id' => 17,
                'dietary_lifestyle_id' => 5,
            ),
            62 => 
            array (
                'id' => 63,
                'recipe_id' => 18,
                'dietary_lifestyle_id' => 2,
            ),
            63 => 
            array (
                'id' => 64,
                'recipe_id' => 18,
                'dietary_lifestyle_id' => 3,
            ),
            64 => 
            array (
                'id' => 65,
                'recipe_id' => 18,
                'dietary_lifestyle_id' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'recipe_id' => 19,
                'dietary_lifestyle_id' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'recipe_id' => 19,
                'dietary_lifestyle_id' => 2,
            ),
            67 => 
            array (
                'id' => 68,
                'recipe_id' => 19,
                'dietary_lifestyle_id' => 3,
            ),
            68 => 
            array (
                'id' => 69,
                'recipe_id' => 19,
                'dietary_lifestyle_id' => 4,
            ),
            69 => 
            array (
                'id' => 70,
                'recipe_id' => 19,
                'dietary_lifestyle_id' => 5,
            ),
            70 => 
            array (
                'id' => 71,
                'recipe_id' => 20,
                'dietary_lifestyle_id' => 1,
            ),
            71 => 
            array (
                'id' => 72,
                'recipe_id' => 20,
                'dietary_lifestyle_id' => 2,
            ),
            72 => 
            array (
                'id' => 73,
                'recipe_id' => 20,
                'dietary_lifestyle_id' => 3,
            ),
            73 => 
            array (
                'id' => 74,
                'recipe_id' => 20,
                'dietary_lifestyle_id' => 4,
            ),
            74 => 
            array (
                'id' => 75,
                'recipe_id' => 20,
                'dietary_lifestyle_id' => 5,
            ),
            75 => 
            array (
                'id' => 76,
                'recipe_id' => 21,
                'dietary_lifestyle_id' => 2,
            ),
            76 => 
            array (
                'id' => 77,
                'recipe_id' => 21,
                'dietary_lifestyle_id' => 5,
            ),
            77 => 
            array (
                'id' => 78,
                'recipe_id' => 21,
                'dietary_lifestyle_id' => 3,
            ),
            78 => 
            array (
                'id' => 79,
                'recipe_id' => 22,
                'dietary_lifestyle_id' => 2,
            ),
            79 => 
            array (
                'id' => 80,
                'recipe_id' => 22,
                'dietary_lifestyle_id' => 3,
            ),
            80 => 
            array (
                'id' => 81,
                'recipe_id' => 22,
                'dietary_lifestyle_id' => 5,
            ),
            81 => 
            array (
                'id' => 82,
                'recipe_id' => 23,
                'dietary_lifestyle_id' => 1,
            ),
            82 => 
            array (
                'id' => 83,
                'recipe_id' => 23,
                'dietary_lifestyle_id' => 2,
            ),
            83 => 
            array (
                'id' => 84,
                'recipe_id' => 23,
                'dietary_lifestyle_id' => 3,
            ),
            84 => 
            array (
                'id' => 85,
                'recipe_id' => 24,
                'dietary_lifestyle_id' => 1,
            ),
            85 => 
            array (
                'id' => 86,
                'recipe_id' => 24,
                'dietary_lifestyle_id' => 2,
            ),
            86 => 
            array (
                'id' => 87,
                'recipe_id' => 24,
                'dietary_lifestyle_id' => 3,
            ),
            87 => 
            array (
                'id' => 88,
                'recipe_id' => 25,
                'dietary_lifestyle_id' => 1,
            ),
            88 => 
            array (
                'id' => 89,
                'recipe_id' => 25,
                'dietary_lifestyle_id' => 2,
            ),
            89 => 
            array (
                'id' => 90,
                'recipe_id' => 25,
                'dietary_lifestyle_id' => 3,
            ),
            90 => 
            array (
                'id' => 91,
                'recipe_id' => 25,
                'dietary_lifestyle_id' => 4,
            ),
            91 => 
            array (
                'id' => 92,
                'recipe_id' => 25,
                'dietary_lifestyle_id' => 5,
            ),
            92 => 
            array (
                'id' => 93,
                'recipe_id' => 26,
                'dietary_lifestyle_id' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'recipe_id' => 26,
                'dietary_lifestyle_id' => 2,
            ),
            94 => 
            array (
                'id' => 95,
                'recipe_id' => 26,
                'dietary_lifestyle_id' => 3,
            ),
            95 => 
            array (
                'id' => 96,
                'recipe_id' => 26,
                'dietary_lifestyle_id' => 5,
            ),
            96 => 
            array (
                'id' => 97,
                'recipe_id' => 26,
                'dietary_lifestyle_id' => 4,
            ),
            97 => 
            array (
                'id' => 98,
                'recipe_id' => 27,
                'dietary_lifestyle_id' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'recipe_id' => 27,
                'dietary_lifestyle_id' => 2,
            ),
            99 => 
            array (
                'id' => 100,
                'recipe_id' => 27,
                'dietary_lifestyle_id' => 3,
            ),
            100 => 
            array (
                'id' => 101,
                'recipe_id' => 27,
                'dietary_lifestyle_id' => 4,
            ),
            101 => 
            array (
                'id' => 102,
                'recipe_id' => 27,
                'dietary_lifestyle_id' => 5,
            ),
            102 => 
            array (
                'id' => 103,
                'recipe_id' => 28,
                'dietary_lifestyle_id' => 2,
            ),
            103 => 
            array (
                'id' => 104,
                'recipe_id' => 28,
                'dietary_lifestyle_id' => 5,
            ),
            104 => 
            array (
                'id' => 105,
                'recipe_id' => 28,
                'dietary_lifestyle_id' => 3,
            ),
            105 => 
            array (
                'id' => 106,
                'recipe_id' => 29,
                'dietary_lifestyle_id' => 1,
            ),
            106 => 
            array (
                'id' => 107,
                'recipe_id' => 29,
                'dietary_lifestyle_id' => 2,
            ),
            107 => 
            array (
                'id' => 108,
                'recipe_id' => 29,
                'dietary_lifestyle_id' => 3,
            ),
            108 => 
            array (
                'id' => 109,
                'recipe_id' => 29,
                'dietary_lifestyle_id' => 4,
            ),
            109 => 
            array (
                'id' => 110,
                'recipe_id' => 29,
                'dietary_lifestyle_id' => 5,
            ),
            110 => 
            array (
                'id' => 111,
                'recipe_id' => 30,
                'dietary_lifestyle_id' => 1,
            ),
            111 => 
            array (
                'id' => 112,
                'recipe_id' => 30,
                'dietary_lifestyle_id' => 2,
            ),
            112 => 
            array (
                'id' => 113,
                'recipe_id' => 31,
                'dietary_lifestyle_id' => 1,
            ),
            113 => 
            array (
                'id' => 114,
                'recipe_id' => 31,
                'dietary_lifestyle_id' => 2,
            ),
            114 => 
            array (
                'id' => 115,
                'recipe_id' => 31,
                'dietary_lifestyle_id' => 3,
            ),
            115 => 
            array (
                'id' => 116,
                'recipe_id' => 31,
                'dietary_lifestyle_id' => 4,
            ),
            116 => 
            array (
                'id' => 117,
                'recipe_id' => 31,
                'dietary_lifestyle_id' => 5,
            ),
            117 => 
            array (
                'id' => 118,
                'recipe_id' => 32,
                'dietary_lifestyle_id' => 1,
            ),
            118 => 
            array (
                'id' => 119,
                'recipe_id' => 32,
                'dietary_lifestyle_id' => 3,
            ),
            119 => 
            array (
                'id' => 120,
                'recipe_id' => 32,
                'dietary_lifestyle_id' => 2,
            ),
            120 => 
            array (
                'id' => 121,
                'recipe_id' => 32,
                'dietary_lifestyle_id' => 5,
            ),
            121 => 
            array (
                'id' => 122,
                'recipe_id' => 32,
                'dietary_lifestyle_id' => 4,
            ),
            122 => 
            array (
                'id' => 123,
                'recipe_id' => 33,
                'dietary_lifestyle_id' => 2,
            ),
            123 => 
            array (
                'id' => 124,
                'recipe_id' => 34,
                'dietary_lifestyle_id' => 1,
            ),
            124 => 
            array (
                'id' => 125,
                'recipe_id' => 34,
                'dietary_lifestyle_id' => 2,
            ),
            125 => 
            array (
                'id' => 126,
                'recipe_id' => 34,
                'dietary_lifestyle_id' => 3,
            ),
            126 => 
            array (
                'id' => 127,
                'recipe_id' => 34,
                'dietary_lifestyle_id' => 4,
            ),
            127 => 
            array (
                'id' => 128,
                'recipe_id' => 34,
                'dietary_lifestyle_id' => 5,
            ),
            128 => 
            array (
                'id' => 129,
                'recipe_id' => 35,
                'dietary_lifestyle_id' => 1,
            ),
            129 => 
            array (
                'id' => 130,
                'recipe_id' => 35,
                'dietary_lifestyle_id' => 2,
            ),
            130 => 
            array (
                'id' => 131,
                'recipe_id' => 35,
                'dietary_lifestyle_id' => 3,
            ),
            131 => 
            array (
                'id' => 132,
                'recipe_id' => 35,
                'dietary_lifestyle_id' => 4,
            ),
            132 => 
            array (
                'id' => 133,
                'recipe_id' => 35,
                'dietary_lifestyle_id' => 5,
            ),
            133 => 
            array (
                'id' => 134,
                'recipe_id' => 36,
                'dietary_lifestyle_id' => 2,
            ),
            134 => 
            array (
                'id' => 135,
                'recipe_id' => 36,
                'dietary_lifestyle_id' => 3,
            ),
            135 => 
            array (
                'id' => 136,
                'recipe_id' => 36,
                'dietary_lifestyle_id' => 5,
            ),
            136 => 
            array (
                'id' => 137,
                'recipe_id' => 37,
                'dietary_lifestyle_id' => 2,
            ),
            137 => 
            array (
                'id' => 138,
                'recipe_id' => 37,
                'dietary_lifestyle_id' => 5,
            ),
            138 => 
            array (
                'id' => 139,
                'recipe_id' => 37,
                'dietary_lifestyle_id' => 3,
            ),
            139 => 
            array (
                'id' => 140,
                'recipe_id' => 38,
                'dietary_lifestyle_id' => 5,
            ),
            140 => 
            array (
                'id' => 141,
                'recipe_id' => 38,
                'dietary_lifestyle_id' => 3,
            ),
            141 => 
            array (
                'id' => 142,
                'recipe_id' => 39,
                'dietary_lifestyle_id' => 2,
            ),
            142 => 
            array (
                'id' => 143,
                'recipe_id' => 40,
                'dietary_lifestyle_id' => 1,
            ),
            143 => 
            array (
                'id' => 144,
                'recipe_id' => 40,
                'dietary_lifestyle_id' => 2,
            ),
            144 => 
            array (
                'id' => 145,
                'recipe_id' => 41,
                'dietary_lifestyle_id' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'recipe_id' => 41,
                'dietary_lifestyle_id' => 2,
            ),
            146 => 
            array (
                'id' => 147,
                'recipe_id' => 42,
                'dietary_lifestyle_id' => 1,
            ),
            147 => 
            array (
                'id' => 148,
                'recipe_id' => 42,
                'dietary_lifestyle_id' => 2,
            ),
            148 => 
            array (
                'id' => 149,
                'recipe_id' => 42,
                'dietary_lifestyle_id' => 3,
            ),
            149 => 
            array (
                'id' => 150,
                'recipe_id' => 42,
                'dietary_lifestyle_id' => 4,
            ),
            150 => 
            array (
                'id' => 151,
                'recipe_id' => 42,
                'dietary_lifestyle_id' => 5,
            ),
            151 => 
            array (
                'id' => 152,
                'recipe_id' => 43,
                'dietary_lifestyle_id' => 2,
            ),
            152 => 
            array (
                'id' => 153,
                'recipe_id' => 43,
                'dietary_lifestyle_id' => 3,
            ),
            153 => 
            array (
                'id' => 154,
                'recipe_id' => 44,
                'dietary_lifestyle_id' => 1,
            ),
            154 => 
            array (
                'id' => 155,
                'recipe_id' => 44,
                'dietary_lifestyle_id' => 2,
            ),
            155 => 
            array (
                'id' => 156,
                'recipe_id' => 44,
                'dietary_lifestyle_id' => 3,
            ),
            156 => 
            array (
                'id' => 157,
                'recipe_id' => 45,
                'dietary_lifestyle_id' => 1,
            ),
            157 => 
            array (
                'id' => 158,
                'recipe_id' => 45,
                'dietary_lifestyle_id' => 2,
            ),
            158 => 
            array (
                'id' => 159,
                'recipe_id' => 45,
                'dietary_lifestyle_id' => 3,
            ),
            159 => 
            array (
                'id' => 160,
                'recipe_id' => 46,
                'dietary_lifestyle_id' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'recipe_id' => 46,
                'dietary_lifestyle_id' => 2,
            ),
            161 => 
            array (
                'id' => 162,
                'recipe_id' => 47,
                'dietary_lifestyle_id' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'recipe_id' => 47,
                'dietary_lifestyle_id' => 2,
            ),
            163 => 
            array (
                'id' => 164,
                'recipe_id' => 47,
                'dietary_lifestyle_id' => 3,
            ),
            164 => 
            array (
                'id' => 165,
                'recipe_id' => 47,
                'dietary_lifestyle_id' => 4,
            ),
            165 => 
            array (
                'id' => 166,
                'recipe_id' => 47,
                'dietary_lifestyle_id' => 5,
            ),
            166 => 
            array (
                'id' => 167,
                'recipe_id' => 48,
                'dietary_lifestyle_id' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'recipe_id' => 48,
                'dietary_lifestyle_id' => 2,
            ),
            168 => 
            array (
                'id' => 169,
                'recipe_id' => 48,
                'dietary_lifestyle_id' => 3,
            ),
            169 => 
            array (
                'id' => 170,
                'recipe_id' => 48,
                'dietary_lifestyle_id' => 4,
            ),
            170 => 
            array (
                'id' => 171,
                'recipe_id' => 48,
                'dietary_lifestyle_id' => 5,
            ),
            171 => 
            array (
                'id' => 172,
                'recipe_id' => 49,
                'dietary_lifestyle_id' => 1,
            ),
            172 => 
            array (
                'id' => 173,
                'recipe_id' => 49,
                'dietary_lifestyle_id' => 2,
            ),
            173 => 
            array (
                'id' => 174,
                'recipe_id' => 49,
                'dietary_lifestyle_id' => 3,
            ),
            174 => 
            array (
                'id' => 175,
                'recipe_id' => 49,
                'dietary_lifestyle_id' => 4,
            ),
            175 => 
            array (
                'id' => 176,
                'recipe_id' => 49,
                'dietary_lifestyle_id' => 5,
            ),
            176 => 
            array (
                'id' => 177,
                'recipe_id' => 50,
                'dietary_lifestyle_id' => 2,
            ),
            177 => 
            array (
                'id' => 178,
                'recipe_id' => 50,
                'dietary_lifestyle_id' => 5,
            ),
            178 => 
            array (
                'id' => 179,
                'recipe_id' => 50,
                'dietary_lifestyle_id' => 3,
            ),
            179 => 
            array (
                'id' => 180,
                'recipe_id' => 50,
                'dietary_lifestyle_id' => 4,
            ),
            180 => 
            array (
                'id' => 181,
                'recipe_id' => 50,
                'dietary_lifestyle_id' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'recipe_id' => 51,
                'dietary_lifestyle_id' => 2,
            ),
            182 => 
            array (
                'id' => 183,
                'recipe_id' => 51,
                'dietary_lifestyle_id' => 5,
            ),
            183 => 
            array (
                'id' => 184,
                'recipe_id' => 51,
                'dietary_lifestyle_id' => 3,
            ),
            184 => 
            array (
                'id' => 185,
                'recipe_id' => 52,
                'dietary_lifestyle_id' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'recipe_id' => 52,
                'dietary_lifestyle_id' => 2,
            ),
            186 => 
            array (
                'id' => 187,
                'recipe_id' => 52,
                'dietary_lifestyle_id' => 3,
            ),
            187 => 
            array (
                'id' => 188,
                'recipe_id' => 52,
                'dietary_lifestyle_id' => 4,
            ),
            188 => 
            array (
                'id' => 189,
                'recipe_id' => 52,
                'dietary_lifestyle_id' => 5,
            ),
            189 => 
            array (
                'id' => 190,
                'recipe_id' => 53,
                'dietary_lifestyle_id' => 1,
            ),
            190 => 
            array (
                'id' => 191,
                'recipe_id' => 53,
                'dietary_lifestyle_id' => 2,
            ),
            191 => 
            array (
                'id' => 192,
                'recipe_id' => 53,
                'dietary_lifestyle_id' => 3,
            ),
            192 => 
            array (
                'id' => 193,
                'recipe_id' => 53,
                'dietary_lifestyle_id' => 4,
            ),
            193 => 
            array (
                'id' => 194,
                'recipe_id' => 53,
                'dietary_lifestyle_id' => 5,
            ),
            194 => 
            array (
                'id' => 195,
                'recipe_id' => 54,
                'dietary_lifestyle_id' => 1,
            ),
            195 => 
            array (
                'id' => 196,
                'recipe_id' => 54,
                'dietary_lifestyle_id' => 2,
            ),
            196 => 
            array (
                'id' => 197,
                'recipe_id' => 54,
                'dietary_lifestyle_id' => 3,
            ),
            197 => 
            array (
                'id' => 198,
                'recipe_id' => 54,
                'dietary_lifestyle_id' => 4,
            ),
            198 => 
            array (
                'id' => 199,
                'recipe_id' => 54,
                'dietary_lifestyle_id' => 5,
            ),
            199 => 
            array (
                'id' => 200,
                'recipe_id' => 55,
                'dietary_lifestyle_id' => 1,
            ),
            200 => 
            array (
                'id' => 201,
                'recipe_id' => 55,
                'dietary_lifestyle_id' => 5,
            ),
            201 => 
            array (
                'id' => 202,
                'recipe_id' => 55,
                'dietary_lifestyle_id' => 2,
            ),
            202 => 
            array (
                'id' => 203,
                'recipe_id' => 55,
                'dietary_lifestyle_id' => 3,
            ),
            203 => 
            array (
                'id' => 204,
                'recipe_id' => 55,
                'dietary_lifestyle_id' => 4,
            ),
            204 => 
            array (
                'id' => 205,
                'recipe_id' => 56,
                'dietary_lifestyle_id' => 1,
            ),
            205 => 
            array (
                'id' => 206,
                'recipe_id' => 56,
                'dietary_lifestyle_id' => 2,
            ),
            206 => 
            array (
                'id' => 207,
                'recipe_id' => 56,
                'dietary_lifestyle_id' => 3,
            ),
            207 => 
            array (
                'id' => 208,
                'recipe_id' => 56,
                'dietary_lifestyle_id' => 4,
            ),
            208 => 
            array (
                'id' => 209,
                'recipe_id' => 56,
                'dietary_lifestyle_id' => 5,
            ),
            209 => 
            array (
                'id' => 210,
                'recipe_id' => 57,
                'dietary_lifestyle_id' => 1,
            ),
            210 => 
            array (
                'id' => 211,
                'recipe_id' => 57,
                'dietary_lifestyle_id' => 2,
            ),
            211 => 
            array (
                'id' => 212,
                'recipe_id' => 57,
                'dietary_lifestyle_id' => 3,
            ),
            212 => 
            array (
                'id' => 213,
                'recipe_id' => 57,
                'dietary_lifestyle_id' => 4,
            ),
            213 => 
            array (
                'id' => 214,
                'recipe_id' => 57,
                'dietary_lifestyle_id' => 5,
            ),
            214 => 
            array (
                'id' => 215,
                'recipe_id' => 58,
                'dietary_lifestyle_id' => 1,
            ),
            215 => 
            array (
                'id' => 216,
                'recipe_id' => 58,
                'dietary_lifestyle_id' => 2,
            ),
            216 => 
            array (
                'id' => 217,
                'recipe_id' => 58,
                'dietary_lifestyle_id' => 3,
            ),
            217 => 
            array (
                'id' => 218,
                'recipe_id' => 58,
                'dietary_lifestyle_id' => 5,
            ),
            218 => 
            array (
                'id' => 219,
                'recipe_id' => 58,
                'dietary_lifestyle_id' => 4,
            ),
            219 => 
            array (
                'id' => 220,
                'recipe_id' => 59,
                'dietary_lifestyle_id' => 1,
            ),
            220 => 
            array (
                'id' => 221,
                'recipe_id' => 59,
                'dietary_lifestyle_id' => 2,
            ),
            221 => 
            array (
                'id' => 222,
                'recipe_id' => 59,
                'dietary_lifestyle_id' => 3,
            ),
            222 => 
            array (
                'id' => 223,
                'recipe_id' => 59,
                'dietary_lifestyle_id' => 4,
            ),
            223 => 
            array (
                'id' => 224,
                'recipe_id' => 59,
                'dietary_lifestyle_id' => 5,
            ),
            224 => 
            array (
                'id' => 225,
                'recipe_id' => 60,
                'dietary_lifestyle_id' => 1,
            ),
            225 => 
            array (
                'id' => 226,
                'recipe_id' => 60,
                'dietary_lifestyle_id' => 2,
            ),
            226 => 
            array (
                'id' => 227,
                'recipe_id' => 60,
                'dietary_lifestyle_id' => 3,
            ),
            227 => 
            array (
                'id' => 228,
                'recipe_id' => 60,
                'dietary_lifestyle_id' => 4,
            ),
            228 => 
            array (
                'id' => 229,
                'recipe_id' => 60,
                'dietary_lifestyle_id' => 5,
            ),
            229 => 
            array (
                'id' => 230,
                'recipe_id' => 61,
                'dietary_lifestyle_id' => 1,
            ),
            230 => 
            array (
                'id' => 231,
                'recipe_id' => 61,
                'dietary_lifestyle_id' => 2,
            ),
            231 => 
            array (
                'id' => 232,
                'recipe_id' => 61,
                'dietary_lifestyle_id' => 3,
            ),
            232 => 
            array (
                'id' => 233,
                'recipe_id' => 61,
                'dietary_lifestyle_id' => 4,
            ),
            233 => 
            array (
                'id' => 234,
                'recipe_id' => 61,
                'dietary_lifestyle_id' => 5,
            ),
            234 => 
            array (
                'id' => 235,
                'recipe_id' => 62,
                'dietary_lifestyle_id' => 1,
            ),
            235 => 
            array (
                'id' => 236,
                'recipe_id' => 62,
                'dietary_lifestyle_id' => 2,
            ),
            236 => 
            array (
                'id' => 237,
                'recipe_id' => 62,
                'dietary_lifestyle_id' => 3,
            ),
            237 => 
            array (
                'id' => 238,
                'recipe_id' => 62,
                'dietary_lifestyle_id' => 4,
            ),
            238 => 
            array (
                'id' => 239,
                'recipe_id' => 62,
                'dietary_lifestyle_id' => 5,
            ),
            239 => 
            array (
                'id' => 240,
                'recipe_id' => 63,
                'dietary_lifestyle_id' => 3,
            ),
            240 => 
            array (
                'id' => 241,
                'recipe_id' => 63,
                'dietary_lifestyle_id' => 5,
            ),
            241 => 
            array (
                'id' => 242,
                'recipe_id' => 63,
                'dietary_lifestyle_id' => 2,
            ),
            242 => 
            array (
                'id' => 243,
                'recipe_id' => 65,
                'dietary_lifestyle_id' => 1,
            ),
            243 => 
            array (
                'id' => 244,
                'recipe_id' => 65,
                'dietary_lifestyle_id' => 2,
            ),
            244 => 
            array (
                'id' => 245,
                'recipe_id' => 65,
                'dietary_lifestyle_id' => 3,
            ),
            245 => 
            array (
                'id' => 246,
                'recipe_id' => 65,
                'dietary_lifestyle_id' => 4,
            ),
            246 => 
            array (
                'id' => 247,
                'recipe_id' => 65,
                'dietary_lifestyle_id' => 5,
            ),
            247 => 
            array (
                'id' => 248,
                'recipe_id' => 66,
                'dietary_lifestyle_id' => 1,
            ),
            248 => 
            array (
                'id' => 249,
                'recipe_id' => 66,
                'dietary_lifestyle_id' => 2,
            ),
            249 => 
            array (
                'id' => 250,
                'recipe_id' => 67,
                'dietary_lifestyle_id' => 1,
            ),
            250 => 
            array (
                'id' => 251,
                'recipe_id' => 67,
                'dietary_lifestyle_id' => 2,
            ),
            251 => 
            array (
                'id' => 252,
                'recipe_id' => 67,
                'dietary_lifestyle_id' => 3,
            ),
            252 => 
            array (
                'id' => 253,
                'recipe_id' => 67,
                'dietary_lifestyle_id' => 4,
            ),
            253 => 
            array (
                'id' => 254,
                'recipe_id' => 67,
                'dietary_lifestyle_id' => 5,
            ),
            254 => 
            array (
                'id' => 255,
                'recipe_id' => 68,
                'dietary_lifestyle_id' => 1,
            ),
            255 => 
            array (
                'id' => 256,
                'recipe_id' => 68,
                'dietary_lifestyle_id' => 2,
            ),
            256 => 
            array (
                'id' => 257,
                'recipe_id' => 68,
                'dietary_lifestyle_id' => 3,
            ),
            257 => 
            array (
                'id' => 258,
                'recipe_id' => 68,
                'dietary_lifestyle_id' => 4,
            ),
            258 => 
            array (
                'id' => 259,
                'recipe_id' => 68,
                'dietary_lifestyle_id' => 5,
            ),
            259 => 
            array (
                'id' => 260,
                'recipe_id' => 69,
                'dietary_lifestyle_id' => 1,
            ),
            260 => 
            array (
                'id' => 261,
                'recipe_id' => 69,
                'dietary_lifestyle_id' => 2,
            ),
            261 => 
            array (
                'id' => 262,
                'recipe_id' => 69,
                'dietary_lifestyle_id' => 3,
            ),
            262 => 
            array (
                'id' => 263,
                'recipe_id' => 69,
                'dietary_lifestyle_id' => 4,
            ),
            263 => 
            array (
                'id' => 264,
                'recipe_id' => 69,
                'dietary_lifestyle_id' => 5,
            ),
            264 => 
            array (
                'id' => 265,
                'recipe_id' => 71,
                'dietary_lifestyle_id' => 2,
            ),
            265 => 
            array (
                'id' => 266,
                'recipe_id' => 71,
                'dietary_lifestyle_id' => 1,
            ),
            266 => 
            array (
                'id' => 267,
                'recipe_id' => 72,
                'dietary_lifestyle_id' => 1,
            ),
            267 => 
            array (
                'id' => 268,
                'recipe_id' => 72,
                'dietary_lifestyle_id' => 2,
            ),
            268 => 
            array (
                'id' => 269,
                'recipe_id' => 72,
                'dietary_lifestyle_id' => 3,
            ),
            269 => 
            array (
                'id' => 270,
                'recipe_id' => 72,
                'dietary_lifestyle_id' => 5,
            ),
            270 => 
            array (
                'id' => 271,
                'recipe_id' => 72,
                'dietary_lifestyle_id' => 4,
            ),
            271 => 
            array (
                'id' => 272,
                'recipe_id' => 73,
                'dietary_lifestyle_id' => 1,
            ),
            272 => 
            array (
                'id' => 273,
                'recipe_id' => 73,
                'dietary_lifestyle_id' => 2,
            ),
            273 => 
            array (
                'id' => 274,
                'recipe_id' => 73,
                'dietary_lifestyle_id' => 3,
            ),
            274 => 
            array (
                'id' => 275,
                'recipe_id' => 73,
                'dietary_lifestyle_id' => 4,
            ),
            275 => 
            array (
                'id' => 276,
                'recipe_id' => 73,
                'dietary_lifestyle_id' => 5,
            ),
            276 => 
            array (
                'id' => 277,
                'recipe_id' => 74,
                'dietary_lifestyle_id' => 1,
            ),
            277 => 
            array (
                'id' => 278,
                'recipe_id' => 74,
                'dietary_lifestyle_id' => 2,
            ),
            278 => 
            array (
                'id' => 279,
                'recipe_id' => 75,
                'dietary_lifestyle_id' => 1,
            ),
            279 => 
            array (
                'id' => 280,
                'recipe_id' => 75,
                'dietary_lifestyle_id' => 2,
            ),
            280 => 
            array (
                'id' => 281,
                'recipe_id' => 76,
                'dietary_lifestyle_id' => 1,
            ),
            281 => 
            array (
                'id' => 282,
                'recipe_id' => 76,
                'dietary_lifestyle_id' => 2,
            ),
            282 => 
            array (
                'id' => 283,
                'recipe_id' => 76,
                'dietary_lifestyle_id' => 3,
            ),
            283 => 
            array (
                'id' => 284,
                'recipe_id' => 76,
                'dietary_lifestyle_id' => 4,
            ),
            284 => 
            array (
                'id' => 285,
                'recipe_id' => 76,
                'dietary_lifestyle_id' => 5,
            ),
            285 => 
            array (
                'id' => 286,
                'recipe_id' => 77,
                'dietary_lifestyle_id' => 2,
            ),
            286 => 
            array (
                'id' => 287,
                'recipe_id' => 77,
                'dietary_lifestyle_id' => 5,
            ),
            287 => 
            array (
                'id' => 288,
                'recipe_id' => 77,
                'dietary_lifestyle_id' => 3,
            ),
            288 => 
            array (
                'id' => 289,
                'recipe_id' => 78,
                'dietary_lifestyle_id' => 2,
            ),
            289 => 
            array (
                'id' => 290,
                'recipe_id' => 78,
                'dietary_lifestyle_id' => 3,
            ),
            290 => 
            array (
                'id' => 291,
                'recipe_id' => 78,
                'dietary_lifestyle_id' => 5,
            ),
            291 => 
            array (
                'id' => 292,
                'recipe_id' => 79,
                'dietary_lifestyle_id' => 2,
            ),
            292 => 
            array (
                'id' => 293,
                'recipe_id' => 79,
                'dietary_lifestyle_id' => 1,
            ),
            293 => 
            array (
                'id' => 294,
                'recipe_id' => 80,
                'dietary_lifestyle_id' => 3,
            ),
            294 => 
            array (
                'id' => 295,
                'recipe_id' => 80,
                'dietary_lifestyle_id' => 5,
            ),
            295 => 
            array (
                'id' => 296,
                'recipe_id' => 81,
                'dietary_lifestyle_id' => 3,
            ),
            296 => 
            array (
                'id' => 297,
                'recipe_id' => 81,
                'dietary_lifestyle_id' => 5,
            ),
            297 => 
            array (
                'id' => 298,
                'recipe_id' => 82,
                'dietary_lifestyle_id' => 2,
            ),
            298 => 
            array (
                'id' => 299,
                'recipe_id' => 82,
                'dietary_lifestyle_id' => 3,
            ),
            299 => 
            array (
                'id' => 300,
                'recipe_id' => 83,
                'dietary_lifestyle_id' => 2,
            ),
            300 => 
            array (
                'id' => 301,
                'recipe_id' => 83,
                'dietary_lifestyle_id' => 3,
            ),
            301 => 
            array (
                'id' => 302,
                'recipe_id' => 83,
                'dietary_lifestyle_id' => 5,
            ),
            302 => 
            array (
                'id' => 303,
                'recipe_id' => 84,
                'dietary_lifestyle_id' => 5,
            ),
            303 => 
            array (
                'id' => 304,
                'recipe_id' => 85,
                'dietary_lifestyle_id' => 2,
            ),
            304 => 
            array (
                'id' => 305,
                'recipe_id' => 85,
                'dietary_lifestyle_id' => 1,
            ),
            305 => 
            array (
                'id' => 306,
                'recipe_id' => 86,
                'dietary_lifestyle_id' => 3,
            ),
            306 => 
            array (
                'id' => 307,
                'recipe_id' => 86,
                'dietary_lifestyle_id' => 2,
            ),
            307 => 
            array (
                'id' => 308,
                'recipe_id' => 87,
                'dietary_lifestyle_id' => 2,
            ),
            308 => 
            array (
                'id' => 309,
                'recipe_id' => 87,
                'dietary_lifestyle_id' => 3,
            ),
            309 => 
            array (
                'id' => 310,
                'recipe_id' => 87,
                'dietary_lifestyle_id' => 5,
            ),
            310 => 
            array (
                'id' => 311,
                'recipe_id' => 88,
                'dietary_lifestyle_id' => 1,
            ),
            311 => 
            array (
                'id' => 312,
                'recipe_id' => 88,
                'dietary_lifestyle_id' => 2,
            ),
            312 => 
            array (
                'id' => 313,
                'recipe_id' => 88,
                'dietary_lifestyle_id' => 3,
            ),
            313 => 
            array (
                'id' => 314,
                'recipe_id' => 88,
                'dietary_lifestyle_id' => 4,
            ),
            314 => 
            array (
                'id' => 315,
                'recipe_id' => 88,
                'dietary_lifestyle_id' => 5,
            ),
            315 => 
            array (
                'id' => 316,
                'recipe_id' => 89,
                'dietary_lifestyle_id' => 3,
            ),
            316 => 
            array (
                'id' => 317,
                'recipe_id' => 89,
                'dietary_lifestyle_id' => 5,
            ),
            317 => 
            array (
                'id' => 318,
                'recipe_id' => 90,
                'dietary_lifestyle_id' => 1,
            ),
            318 => 
            array (
                'id' => 319,
                'recipe_id' => 90,
                'dietary_lifestyle_id' => 2,
            ),
            319 => 
            array (
                'id' => 320,
                'recipe_id' => 91,
                'dietary_lifestyle_id' => 2,
            ),
            320 => 
            array (
                'id' => 321,
                'recipe_id' => 91,
                'dietary_lifestyle_id' => 1,
            ),
            321 => 
            array (
                'id' => 322,
                'recipe_id' => 92,
                'dietary_lifestyle_id' => 1,
            ),
            322 => 
            array (
                'id' => 323,
                'recipe_id' => 92,
                'dietary_lifestyle_id' => 2,
            ),
            323 => 
            array (
                'id' => 324,
                'recipe_id' => 92,
                'dietary_lifestyle_id' => 3,
            ),
            324 => 
            array (
                'id' => 325,
                'recipe_id' => 92,
                'dietary_lifestyle_id' => 5,
            ),
            325 => 
            array (
                'id' => 326,
                'recipe_id' => 93,
                'dietary_lifestyle_id' => 5,
            ),
            326 => 
            array (
                'id' => 327,
                'recipe_id' => 93,
                'dietary_lifestyle_id' => 4,
            ),
            327 => 
            array (
                'id' => 328,
                'recipe_id' => 93,
                'dietary_lifestyle_id' => 1,
            ),
            328 => 
            array (
                'id' => 329,
                'recipe_id' => 93,
                'dietary_lifestyle_id' => 2,
            ),
            329 => 
            array (
                'id' => 330,
                'recipe_id' => 93,
                'dietary_lifestyle_id' => 3,
            ),
            330 => 
            array (
                'id' => 331,
                'recipe_id' => 94,
                'dietary_lifestyle_id' => 1,
            ),
            331 => 
            array (
                'id' => 332,
                'recipe_id' => 94,
                'dietary_lifestyle_id' => 2,
            ),
            332 => 
            array (
                'id' => 333,
                'recipe_id' => 94,
                'dietary_lifestyle_id' => 3,
            ),
            333 => 
            array (
                'id' => 334,
                'recipe_id' => 95,
                'dietary_lifestyle_id' => 1,
            ),
            334 => 
            array (
                'id' => 335,
                'recipe_id' => 95,
                'dietary_lifestyle_id' => 2,
            ),
            335 => 
            array (
                'id' => 336,
                'recipe_id' => 95,
                'dietary_lifestyle_id' => 3,
            ),
            336 => 
            array (
                'id' => 337,
                'recipe_id' => 95,
                'dietary_lifestyle_id' => 4,
            ),
            337 => 
            array (
                'id' => 338,
                'recipe_id' => 95,
                'dietary_lifestyle_id' => 5,
            ),
            338 => 
            array (
                'id' => 339,
                'recipe_id' => 96,
                'dietary_lifestyle_id' => 2,
            ),
            339 => 
            array (
                'id' => 340,
                'recipe_id' => 97,
                'dietary_lifestyle_id' => 2,
            ),
            340 => 
            array (
                'id' => 341,
                'recipe_id' => 97,
                'dietary_lifestyle_id' => 1,
            ),
            341 => 
            array (
                'id' => 342,
                'recipe_id' => 98,
                'dietary_lifestyle_id' => 5,
            ),
            342 => 
            array (
                'id' => 343,
                'recipe_id' => 98,
                'dietary_lifestyle_id' => 3,
            ),
            343 => 
            array (
                'id' => 344,
                'recipe_id' => 98,
                'dietary_lifestyle_id' => 2,
            ),
            344 => 
            array (
                'id' => 345,
                'recipe_id' => 98,
                'dietary_lifestyle_id' => 4,
            ),
            345 => 
            array (
                'id' => 346,
                'recipe_id' => 98,
                'dietary_lifestyle_id' => 1,
            ),
            346 => 
            array (
                'id' => 347,
                'recipe_id' => 99,
                'dietary_lifestyle_id' => 5,
            ),
            347 => 
            array (
                'id' => 348,
                'recipe_id' => 99,
                'dietary_lifestyle_id' => 3,
            ),
            348 => 
            array (
                'id' => 349,
                'recipe_id' => 99,
                'dietary_lifestyle_id' => 2,
            ),
            349 => 
            array (
                'id' => 350,
                'recipe_id' => 99,
                'dietary_lifestyle_id' => 4,
            ),
            350 => 
            array (
                'id' => 351,
                'recipe_id' => 99,
                'dietary_lifestyle_id' => 1,
            ),
            351 => 
            array (
                'id' => 352,
                'recipe_id' => 100,
                'dietary_lifestyle_id' => 1,
            ),
            352 => 
            array (
                'id' => 353,
                'recipe_id' => 100,
                'dietary_lifestyle_id' => 2,
            ),
            353 => 
            array (
                'id' => 354,
                'recipe_id' => 100,
                'dietary_lifestyle_id' => 3,
            ),
            354 => 
            array (
                'id' => 355,
                'recipe_id' => 100,
                'dietary_lifestyle_id' => 5,
            ),
            355 => 
            array (
                'id' => 356,
                'recipe_id' => 100,
                'dietary_lifestyle_id' => 4,
            ),
            356 => 
            array (
                'id' => 357,
                'recipe_id' => 101,
                'dietary_lifestyle_id' => 1,
            ),
            357 => 
            array (
                'id' => 358,
                'recipe_id' => 101,
                'dietary_lifestyle_id' => 2,
            ),
            358 => 
            array (
                'id' => 359,
                'recipe_id' => 101,
                'dietary_lifestyle_id' => 3,
            ),
            359 => 
            array (
                'id' => 360,
                'recipe_id' => 101,
                'dietary_lifestyle_id' => 5,
            ),
            360 => 
            array (
                'id' => 361,
                'recipe_id' => 101,
                'dietary_lifestyle_id' => 4,
            ),
            361 => 
            array (
                'id' => 362,
                'recipe_id' => 102,
                'dietary_lifestyle_id' => 2,
            ),
            362 => 
            array (
                'id' => 363,
                'recipe_id' => 103,
                'dietary_lifestyle_id' => 1,
            ),
            363 => 
            array (
                'id' => 364,
                'recipe_id' => 103,
                'dietary_lifestyle_id' => 2,
            ),
            364 => 
            array (
                'id' => 365,
                'recipe_id' => 103,
                'dietary_lifestyle_id' => 3,
            ),
            365 => 
            array (
                'id' => 366,
                'recipe_id' => 103,
                'dietary_lifestyle_id' => 4,
            ),
            366 => 
            array (
                'id' => 367,
                'recipe_id' => 103,
                'dietary_lifestyle_id' => 5,
            ),
            367 => 
            array (
                'id' => 368,
                'recipe_id' => 104,
                'dietary_lifestyle_id' => 5,
            ),
            368 => 
            array (
                'id' => 369,
                'recipe_id' => 104,
                'dietary_lifestyle_id' => 3,
            ),
            369 => 
            array (
                'id' => 370,
                'recipe_id' => 104,
                'dietary_lifestyle_id' => 2,
            ),
            370 => 
            array (
                'id' => 371,
                'recipe_id' => 104,
                'dietary_lifestyle_id' => 4,
            ),
            371 => 
            array (
                'id' => 372,
                'recipe_id' => 104,
                'dietary_lifestyle_id' => 1,
            ),
            372 => 
            array (
                'id' => 373,
                'recipe_id' => 105,
                'dietary_lifestyle_id' => 5,
            ),
            373 => 
            array (
                'id' => 374,
                'recipe_id' => 105,
                'dietary_lifestyle_id' => 3,
            ),
            374 => 
            array (
                'id' => 375,
                'recipe_id' => 105,
                'dietary_lifestyle_id' => 2,
            ),
            375 => 
            array (
                'id' => 376,
                'recipe_id' => 105,
                'dietary_lifestyle_id' => 4,
            ),
            376 => 
            array (
                'id' => 377,
                'recipe_id' => 105,
                'dietary_lifestyle_id' => 1,
            ),
            377 => 
            array (
                'id' => 378,
                'recipe_id' => 106,
                'dietary_lifestyle_id' => 1,
            ),
            378 => 
            array (
                'id' => 379,
                'recipe_id' => 106,
                'dietary_lifestyle_id' => 2,
            ),
            379 => 
            array (
                'id' => 380,
                'recipe_id' => 106,
                'dietary_lifestyle_id' => 3,
            ),
            380 => 
            array (
                'id' => 381,
                'recipe_id' => 106,
                'dietary_lifestyle_id' => 4,
            ),
            381 => 
            array (
                'id' => 382,
                'recipe_id' => 106,
                'dietary_lifestyle_id' => 5,
            ),
            382 => 
            array (
                'id' => 383,
                'recipe_id' => 107,
                'dietary_lifestyle_id' => 1,
            ),
            383 => 
            array (
                'id' => 384,
                'recipe_id' => 107,
                'dietary_lifestyle_id' => 2,
            ),
            384 => 
            array (
                'id' => 385,
                'recipe_id' => 107,
                'dietary_lifestyle_id' => 3,
            ),
            385 => 
            array (
                'id' => 386,
                'recipe_id' => 107,
                'dietary_lifestyle_id' => 4,
            ),
            386 => 
            array (
                'id' => 387,
                'recipe_id' => 107,
                'dietary_lifestyle_id' => 5,
            ),
            387 => 
            array (
                'id' => 388,
                'recipe_id' => 108,
                'dietary_lifestyle_id' => 1,
            ),
            388 => 
            array (
                'id' => 389,
                'recipe_id' => 108,
                'dietary_lifestyle_id' => 2,
            ),
            389 => 
            array (
                'id' => 390,
                'recipe_id' => 109,
                'dietary_lifestyle_id' => 1,
            ),
            390 => 
            array (
                'id' => 391,
                'recipe_id' => 109,
                'dietary_lifestyle_id' => 2,
            ),
            391 => 
            array (
                'id' => 392,
                'recipe_id' => 109,
                'dietary_lifestyle_id' => 3,
            ),
            392 => 
            array (
                'id' => 393,
                'recipe_id' => 109,
                'dietary_lifestyle_id' => 4,
            ),
            393 => 
            array (
                'id' => 394,
                'recipe_id' => 109,
                'dietary_lifestyle_id' => 5,
            ),
            394 => 
            array (
                'id' => 395,
                'recipe_id' => 110,
                'dietary_lifestyle_id' => 1,
            ),
            395 => 
            array (
                'id' => 396,
                'recipe_id' => 110,
                'dietary_lifestyle_id' => 2,
            ),
            396 => 
            array (
                'id' => 397,
                'recipe_id' => 110,
                'dietary_lifestyle_id' => 3,
            ),
            397 => 
            array (
                'id' => 398,
                'recipe_id' => 110,
                'dietary_lifestyle_id' => 4,
            ),
            398 => 
            array (
                'id' => 399,
                'recipe_id' => 110,
                'dietary_lifestyle_id' => 5,
            ),
            399 => 
            array (
                'id' => 400,
                'recipe_id' => 111,
                'dietary_lifestyle_id' => 1,
            ),
            400 => 
            array (
                'id' => 401,
                'recipe_id' => 111,
                'dietary_lifestyle_id' => 2,
            ),
            401 => 
            array (
                'id' => 402,
                'recipe_id' => 111,
                'dietary_lifestyle_id' => 3,
            ),
            402 => 
            array (
                'id' => 403,
                'recipe_id' => 111,
                'dietary_lifestyle_id' => 4,
            ),
            403 => 
            array (
                'id' => 404,
                'recipe_id' => 111,
                'dietary_lifestyle_id' => 5,
            ),
            404 => 
            array (
                'id' => 405,
                'recipe_id' => 112,
                'dietary_lifestyle_id' => 2,
            ),
            405 => 
            array (
                'id' => 406,
                'recipe_id' => 113,
                'dietary_lifestyle_id' => 2,
            ),
            406 => 
            array (
                'id' => 407,
                'recipe_id' => 114,
                'dietary_lifestyle_id' => 1,
            ),
            407 => 
            array (
                'id' => 408,
                'recipe_id' => 114,
                'dietary_lifestyle_id' => 2,
            ),
            408 => 
            array (
                'id' => 409,
                'recipe_id' => 115,
                'dietary_lifestyle_id' => 2,
            ),
            409 => 
            array (
                'id' => 410,
                'recipe_id' => 115,
                'dietary_lifestyle_id' => 3,
            ),
            410 => 
            array (
                'id' => 411,
                'recipe_id' => 115,
                'dietary_lifestyle_id' => 5,
            ),
            411 => 
            array (
                'id' => 412,
                'recipe_id' => 116,
                'dietary_lifestyle_id' => 2,
            ),
            412 => 
            array (
                'id' => 413,
                'recipe_id' => 117,
                'dietary_lifestyle_id' => 5,
            ),
            413 => 
            array (
                'id' => 414,
                'recipe_id' => 117,
                'dietary_lifestyle_id' => 3,
            ),
            414 => 
            array (
                'id' => 415,
                'recipe_id' => 118,
                'dietary_lifestyle_id' => 1,
            ),
            415 => 
            array (
                'id' => 416,
                'recipe_id' => 118,
                'dietary_lifestyle_id' => 2,
            ),
            416 => 
            array (
                'id' => 417,
                'recipe_id' => 119,
                'dietary_lifestyle_id' => 2,
            ),
            417 => 
            array (
                'id' => 418,
                'recipe_id' => 119,
                'dietary_lifestyle_id' => 3,
            ),
            418 => 
            array (
                'id' => 419,
                'recipe_id' => 119,
                'dietary_lifestyle_id' => 4,
            ),
            419 => 
            array (
                'id' => 420,
                'recipe_id' => 119,
                'dietary_lifestyle_id' => 5,
            ),
            420 => 
            array (
                'id' => 421,
                'recipe_id' => 120,
                'dietary_lifestyle_id' => 2,
            ),
            421 => 
            array (
                'id' => 422,
                'recipe_id' => 120,
                'dietary_lifestyle_id' => 1,
            ),
            422 => 
            array (
                'id' => 423,
                'recipe_id' => 121,
                'dietary_lifestyle_id' => 1,
            ),
            423 => 
            array (
                'id' => 424,
                'recipe_id' => 121,
                'dietary_lifestyle_id' => 2,
            ),
            424 => 
            array (
                'id' => 425,
                'recipe_id' => 121,
                'dietary_lifestyle_id' => 3,
            ),
            425 => 
            array (
                'id' => 426,
                'recipe_id' => 121,
                'dietary_lifestyle_id' => 4,
            ),
            426 => 
            array (
                'id' => 427,
                'recipe_id' => 121,
                'dietary_lifestyle_id' => 5,
            ),
            427 => 
            array (
                'id' => 428,
                'recipe_id' => 122,
                'dietary_lifestyle_id' => 1,
            ),
            428 => 
            array (
                'id' => 429,
                'recipe_id' => 122,
                'dietary_lifestyle_id' => 2,
            ),
            429 => 
            array (
                'id' => 430,
                'recipe_id' => 123,
                'dietary_lifestyle_id' => 1,
            ),
            430 => 
            array (
                'id' => 431,
                'recipe_id' => 123,
                'dietary_lifestyle_id' => 2,
            ),
            431 => 
            array (
                'id' => 432,
                'recipe_id' => 124,
                'dietary_lifestyle_id' => 2,
            ),
            432 => 
            array (
                'id' => 433,
                'recipe_id' => 125,
                'dietary_lifestyle_id' => 1,
            ),
            433 => 
            array (
                'id' => 434,
                'recipe_id' => 125,
                'dietary_lifestyle_id' => 2,
            ),
            434 => 
            array (
                'id' => 435,
                'recipe_id' => 125,
                'dietary_lifestyle_id' => 3,
            ),
            435 => 
            array (
                'id' => 436,
                'recipe_id' => 125,
                'dietary_lifestyle_id' => 4,
            ),
            436 => 
            array (
                'id' => 437,
                'recipe_id' => 125,
                'dietary_lifestyle_id' => 5,
            ),
            437 => 
            array (
                'id' => 438,
                'recipe_id' => 126,
                'dietary_lifestyle_id' => 2,
            ),
            438 => 
            array (
                'id' => 439,
                'recipe_id' => 127,
                'dietary_lifestyle_id' => 1,
            ),
            439 => 
            array (
                'id' => 440,
                'recipe_id' => 127,
                'dietary_lifestyle_id' => 2,
            ),
            440 => 
            array (
                'id' => 441,
                'recipe_id' => 127,
                'dietary_lifestyle_id' => 3,
            ),
            441 => 
            array (
                'id' => 442,
                'recipe_id' => 127,
                'dietary_lifestyle_id' => 4,
            ),
            442 => 
            array (
                'id' => 443,
                'recipe_id' => 127,
                'dietary_lifestyle_id' => 5,
            ),
            443 => 
            array (
                'id' => 444,
                'recipe_id' => 128,
                'dietary_lifestyle_id' => 1,
            ),
            444 => 
            array (
                'id' => 445,
                'recipe_id' => 128,
                'dietary_lifestyle_id' => 2,
            ),
            445 => 
            array (
                'id' => 446,
                'recipe_id' => 128,
                'dietary_lifestyle_id' => 3,
            ),
            446 => 
            array (
                'id' => 447,
                'recipe_id' => 128,
                'dietary_lifestyle_id' => 5,
            ),
            447 => 
            array (
                'id' => 448,
                'recipe_id' => 129,
                'dietary_lifestyle_id' => 1,
            ),
            448 => 
            array (
                'id' => 449,
                'recipe_id' => 129,
                'dietary_lifestyle_id' => 2,
            ),
            449 => 
            array (
                'id' => 450,
                'recipe_id' => 129,
                'dietary_lifestyle_id' => 3,
            ),
            450 => 
            array (
                'id' => 451,
                'recipe_id' => 130,
                'dietary_lifestyle_id' => 2,
            ),
            451 => 
            array (
                'id' => 452,
                'recipe_id' => 130,
                'dietary_lifestyle_id' => 1,
            ),
            452 => 
            array (
                'id' => 453,
                'recipe_id' => 131,
                'dietary_lifestyle_id' => 2,
            ),
            453 => 
            array (
                'id' => 454,
                'recipe_id' => 131,
                'dietary_lifestyle_id' => 3,
            ),
            454 => 
            array (
                'id' => 455,
                'recipe_id' => 131,
                'dietary_lifestyle_id' => 1,
            ),
            455 => 
            array (
                'id' => 456,
                'recipe_id' => 133,
                'dietary_lifestyle_id' => 1,
            ),
            456 => 
            array (
                'id' => 457,
                'recipe_id' => 133,
                'dietary_lifestyle_id' => 2,
            ),
            457 => 
            array (
                'id' => 458,
                'recipe_id' => 133,
                'dietary_lifestyle_id' => 3,
            ),
            458 => 
            array (
                'id' => 459,
                'recipe_id' => 133,
                'dietary_lifestyle_id' => 4,
            ),
            459 => 
            array (
                'id' => 460,
                'recipe_id' => 133,
                'dietary_lifestyle_id' => 5,
            ),
            460 => 
            array (
                'id' => 461,
                'recipe_id' => 134,
                'dietary_lifestyle_id' => 1,
            ),
            461 => 
            array (
                'id' => 462,
                'recipe_id' => 134,
                'dietary_lifestyle_id' => 2,
            ),
            462 => 
            array (
                'id' => 463,
                'recipe_id' => 134,
                'dietary_lifestyle_id' => 3,
            ),
            463 => 
            array (
                'id' => 464,
                'recipe_id' => 134,
                'dietary_lifestyle_id' => 4,
            ),
            464 => 
            array (
                'id' => 465,
                'recipe_id' => 134,
                'dietary_lifestyle_id' => 5,
            ),
            465 => 
            array (
                'id' => 466,
                'recipe_id' => 135,
                'dietary_lifestyle_id' => 1,
            ),
            466 => 
            array (
                'id' => 467,
                'recipe_id' => 135,
                'dietary_lifestyle_id' => 2,
            ),
            467 => 
            array (
                'id' => 468,
                'recipe_id' => 136,
                'dietary_lifestyle_id' => 1,
            ),
            468 => 
            array (
                'id' => 469,
                'recipe_id' => 136,
                'dietary_lifestyle_id' => 2,
            ),
            469 => 
            array (
                'id' => 470,
                'recipe_id' => 137,
                'dietary_lifestyle_id' => 1,
            ),
            470 => 
            array (
                'id' => 471,
                'recipe_id' => 137,
                'dietary_lifestyle_id' => 2,
            ),
            471 => 
            array (
                'id' => 472,
                'recipe_id' => 137,
                'dietary_lifestyle_id' => 3,
            ),
            472 => 
            array (
                'id' => 473,
                'recipe_id' => 137,
                'dietary_lifestyle_id' => 4,
            ),
            473 => 
            array (
                'id' => 474,
                'recipe_id' => 137,
                'dietary_lifestyle_id' => 5,
            ),
            474 => 
            array (
                'id' => 475,
                'recipe_id' => 138,
                'dietary_lifestyle_id' => 1,
            ),
            475 => 
            array (
                'id' => 476,
                'recipe_id' => 138,
                'dietary_lifestyle_id' => 2,
            ),
            476 => 
            array (
                'id' => 477,
                'recipe_id' => 138,
                'dietary_lifestyle_id' => 3,
            ),
            477 => 
            array (
                'id' => 478,
                'recipe_id' => 138,
                'dietary_lifestyle_id' => 4,
            ),
            478 => 
            array (
                'id' => 479,
                'recipe_id' => 138,
                'dietary_lifestyle_id' => 5,
            ),
            479 => 
            array (
                'id' => 480,
                'recipe_id' => 139,
                'dietary_lifestyle_id' => 1,
            ),
            480 => 
            array (
                'id' => 481,
                'recipe_id' => 139,
                'dietary_lifestyle_id' => 2,
            ),
            481 => 
            array (
                'id' => 482,
                'recipe_id' => 139,
                'dietary_lifestyle_id' => 3,
            ),
            482 => 
            array (
                'id' => 483,
                'recipe_id' => 139,
                'dietary_lifestyle_id' => 4,
            ),
            483 => 
            array (
                'id' => 484,
                'recipe_id' => 139,
                'dietary_lifestyle_id' => 5,
            ),
            484 => 
            array (
                'id' => 485,
                'recipe_id' => 140,
                'dietary_lifestyle_id' => 1,
            ),
            485 => 
            array (
                'id' => 486,
                'recipe_id' => 140,
                'dietary_lifestyle_id' => 2,
            ),
            486 => 
            array (
                'id' => 487,
                'recipe_id' => 140,
                'dietary_lifestyle_id' => 3,
            ),
            487 => 
            array (
                'id' => 488,
                'recipe_id' => 141,
                'dietary_lifestyle_id' => 1,
            ),
            488 => 
            array (
                'id' => 489,
                'recipe_id' => 141,
                'dietary_lifestyle_id' => 2,
            ),
            489 => 
            array (
                'id' => 490,
                'recipe_id' => 141,
                'dietary_lifestyle_id' => 3,
            ),
            490 => 
            array (
                'id' => 491,
                'recipe_id' => 141,
                'dietary_lifestyle_id' => 4,
            ),
            491 => 
            array (
                'id' => 492,
                'recipe_id' => 141,
                'dietary_lifestyle_id' => 5,
            ),
            492 => 
            array (
                'id' => 493,
                'recipe_id' => 142,
                'dietary_lifestyle_id' => 1,
            ),
            493 => 
            array (
                'id' => 494,
                'recipe_id' => 142,
                'dietary_lifestyle_id' => 2,
            ),
            494 => 
            array (
                'id' => 495,
                'recipe_id' => 142,
                'dietary_lifestyle_id' => 3,
            ),
            495 => 
            array (
                'id' => 496,
                'recipe_id' => 142,
                'dietary_lifestyle_id' => 4,
            ),
            496 => 
            array (
                'id' => 497,
                'recipe_id' => 142,
                'dietary_lifestyle_id' => 5,
            ),
            497 => 
            array (
                'id' => 498,
                'recipe_id' => 143,
                'dietary_lifestyle_id' => 1,
            ),
            498 => 
            array (
                'id' => 499,
                'recipe_id' => 143,
                'dietary_lifestyle_id' => 2,
            ),
            499 => 
            array (
                'id' => 500,
                'recipe_id' => 143,
                'dietary_lifestyle_id' => 3,
            ),
        ));
        \DB::table('recipe_dietary_lifestyle')->insert(array (
            0 => 
            array (
                'id' => 501,
                'recipe_id' => 143,
                'dietary_lifestyle_id' => 5,
            ),
            1 => 
            array (
                'id' => 502,
                'recipe_id' => 144,
                'dietary_lifestyle_id' => 1,
            ),
            2 => 
            array (
                'id' => 503,
                'recipe_id' => 144,
                'dietary_lifestyle_id' => 2,
            ),
            3 => 
            array (
                'id' => 504,
                'recipe_id' => 144,
                'dietary_lifestyle_id' => 3,
            ),
            4 => 
            array (
                'id' => 505,
                'recipe_id' => 145,
                'dietary_lifestyle_id' => 1,
            ),
            5 => 
            array (
                'id' => 506,
                'recipe_id' => 145,
                'dietary_lifestyle_id' => 2,
            ),
            6 => 
            array (
                'id' => 507,
                'recipe_id' => 146,
                'dietary_lifestyle_id' => 2,
            ),
            7 => 
            array (
                'id' => 508,
                'recipe_id' => 147,
                'dietary_lifestyle_id' => 1,
            ),
            8 => 
            array (
                'id' => 509,
                'recipe_id' => 147,
                'dietary_lifestyle_id' => 2,
            ),
            9 => 
            array (
                'id' => 510,
                'recipe_id' => 148,
                'dietary_lifestyle_id' => 2,
            ),
            10 => 
            array (
                'id' => 511,
                'recipe_id' => 148,
                'dietary_lifestyle_id' => 3,
            ),
            11 => 
            array (
                'id' => 512,
                'recipe_id' => 148,
                'dietary_lifestyle_id' => 5,
            ),
            12 => 
            array (
                'id' => 513,
                'recipe_id' => 149,
                'dietary_lifestyle_id' => 1,
            ),
            13 => 
            array (
                'id' => 514,
                'recipe_id' => 149,
                'dietary_lifestyle_id' => 2,
            ),
            14 => 
            array (
                'id' => 515,
                'recipe_id' => 149,
                'dietary_lifestyle_id' => 3,
            ),
            15 => 
            array (
                'id' => 516,
                'recipe_id' => 149,
                'dietary_lifestyle_id' => 4,
            ),
            16 => 
            array (
                'id' => 517,
                'recipe_id' => 149,
                'dietary_lifestyle_id' => 5,
            ),
            17 => 
            array (
                'id' => 518,
                'recipe_id' => 150,
                'dietary_lifestyle_id' => 1,
            ),
            18 => 
            array (
                'id' => 519,
                'recipe_id' => 150,
                'dietary_lifestyle_id' => 2,
            ),
            19 => 
            array (
                'id' => 520,
                'recipe_id' => 150,
                'dietary_lifestyle_id' => 3,
            ),
            20 => 
            array (
                'id' => 521,
                'recipe_id' => 151,
                'dietary_lifestyle_id' => 1,
            ),
            21 => 
            array (
                'id' => 522,
                'recipe_id' => 151,
                'dietary_lifestyle_id' => 2,
            ),
            22 => 
            array (
                'id' => 523,
                'recipe_id' => 151,
                'dietary_lifestyle_id' => 3,
            ),
            23 => 
            array (
                'id' => 524,
                'recipe_id' => 152,
                'dietary_lifestyle_id' => 1,
            ),
            24 => 
            array (
                'id' => 525,
                'recipe_id' => 152,
                'dietary_lifestyle_id' => 3,
            ),
            25 => 
            array (
                'id' => 526,
                'recipe_id' => 152,
                'dietary_lifestyle_id' => 4,
            ),
            26 => 
            array (
                'id' => 527,
                'recipe_id' => 152,
                'dietary_lifestyle_id' => 5,
            ),
            27 => 
            array (
                'id' => 528,
                'recipe_id' => 153,
                'dietary_lifestyle_id' => 1,
            ),
            28 => 
            array (
                'id' => 529,
                'recipe_id' => 153,
                'dietary_lifestyle_id' => 3,
            ),
            29 => 
            array (
                'id' => 530,
                'recipe_id' => 153,
                'dietary_lifestyle_id' => 5,
            ),
            30 => 
            array (
                'id' => 531,
                'recipe_id' => 153,
                'dietary_lifestyle_id' => 4,
            ),
            31 => 
            array (
                'id' => 532,
                'recipe_id' => 154,
                'dietary_lifestyle_id' => 3,
            ),
            32 => 
            array (
                'id' => 533,
                'recipe_id' => 154,
                'dietary_lifestyle_id' => 5,
            ),
            33 => 
            array (
                'id' => 534,
                'recipe_id' => 155,
                'dietary_lifestyle_id' => 1,
            ),
            34 => 
            array (
                'id' => 535,
                'recipe_id' => 155,
                'dietary_lifestyle_id' => 2,
            ),
            35 => 
            array (
                'id' => 536,
                'recipe_id' => 155,
                'dietary_lifestyle_id' => 3,
            ),
            36 => 
            array (
                'id' => 537,
                'recipe_id' => 155,
                'dietary_lifestyle_id' => 4,
            ),
            37 => 
            array (
                'id' => 538,
                'recipe_id' => 155,
                'dietary_lifestyle_id' => 5,
            ),
            38 => 
            array (
                'id' => 539,
                'recipe_id' => 156,
                'dietary_lifestyle_id' => 1,
            ),
            39 => 
            array (
                'id' => 540,
                'recipe_id' => 156,
                'dietary_lifestyle_id' => 2,
            ),
            40 => 
            array (
                'id' => 541,
                'recipe_id' => 157,
                'dietary_lifestyle_id' => 1,
            ),
            41 => 
            array (
                'id' => 542,
                'recipe_id' => 157,
                'dietary_lifestyle_id' => 2,
            ),
            42 => 
            array (
                'id' => 543,
                'recipe_id' => 158,
                'dietary_lifestyle_id' => 2,
            ),
            43 => 
            array (
                'id' => 544,
                'recipe_id' => 158,
                'dietary_lifestyle_id' => 3,
            ),
            44 => 
            array (
                'id' => 545,
                'recipe_id' => 158,
                'dietary_lifestyle_id' => 5,
            ),
            45 => 
            array (
                'id' => 546,
                'recipe_id' => 159,
                'dietary_lifestyle_id' => 1,
            ),
            46 => 
            array (
                'id' => 547,
                'recipe_id' => 159,
                'dietary_lifestyle_id' => 2,
            ),
            47 => 
            array (
                'id' => 548,
                'recipe_id' => 159,
                'dietary_lifestyle_id' => 3,
            ),
            48 => 
            array (
                'id' => 549,
                'recipe_id' => 159,
                'dietary_lifestyle_id' => 4,
            ),
            49 => 
            array (
                'id' => 550,
                'recipe_id' => 159,
                'dietary_lifestyle_id' => 5,
            ),
            50 => 
            array (
                'id' => 551,
                'recipe_id' => 160,
                'dietary_lifestyle_id' => 1,
            ),
            51 => 
            array (
                'id' => 552,
                'recipe_id' => 160,
                'dietary_lifestyle_id' => 2,
            ),
            52 => 
            array (
                'id' => 553,
                'recipe_id' => 161,
                'dietary_lifestyle_id' => 3,
            ),
            53 => 
            array (
                'id' => 554,
                'recipe_id' => 161,
                'dietary_lifestyle_id' => 5,
            ),
            54 => 
            array (
                'id' => 555,
                'recipe_id' => 161,
                'dietary_lifestyle_id' => 2,
            ),
            55 => 
            array (
                'id' => 556,
                'recipe_id' => 161,
                'dietary_lifestyle_id' => 4,
            ),
            56 => 
            array (
                'id' => 557,
                'recipe_id' => 161,
                'dietary_lifestyle_id' => 1,
            ),
            57 => 
            array (
                'id' => 558,
                'recipe_id' => 162,
                'dietary_lifestyle_id' => 2,
            ),
            58 => 
            array (
                'id' => 559,
                'recipe_id' => 163,
                'dietary_lifestyle_id' => 1,
            ),
            59 => 
            array (
                'id' => 560,
                'recipe_id' => 163,
                'dietary_lifestyle_id' => 2,
            ),
            60 => 
            array (
                'id' => 561,
                'recipe_id' => 163,
                'dietary_lifestyle_id' => 3,
            ),
            61 => 
            array (
                'id' => 562,
                'recipe_id' => 164,
                'dietary_lifestyle_id' => 1,
            ),
            62 => 
            array (
                'id' => 563,
                'recipe_id' => 164,
                'dietary_lifestyle_id' => 2,
            ),
            63 => 
            array (
                'id' => 564,
                'recipe_id' => 164,
                'dietary_lifestyle_id' => 3,
            ),
            64 => 
            array (
                'id' => 565,
                'recipe_id' => 164,
                'dietary_lifestyle_id' => 4,
            ),
            65 => 
            array (
                'id' => 566,
                'recipe_id' => 164,
                'dietary_lifestyle_id' => 5,
            ),
            66 => 
            array (
                'id' => 567,
                'recipe_id' => 165,
                'dietary_lifestyle_id' => 1,
            ),
            67 => 
            array (
                'id' => 568,
                'recipe_id' => 165,
                'dietary_lifestyle_id' => 2,
            ),
            68 => 
            array (
                'id' => 569,
                'recipe_id' => 165,
                'dietary_lifestyle_id' => 3,
            ),
            69 => 
            array (
                'id' => 570,
                'recipe_id' => 165,
                'dietary_lifestyle_id' => 4,
            ),
            70 => 
            array (
                'id' => 571,
                'recipe_id' => 165,
                'dietary_lifestyle_id' => 5,
            ),
            71 => 
            array (
                'id' => 572,
                'recipe_id' => 166,
                'dietary_lifestyle_id' => 1,
            ),
            72 => 
            array (
                'id' => 573,
                'recipe_id' => 166,
                'dietary_lifestyle_id' => 2,
            ),
            73 => 
            array (
                'id' => 574,
                'recipe_id' => 166,
                'dietary_lifestyle_id' => 3,
            ),
            74 => 
            array (
                'id' => 575,
                'recipe_id' => 166,
                'dietary_lifestyle_id' => 4,
            ),
            75 => 
            array (
                'id' => 576,
                'recipe_id' => 166,
                'dietary_lifestyle_id' => 5,
            ),
            76 => 
            array (
                'id' => 577,
                'recipe_id' => 167,
                'dietary_lifestyle_id' => 1,
            ),
            77 => 
            array (
                'id' => 578,
                'recipe_id' => 167,
                'dietary_lifestyle_id' => 2,
            ),
            78 => 
            array (
                'id' => 579,
                'recipe_id' => 167,
                'dietary_lifestyle_id' => 3,
            ),
            79 => 
            array (
                'id' => 580,
                'recipe_id' => 167,
                'dietary_lifestyle_id' => 4,
            ),
            80 => 
            array (
                'id' => 581,
                'recipe_id' => 167,
                'dietary_lifestyle_id' => 5,
            ),
            81 => 
            array (
                'id' => 582,
                'recipe_id' => 168,
                'dietary_lifestyle_id' => 1,
            ),
            82 => 
            array (
                'id' => 583,
                'recipe_id' => 168,
                'dietary_lifestyle_id' => 2,
            ),
            83 => 
            array (
                'id' => 584,
                'recipe_id' => 168,
                'dietary_lifestyle_id' => 3,
            ),
            84 => 
            array (
                'id' => 585,
                'recipe_id' => 168,
                'dietary_lifestyle_id' => 4,
            ),
            85 => 
            array (
                'id' => 586,
                'recipe_id' => 168,
                'dietary_lifestyle_id' => 5,
            ),
            86 => 
            array (
                'id' => 587,
                'recipe_id' => 169,
                'dietary_lifestyle_id' => 1,
            ),
            87 => 
            array (
                'id' => 588,
                'recipe_id' => 169,
                'dietary_lifestyle_id' => 2,
            ),
            88 => 
            array (
                'id' => 589,
                'recipe_id' => 169,
                'dietary_lifestyle_id' => 3,
            ),
            89 => 
            array (
                'id' => 590,
                'recipe_id' => 170,
                'dietary_lifestyle_id' => 1,
            ),
            90 => 
            array (
                'id' => 591,
                'recipe_id' => 170,
                'dietary_lifestyle_id' => 2,
            ),
            91 => 
            array (
                'id' => 592,
                'recipe_id' => 170,
                'dietary_lifestyle_id' => 3,
            ),
            92 => 
            array (
                'id' => 593,
                'recipe_id' => 170,
                'dietary_lifestyle_id' => 4,
            ),
            93 => 
            array (
                'id' => 594,
                'recipe_id' => 170,
                'dietary_lifestyle_id' => 5,
            ),
            94 => 
            array (
                'id' => 595,
                'recipe_id' => 171,
                'dietary_lifestyle_id' => 1,
            ),
            95 => 
            array (
                'id' => 596,
                'recipe_id' => 171,
                'dietary_lifestyle_id' => 2,
            ),
            96 => 
            array (
                'id' => 597,
                'recipe_id' => 171,
                'dietary_lifestyle_id' => 5,
            ),
            97 => 
            array (
                'id' => 598,
                'recipe_id' => 171,
                'dietary_lifestyle_id' => 3,
            ),
            98 => 
            array (
                'id' => 599,
                'recipe_id' => 171,
                'dietary_lifestyle_id' => 4,
            ),
            99 => 
            array (
                'id' => 600,
                'recipe_id' => 172,
                'dietary_lifestyle_id' => 1,
            ),
            100 => 
            array (
                'id' => 601,
                'recipe_id' => 172,
                'dietary_lifestyle_id' => 2,
            ),
            101 => 
            array (
                'id' => 602,
                'recipe_id' => 173,
                'dietary_lifestyle_id' => 1,
            ),
            102 => 
            array (
                'id' => 603,
                'recipe_id' => 173,
                'dietary_lifestyle_id' => 2,
            ),
            103 => 
            array (
                'id' => 604,
                'recipe_id' => 174,
                'dietary_lifestyle_id' => 1,
            ),
            104 => 
            array (
                'id' => 605,
                'recipe_id' => 174,
                'dietary_lifestyle_id' => 2,
            ),
            105 => 
            array (
                'id' => 606,
                'recipe_id' => 176,
                'dietary_lifestyle_id' => 1,
            ),
            106 => 
            array (
                'id' => 607,
                'recipe_id' => 176,
                'dietary_lifestyle_id' => 2,
            ),
            107 => 
            array (
                'id' => 608,
                'recipe_id' => 177,
                'dietary_lifestyle_id' => 1,
            ),
            108 => 
            array (
                'id' => 609,
                'recipe_id' => 177,
                'dietary_lifestyle_id' => 2,
            ),
            109 => 
            array (
                'id' => 610,
                'recipe_id' => 177,
                'dietary_lifestyle_id' => 3,
            ),
            110 => 
            array (
                'id' => 611,
                'recipe_id' => 177,
                'dietary_lifestyle_id' => 5,
            ),
            111 => 
            array (
                'id' => 612,
                'recipe_id' => 177,
                'dietary_lifestyle_id' => 4,
            ),
            112 => 
            array (
                'id' => 613,
                'recipe_id' => 178,
                'dietary_lifestyle_id' => 1,
            ),
            113 => 
            array (
                'id' => 614,
                'recipe_id' => 178,
                'dietary_lifestyle_id' => 2,
            ),
            114 => 
            array (
                'id' => 615,
                'recipe_id' => 178,
                'dietary_lifestyle_id' => 3,
            ),
            115 => 
            array (
                'id' => 616,
                'recipe_id' => 178,
                'dietary_lifestyle_id' => 4,
            ),
            116 => 
            array (
                'id' => 617,
                'recipe_id' => 178,
                'dietary_lifestyle_id' => 5,
            ),
            117 => 
            array (
                'id' => 618,
                'recipe_id' => 179,
                'dietary_lifestyle_id' => 1,
            ),
            118 => 
            array (
                'id' => 619,
                'recipe_id' => 179,
                'dietary_lifestyle_id' => 2,
            ),
            119 => 
            array (
                'id' => 620,
                'recipe_id' => 179,
                'dietary_lifestyle_id' => 3,
            ),
            120 => 
            array (
                'id' => 621,
                'recipe_id' => 179,
                'dietary_lifestyle_id' => 4,
            ),
            121 => 
            array (
                'id' => 622,
                'recipe_id' => 179,
                'dietary_lifestyle_id' => 5,
            ),
            122 => 
            array (
                'id' => 623,
                'recipe_id' => 180,
                'dietary_lifestyle_id' => 1,
            ),
            123 => 
            array (
                'id' => 624,
                'recipe_id' => 180,
                'dietary_lifestyle_id' => 2,
            ),
            124 => 
            array (
                'id' => 625,
                'recipe_id' => 181,
                'dietary_lifestyle_id' => 1,
            ),
            125 => 
            array (
                'id' => 626,
                'recipe_id' => 181,
                'dietary_lifestyle_id' => 2,
            ),
            126 => 
            array (
                'id' => 627,
                'recipe_id' => 182,
                'dietary_lifestyle_id' => 2,
            ),
            127 => 
            array (
                'id' => 628,
                'recipe_id' => 182,
                'dietary_lifestyle_id' => 3,
            ),
            128 => 
            array (
                'id' => 629,
                'recipe_id' => 183,
                'dietary_lifestyle_id' => 2,
            ),
            129 => 
            array (
                'id' => 630,
                'recipe_id' => 183,
                'dietary_lifestyle_id' => 3,
            ),
            130 => 
            array (
                'id' => 631,
                'recipe_id' => 184,
                'dietary_lifestyle_id' => 1,
            ),
            131 => 
            array (
                'id' => 632,
                'recipe_id' => 184,
                'dietary_lifestyle_id' => 2,
            ),
            132 => 
            array (
                'id' => 633,
                'recipe_id' => 185,
                'dietary_lifestyle_id' => 1,
            ),
            133 => 
            array (
                'id' => 634,
                'recipe_id' => 185,
                'dietary_lifestyle_id' => 2,
            ),
            134 => 
            array (
                'id' => 635,
                'recipe_id' => 185,
                'dietary_lifestyle_id' => 3,
            ),
            135 => 
            array (
                'id' => 636,
                'recipe_id' => 185,
                'dietary_lifestyle_id' => 4,
            ),
            136 => 
            array (
                'id' => 637,
                'recipe_id' => 185,
                'dietary_lifestyle_id' => 5,
            ),
            137 => 
            array (
                'id' => 638,
                'recipe_id' => 186,
                'dietary_lifestyle_id' => 2,
            ),
            138 => 
            array (
                'id' => 639,
                'recipe_id' => 187,
                'dietary_lifestyle_id' => 1,
            ),
            139 => 
            array (
                'id' => 640,
                'recipe_id' => 187,
                'dietary_lifestyle_id' => 2,
            ),
            140 => 
            array (
                'id' => 641,
                'recipe_id' => 187,
                'dietary_lifestyle_id' => 3,
            ),
            141 => 
            array (
                'id' => 642,
                'recipe_id' => 188,
                'dietary_lifestyle_id' => 1,
            ),
            142 => 
            array (
                'id' => 643,
                'recipe_id' => 188,
                'dietary_lifestyle_id' => 2,
            ),
            143 => 
            array (
                'id' => 644,
                'recipe_id' => 189,
                'dietary_lifestyle_id' => 5,
            ),
            144 => 
            array (
                'id' => 645,
                'recipe_id' => 189,
                'dietary_lifestyle_id' => 1,
            ),
            145 => 
            array (
                'id' => 646,
                'recipe_id' => 189,
                'dietary_lifestyle_id' => 2,
            ),
            146 => 
            array (
                'id' => 647,
                'recipe_id' => 189,
                'dietary_lifestyle_id' => 3,
            ),
            147 => 
            array (
                'id' => 648,
                'recipe_id' => 189,
                'dietary_lifestyle_id' => 4,
            ),
            148 => 
            array (
                'id' => 649,
                'recipe_id' => 190,
                'dietary_lifestyle_id' => 2,
            ),
            149 => 
            array (
                'id' => 650,
                'recipe_id' => 190,
                'dietary_lifestyle_id' => 3,
            ),
            150 => 
            array (
                'id' => 651,
                'recipe_id' => 191,
                'dietary_lifestyle_id' => 1,
            ),
            151 => 
            array (
                'id' => 652,
                'recipe_id' => 191,
                'dietary_lifestyle_id' => 2,
            ),
            152 => 
            array (
                'id' => 653,
                'recipe_id' => 191,
                'dietary_lifestyle_id' => 3,
            ),
            153 => 
            array (
                'id' => 654,
                'recipe_id' => 191,
                'dietary_lifestyle_id' => 4,
            ),
            154 => 
            array (
                'id' => 655,
                'recipe_id' => 191,
                'dietary_lifestyle_id' => 5,
            ),
            155 => 
            array (
                'id' => 656,
                'recipe_id' => 192,
                'dietary_lifestyle_id' => 2,
            ),
            156 => 
            array (
                'id' => 657,
                'recipe_id' => 193,
                'dietary_lifestyle_id' => 1,
            ),
            157 => 
            array (
                'id' => 658,
                'recipe_id' => 193,
                'dietary_lifestyle_id' => 2,
            ),
            158 => 
            array (
                'id' => 659,
                'recipe_id' => 193,
                'dietary_lifestyle_id' => 3,
            ),
            159 => 
            array (
                'id' => 660,
                'recipe_id' => 193,
                'dietary_lifestyle_id' => 4,
            ),
            160 => 
            array (
                'id' => 661,
                'recipe_id' => 193,
                'dietary_lifestyle_id' => 5,
            ),
            161 => 
            array (
                'id' => 662,
                'recipe_id' => 194,
                'dietary_lifestyle_id' => 1,
            ),
            162 => 
            array (
                'id' => 663,
                'recipe_id' => 194,
                'dietary_lifestyle_id' => 2,
            ),
            163 => 
            array (
                'id' => 664,
                'recipe_id' => 194,
                'dietary_lifestyle_id' => 3,
            ),
            164 => 
            array (
                'id' => 665,
                'recipe_id' => 194,
                'dietary_lifestyle_id' => 4,
            ),
            165 => 
            array (
                'id' => 666,
                'recipe_id' => 194,
                'dietary_lifestyle_id' => 5,
            ),
            166 => 
            array (
                'id' => 667,
                'recipe_id' => 195,
                'dietary_lifestyle_id' => 1,
            ),
            167 => 
            array (
                'id' => 668,
                'recipe_id' => 195,
                'dietary_lifestyle_id' => 2,
            ),
            168 => 
            array (
                'id' => 669,
                'recipe_id' => 195,
                'dietary_lifestyle_id' => 3,
            ),
            169 => 
            array (
                'id' => 670,
                'recipe_id' => 196,
                'dietary_lifestyle_id' => 2,
            ),
            170 => 
            array (
                'id' => 671,
                'recipe_id' => 196,
                'dietary_lifestyle_id' => 3,
            ),
            171 => 
            array (
                'id' => 672,
                'recipe_id' => 196,
                'dietary_lifestyle_id' => 5,
            ),
            172 => 
            array (
                'id' => 673,
                'recipe_id' => 197,
                'dietary_lifestyle_id' => 1,
            ),
            173 => 
            array (
                'id' => 674,
                'recipe_id' => 197,
                'dietary_lifestyle_id' => 2,
            ),
            174 => 
            array (
                'id' => 675,
                'recipe_id' => 197,
                'dietary_lifestyle_id' => 3,
            ),
            175 => 
            array (
                'id' => 676,
                'recipe_id' => 197,
                'dietary_lifestyle_id' => 4,
            ),
            176 => 
            array (
                'id' => 677,
                'recipe_id' => 197,
                'dietary_lifestyle_id' => 5,
            ),
            177 => 
            array (
                'id' => 678,
                'recipe_id' => 198,
                'dietary_lifestyle_id' => 1,
            ),
            178 => 
            array (
                'id' => 679,
                'recipe_id' => 198,
                'dietary_lifestyle_id' => 2,
            ),
            179 => 
            array (
                'id' => 680,
                'recipe_id' => 198,
                'dietary_lifestyle_id' => 3,
            ),
            180 => 
            array (
                'id' => 681,
                'recipe_id' => 198,
                'dietary_lifestyle_id' => 4,
            ),
            181 => 
            array (
                'id' => 682,
                'recipe_id' => 198,
                'dietary_lifestyle_id' => 5,
            ),
            182 => 
            array (
                'id' => 683,
                'recipe_id' => 199,
                'dietary_lifestyle_id' => 1,
            ),
            183 => 
            array (
                'id' => 684,
                'recipe_id' => 199,
                'dietary_lifestyle_id' => 2,
            ),
            184 => 
            array (
                'id' => 685,
                'recipe_id' => 200,
                'dietary_lifestyle_id' => 2,
            ),
            185 => 
            array (
                'id' => 686,
                'recipe_id' => 200,
                'dietary_lifestyle_id' => 3,
            ),
            186 => 
            array (
                'id' => 687,
                'recipe_id' => 200,
                'dietary_lifestyle_id' => 5,
            ),
            187 => 
            array (
                'id' => 688,
                'recipe_id' => 201,
                'dietary_lifestyle_id' => 1,
            ),
            188 => 
            array (
                'id' => 689,
                'recipe_id' => 201,
                'dietary_lifestyle_id' => 2,
            ),
            189 => 
            array (
                'id' => 690,
                'recipe_id' => 202,
                'dietary_lifestyle_id' => 1,
            ),
            190 => 
            array (
                'id' => 691,
                'recipe_id' => 202,
                'dietary_lifestyle_id' => 2,
            ),
            191 => 
            array (
                'id' => 692,
                'recipe_id' => 202,
                'dietary_lifestyle_id' => 3,
            ),
            192 => 
            array (
                'id' => 693,
                'recipe_id' => 202,
                'dietary_lifestyle_id' => 4,
            ),
            193 => 
            array (
                'id' => 694,
                'recipe_id' => 202,
                'dietary_lifestyle_id' => 5,
            ),
            194 => 
            array (
                'id' => 695,
                'recipe_id' => 203,
                'dietary_lifestyle_id' => 1,
            ),
            195 => 
            array (
                'id' => 696,
                'recipe_id' => 203,
                'dietary_lifestyle_id' => 2,
            ),
            196 => 
            array (
                'id' => 697,
                'recipe_id' => 203,
                'dietary_lifestyle_id' => 3,
            ),
            197 => 
            array (
                'id' => 698,
                'recipe_id' => 203,
                'dietary_lifestyle_id' => 4,
            ),
            198 => 
            array (
                'id' => 699,
                'recipe_id' => 203,
                'dietary_lifestyle_id' => 5,
            ),
            199 => 
            array (
                'id' => 700,
                'recipe_id' => 204,
                'dietary_lifestyle_id' => 2,
            ),
            200 => 
            array (
                'id' => 701,
                'recipe_id' => 204,
                'dietary_lifestyle_id' => 3,
            ),
            201 => 
            array (
                'id' => 702,
                'recipe_id' => 204,
                'dietary_lifestyle_id' => 5,
            ),
            202 => 
            array (
                'id' => 703,
                'recipe_id' => 205,
                'dietary_lifestyle_id' => 1,
            ),
            203 => 
            array (
                'id' => 704,
                'recipe_id' => 205,
                'dietary_lifestyle_id' => 2,
            ),
            204 => 
            array (
                'id' => 705,
                'recipe_id' => 206,
                'dietary_lifestyle_id' => 2,
            ),
            205 => 
            array (
                'id' => 706,
                'recipe_id' => 206,
                'dietary_lifestyle_id' => 3,
            ),
            206 => 
            array (
                'id' => 707,
                'recipe_id' => 206,
                'dietary_lifestyle_id' => 5,
            ),
            207 => 
            array (
                'id' => 708,
                'recipe_id' => 207,
                'dietary_lifestyle_id' => 1,
            ),
            208 => 
            array (
                'id' => 709,
                'recipe_id' => 207,
                'dietary_lifestyle_id' => 2,
            ),
            209 => 
            array (
                'id' => 710,
                'recipe_id' => 207,
                'dietary_lifestyle_id' => 3,
            ),
            210 => 
            array (
                'id' => 711,
                'recipe_id' => 207,
                'dietary_lifestyle_id' => 4,
            ),
            211 => 
            array (
                'id' => 712,
                'recipe_id' => 207,
                'dietary_lifestyle_id' => 5,
            ),
            212 => 
            array (
                'id' => 713,
                'recipe_id' => 208,
                'dietary_lifestyle_id' => 1,
            ),
            213 => 
            array (
                'id' => 714,
                'recipe_id' => 208,
                'dietary_lifestyle_id' => 2,
            ),
            214 => 
            array (
                'id' => 715,
                'recipe_id' => 208,
                'dietary_lifestyle_id' => 3,
            ),
            215 => 
            array (
                'id' => 716,
                'recipe_id' => 208,
                'dietary_lifestyle_id' => 4,
            ),
            216 => 
            array (
                'id' => 717,
                'recipe_id' => 208,
                'dietary_lifestyle_id' => 5,
            ),
            217 => 
            array (
                'id' => 718,
                'recipe_id' => 209,
                'dietary_lifestyle_id' => 1,
            ),
            218 => 
            array (
                'id' => 719,
                'recipe_id' => 209,
                'dietary_lifestyle_id' => 2,
            ),
            219 => 
            array (
                'id' => 720,
                'recipe_id' => 210,
                'dietary_lifestyle_id' => 1,
            ),
            220 => 
            array (
                'id' => 721,
                'recipe_id' => 210,
                'dietary_lifestyle_id' => 2,
            ),
            221 => 
            array (
                'id' => 722,
                'recipe_id' => 211,
                'dietary_lifestyle_id' => 1,
            ),
            222 => 
            array (
                'id' => 723,
                'recipe_id' => 211,
                'dietary_lifestyle_id' => 2,
            ),
            223 => 
            array (
                'id' => 724,
                'recipe_id' => 211,
                'dietary_lifestyle_id' => 3,
            ),
            224 => 
            array (
                'id' => 725,
                'recipe_id' => 211,
                'dietary_lifestyle_id' => 4,
            ),
            225 => 
            array (
                'id' => 726,
                'recipe_id' => 211,
                'dietary_lifestyle_id' => 5,
            ),
            226 => 
            array (
                'id' => 727,
                'recipe_id' => 212,
                'dietary_lifestyle_id' => 1,
            ),
            227 => 
            array (
                'id' => 728,
                'recipe_id' => 212,
                'dietary_lifestyle_id' => 2,
            ),
            228 => 
            array (
                'id' => 729,
                'recipe_id' => 212,
                'dietary_lifestyle_id' => 3,
            ),
            229 => 
            array (
                'id' => 730,
                'recipe_id' => 212,
                'dietary_lifestyle_id' => 4,
            ),
            230 => 
            array (
                'id' => 731,
                'recipe_id' => 212,
                'dietary_lifestyle_id' => 5,
            ),
            231 => 
            array (
                'id' => 732,
                'recipe_id' => 213,
                'dietary_lifestyle_id' => 1,
            ),
            232 => 
            array (
                'id' => 733,
                'recipe_id' => 213,
                'dietary_lifestyle_id' => 2,
            ),
            233 => 
            array (
                'id' => 734,
                'recipe_id' => 213,
                'dietary_lifestyle_id' => 3,
            ),
            234 => 
            array (
                'id' => 735,
                'recipe_id' => 213,
                'dietary_lifestyle_id' => 4,
            ),
            235 => 
            array (
                'id' => 736,
                'recipe_id' => 213,
                'dietary_lifestyle_id' => 5,
            ),
            236 => 
            array (
                'id' => 737,
                'recipe_id' => 214,
                'dietary_lifestyle_id' => 1,
            ),
            237 => 
            array (
                'id' => 738,
                'recipe_id' => 214,
                'dietary_lifestyle_id' => 2,
            ),
            238 => 
            array (
                'id' => 739,
                'recipe_id' => 215,
                'dietary_lifestyle_id' => 1,
            ),
            239 => 
            array (
                'id' => 740,
                'recipe_id' => 215,
                'dietary_lifestyle_id' => 2,
            ),
            240 => 
            array (
                'id' => 741,
                'recipe_id' => 216,
                'dietary_lifestyle_id' => 1,
            ),
            241 => 
            array (
                'id' => 742,
                'recipe_id' => 216,
                'dietary_lifestyle_id' => 2,
            ),
            242 => 
            array (
                'id' => 743,
                'recipe_id' => 216,
                'dietary_lifestyle_id' => 3,
            ),
            243 => 
            array (
                'id' => 744,
                'recipe_id' => 216,
                'dietary_lifestyle_id' => 4,
            ),
            244 => 
            array (
                'id' => 745,
                'recipe_id' => 216,
                'dietary_lifestyle_id' => 5,
            ),
            245 => 
            array (
                'id' => 746,
                'recipe_id' => 217,
                'dietary_lifestyle_id' => 2,
            ),
            246 => 
            array (
                'id' => 747,
                'recipe_id' => 218,
                'dietary_lifestyle_id' => 2,
            ),
            247 => 
            array (
                'id' => 748,
                'recipe_id' => 218,
                'dietary_lifestyle_id' => 1,
            ),
            248 => 
            array (
                'id' => 749,
                'recipe_id' => 219,
                'dietary_lifestyle_id' => 1,
            ),
            249 => 
            array (
                'id' => 750,
                'recipe_id' => 219,
                'dietary_lifestyle_id' => 2,
            ),
            250 => 
            array (
                'id' => 751,
                'recipe_id' => 219,
                'dietary_lifestyle_id' => 3,
            ),
            251 => 
            array (
                'id' => 752,
                'recipe_id' => 219,
                'dietary_lifestyle_id' => 4,
            ),
            252 => 
            array (
                'id' => 753,
                'recipe_id' => 219,
                'dietary_lifestyle_id' => 5,
            ),
            253 => 
            array (
                'id' => 754,
                'recipe_id' => 220,
                'dietary_lifestyle_id' => 1,
            ),
            254 => 
            array (
                'id' => 755,
                'recipe_id' => 220,
                'dietary_lifestyle_id' => 2,
            ),
            255 => 
            array (
                'id' => 756,
                'recipe_id' => 221,
                'dietary_lifestyle_id' => 1,
            ),
            256 => 
            array (
                'id' => 757,
                'recipe_id' => 221,
                'dietary_lifestyle_id' => 3,
            ),
            257 => 
            array (
                'id' => 758,
                'recipe_id' => 221,
                'dietary_lifestyle_id' => 4,
            ),
            258 => 
            array (
                'id' => 759,
                'recipe_id' => 221,
                'dietary_lifestyle_id' => 5,
            ),
            259 => 
            array (
                'id' => 760,
                'recipe_id' => 222,
                'dietary_lifestyle_id' => 1,
            ),
            260 => 
            array (
                'id' => 761,
                'recipe_id' => 222,
                'dietary_lifestyle_id' => 2,
            ),
            261 => 
            array (
                'id' => 762,
                'recipe_id' => 222,
                'dietary_lifestyle_id' => 3,
            ),
            262 => 
            array (
                'id' => 763,
                'recipe_id' => 222,
                'dietary_lifestyle_id' => 4,
            ),
            263 => 
            array (
                'id' => 764,
                'recipe_id' => 222,
                'dietary_lifestyle_id' => 5,
            ),
            264 => 
            array (
                'id' => 765,
                'recipe_id' => 223,
                'dietary_lifestyle_id' => 3,
            ),
            265 => 
            array (
                'id' => 766,
                'recipe_id' => 223,
                'dietary_lifestyle_id' => 1,
            ),
            266 => 
            array (
                'id' => 767,
                'recipe_id' => 223,
                'dietary_lifestyle_id' => 2,
            ),
            267 => 
            array (
                'id' => 768,
                'recipe_id' => 224,
                'dietary_lifestyle_id' => 2,
            ),
            268 => 
            array (
                'id' => 769,
                'recipe_id' => 225,
                'dietary_lifestyle_id' => 1,
            ),
            269 => 
            array (
                'id' => 770,
                'recipe_id' => 225,
                'dietary_lifestyle_id' => 2,
            ),
            270 => 
            array (
                'id' => 771,
                'recipe_id' => 227,
                'dietary_lifestyle_id' => 1,
            ),
            271 => 
            array (
                'id' => 772,
                'recipe_id' => 227,
                'dietary_lifestyle_id' => 4,
            ),
            272 => 
            array (
                'id' => 773,
                'recipe_id' => 227,
                'dietary_lifestyle_id' => 5,
            ),
            273 => 
            array (
                'id' => 774,
                'recipe_id' => 227,
                'dietary_lifestyle_id' => 3,
            ),
            274 => 
            array (
                'id' => 775,
                'recipe_id' => 227,
                'dietary_lifestyle_id' => 2,
            ),
            275 => 
            array (
                'id' => 776,
                'recipe_id' => 228,
                'dietary_lifestyle_id' => 1,
            ),
            276 => 
            array (
                'id' => 777,
                'recipe_id' => 228,
                'dietary_lifestyle_id' => 4,
            ),
            277 => 
            array (
                'id' => 778,
                'recipe_id' => 228,
                'dietary_lifestyle_id' => 5,
            ),
            278 => 
            array (
                'id' => 779,
                'recipe_id' => 228,
                'dietary_lifestyle_id' => 3,
            ),
            279 => 
            array (
                'id' => 780,
                'recipe_id' => 229,
                'dietary_lifestyle_id' => 1,
            ),
            280 => 
            array (
                'id' => 781,
                'recipe_id' => 229,
                'dietary_lifestyle_id' => 2,
            ),
            281 => 
            array (
                'id' => 782,
                'recipe_id' => 229,
                'dietary_lifestyle_id' => 3,
            ),
            282 => 
            array (
                'id' => 783,
                'recipe_id' => 229,
                'dietary_lifestyle_id' => 4,
            ),
            283 => 
            array (
                'id' => 784,
                'recipe_id' => 229,
                'dietary_lifestyle_id' => 5,
            ),
            284 => 
            array (
                'id' => 785,
                'recipe_id' => 230,
                'dietary_lifestyle_id' => 2,
            ),
            285 => 
            array (
                'id' => 786,
                'recipe_id' => 230,
                'dietary_lifestyle_id' => 3,
            ),
            286 => 
            array (
                'id' => 787,
                'recipe_id' => 230,
                'dietary_lifestyle_id' => 5,
            ),
        ));
        
        
    }
}