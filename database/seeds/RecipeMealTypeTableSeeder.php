<?php

use Illuminate\Database\Seeder;

class RecipeMealTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_meal_type')->delete();
        
        \DB::table('recipe_meal_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'meal_type_id' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 1,
                'meal_type_id' => 5,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 1,
                'meal_type_id' => 4,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 2,
                'meal_type_id' => 3,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 2,
                'meal_type_id' => 6,
            ),
            5 => 
            array (
                'id' => 6,
                'recipe_id' => 3,
                'meal_type_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'recipe_id' => 3,
                'meal_type_id' => 5,
            ),
            7 => 
            array (
                'id' => 8,
                'recipe_id' => 3,
                'meal_type_id' => 4,
            ),
            8 => 
            array (
                'id' => 9,
                'recipe_id' => 4,
                'meal_type_id' => 2,
            ),
            9 => 
            array (
                'id' => 10,
                'recipe_id' => 4,
                'meal_type_id' => 4,
            ),
            10 => 
            array (
                'id' => 11,
                'recipe_id' => 5,
                'meal_type_id' => 2,
            ),
            11 => 
            array (
                'id' => 12,
                'recipe_id' => 5,
                'meal_type_id' => 4,
            ),
            12 => 
            array (
                'id' => 13,
                'recipe_id' => 6,
                'meal_type_id' => 5,
            ),
            13 => 
            array (
                'id' => 14,
                'recipe_id' => 7,
                'meal_type_id' => 3,
            ),
            14 => 
            array (
                'id' => 15,
                'recipe_id' => 7,
                'meal_type_id' => 6,
            ),
            15 => 
            array (
                'id' => 16,
                'recipe_id' => 8,
                'meal_type_id' => 6,
            ),
            16 => 
            array (
                'id' => 17,
                'recipe_id' => 8,
                'meal_type_id' => 3,
            ),
            17 => 
            array (
                'id' => 18,
                'recipe_id' => 9,
                'meal_type_id' => 3,
            ),
            18 => 
            array (
                'id' => 19,
                'recipe_id' => 9,
                'meal_type_id' => 6,
            ),
            19 => 
            array (
                'id' => 20,
                'recipe_id' => 10,
                'meal_type_id' => 3,
            ),
            20 => 
            array (
                'id' => 21,
                'recipe_id' => 10,
                'meal_type_id' => 6,
            ),
            21 => 
            array (
                'id' => 22,
                'recipe_id' => 11,
                'meal_type_id' => 2,
            ),
            22 => 
            array (
                'id' => 23,
                'recipe_id' => 11,
                'meal_type_id' => 4,
            ),
            23 => 
            array (
                'id' => 24,
                'recipe_id' => 12,
                'meal_type_id' => 2,
            ),
            24 => 
            array (
                'id' => 25,
                'recipe_id' => 12,
                'meal_type_id' => 4,
            ),
            25 => 
            array (
                'id' => 26,
                'recipe_id' => 13,
                'meal_type_id' => 5,
            ),
            26 => 
            array (
                'id' => 27,
                'recipe_id' => 14,
                'meal_type_id' => 5,
            ),
            27 => 
            array (
                'id' => 28,
                'recipe_id' => 15,
                'meal_type_id' => 5,
            ),
            28 => 
            array (
                'id' => 29,
                'recipe_id' => 16,
                'meal_type_id' => 3,
            ),
            29 => 
            array (
                'id' => 30,
                'recipe_id' => 16,
                'meal_type_id' => 6,
            ),
            30 => 
            array (
                'id' => 31,
                'recipe_id' => 17,
                'meal_type_id' => 6,
            ),
            31 => 
            array (
                'id' => 32,
                'recipe_id' => 17,
                'meal_type_id' => 3,
            ),
            32 => 
            array (
                'id' => 33,
                'recipe_id' => 17,
                'meal_type_id' => 5,
            ),
            33 => 
            array (
                'id' => 34,
                'recipe_id' => 18,
                'meal_type_id' => 3,
            ),
            34 => 
            array (
                'id' => 35,
                'recipe_id' => 18,
                'meal_type_id' => 6,
            ),
            35 => 
            array (
                'id' => 36,
                'recipe_id' => 19,
                'meal_type_id' => 2,
            ),
            36 => 
            array (
                'id' => 37,
                'recipe_id' => 19,
                'meal_type_id' => 4,
            ),
            37 => 
            array (
                'id' => 38,
                'recipe_id' => 20,
                'meal_type_id' => 2,
            ),
            38 => 
            array (
                'id' => 39,
                'recipe_id' => 20,
                'meal_type_id' => 4,
            ),
            39 => 
            array (
                'id' => 40,
                'recipe_id' => 21,
                'meal_type_id' => 2,
            ),
            40 => 
            array (
                'id' => 41,
                'recipe_id' => 21,
                'meal_type_id' => 5,
            ),
            41 => 
            array (
                'id' => 42,
                'recipe_id' => 21,
                'meal_type_id' => 4,
            ),
            42 => 
            array (
                'id' => 43,
                'recipe_id' => 22,
                'meal_type_id' => 2,
            ),
            43 => 
            array (
                'id' => 44,
                'recipe_id' => 22,
                'meal_type_id' => 3,
            ),
            44 => 
            array (
                'id' => 45,
                'recipe_id' => 22,
                'meal_type_id' => 6,
            ),
            45 => 
            array (
                'id' => 46,
                'recipe_id' => 23,
                'meal_type_id' => 3,
            ),
            46 => 
            array (
                'id' => 47,
                'recipe_id' => 23,
                'meal_type_id' => 6,
            ),
            47 => 
            array (
                'id' => 48,
                'recipe_id' => 24,
                'meal_type_id' => 3,
            ),
            48 => 
            array (
                'id' => 49,
                'recipe_id' => 24,
                'meal_type_id' => 6,
            ),
            49 => 
            array (
                'id' => 50,
                'recipe_id' => 25,
                'meal_type_id' => 2,
            ),
            50 => 
            array (
                'id' => 51,
                'recipe_id' => 25,
                'meal_type_id' => 4,
            ),
            51 => 
            array (
                'id' => 52,
                'recipe_id' => 26,
                'meal_type_id' => 2,
            ),
            52 => 
            array (
                'id' => 53,
                'recipe_id' => 26,
                'meal_type_id' => 4,
            ),
            53 => 
            array (
                'id' => 54,
                'recipe_id' => 27,
                'meal_type_id' => 2,
            ),
            54 => 
            array (
                'id' => 55,
                'recipe_id' => 27,
                'meal_type_id' => 4,
            ),
            55 => 
            array (
                'id' => 56,
                'recipe_id' => 28,
                'meal_type_id' => 2,
            ),
            56 => 
            array (
                'id' => 57,
                'recipe_id' => 29,
                'meal_type_id' => 2,
            ),
            57 => 
            array (
                'id' => 58,
                'recipe_id' => 29,
                'meal_type_id' => 4,
            ),
            58 => 
            array (
                'id' => 59,
                'recipe_id' => 30,
                'meal_type_id' => 3,
            ),
            59 => 
            array (
                'id' => 60,
                'recipe_id' => 30,
                'meal_type_id' => 6,
            ),
            60 => 
            array (
                'id' => 61,
                'recipe_id' => 31,
                'meal_type_id' => 5,
            ),
            61 => 
            array (
                'id' => 62,
                'recipe_id' => 31,
                'meal_type_id' => 6,
            ),
            62 => 
            array (
                'id' => 63,
                'recipe_id' => 31,
                'meal_type_id' => 3,
            ),
            63 => 
            array (
                'id' => 64,
                'recipe_id' => 32,
                'meal_type_id' => 5,
            ),
            64 => 
            array (
                'id' => 65,
                'recipe_id' => 33,
                'meal_type_id' => 6,
            ),
            65 => 
            array (
                'id' => 66,
                'recipe_id' => 33,
                'meal_type_id' => 3,
            ),
            66 => 
            array (
                'id' => 67,
                'recipe_id' => 34,
                'meal_type_id' => 5,
            ),
            67 => 
            array (
                'id' => 68,
                'recipe_id' => 35,
                'meal_type_id' => 5,
            ),
            68 => 
            array (
                'id' => 69,
                'recipe_id' => 36,
                'meal_type_id' => 5,
            ),
            69 => 
            array (
                'id' => 70,
                'recipe_id' => 37,
                'meal_type_id' => 2,
            ),
            70 => 
            array (
                'id' => 71,
                'recipe_id' => 37,
                'meal_type_id' => 4,
            ),
            71 => 
            array (
                'id' => 72,
                'recipe_id' => 38,
                'meal_type_id' => 2,
            ),
            72 => 
            array (
                'id' => 73,
                'recipe_id' => 38,
                'meal_type_id' => 4,
            ),
            73 => 
            array (
                'id' => 74,
                'recipe_id' => 39,
                'meal_type_id' => 3,
            ),
            74 => 
            array (
                'id' => 75,
                'recipe_id' => 39,
                'meal_type_id' => 6,
            ),
            75 => 
            array (
                'id' => 76,
                'recipe_id' => 40,
                'meal_type_id' => 3,
            ),
            76 => 
            array (
                'id' => 77,
                'recipe_id' => 40,
                'meal_type_id' => 6,
            ),
            77 => 
            array (
                'id' => 78,
                'recipe_id' => 41,
                'meal_type_id' => 6,
            ),
            78 => 
            array (
                'id' => 79,
                'recipe_id' => 41,
                'meal_type_id' => 3,
            ),
            79 => 
            array (
                'id' => 80,
                'recipe_id' => 42,
                'meal_type_id' => 3,
            ),
            80 => 
            array (
                'id' => 81,
                'recipe_id' => 42,
                'meal_type_id' => 6,
            ),
            81 => 
            array (
                'id' => 82,
                'recipe_id' => 43,
                'meal_type_id' => 6,
            ),
            82 => 
            array (
                'id' => 83,
                'recipe_id' => 43,
                'meal_type_id' => 3,
            ),
            83 => 
            array (
                'id' => 84,
                'recipe_id' => 44,
                'meal_type_id' => 6,
            ),
            84 => 
            array (
                'id' => 85,
                'recipe_id' => 44,
                'meal_type_id' => 3,
            ),
            85 => 
            array (
                'id' => 86,
                'recipe_id' => 45,
                'meal_type_id' => 3,
            ),
            86 => 
            array (
                'id' => 87,
                'recipe_id' => 45,
                'meal_type_id' => 6,
            ),
            87 => 
            array (
                'id' => 88,
                'recipe_id' => 46,
                'meal_type_id' => 3,
            ),
            88 => 
            array (
                'id' => 89,
                'recipe_id' => 46,
                'meal_type_id' => 6,
            ),
            89 => 
            array (
                'id' => 90,
                'recipe_id' => 47,
                'meal_type_id' => 4,
            ),
            90 => 
            array (
                'id' => 91,
                'recipe_id' => 47,
                'meal_type_id' => 5,
            ),
            91 => 
            array (
                'id' => 92,
                'recipe_id' => 48,
                'meal_type_id' => 2,
            ),
            92 => 
            array (
                'id' => 93,
                'recipe_id' => 48,
                'meal_type_id' => 4,
            ),
            93 => 
            array (
                'id' => 94,
                'recipe_id' => 49,
                'meal_type_id' => 2,
            ),
            94 => 
            array (
                'id' => 95,
                'recipe_id' => 49,
                'meal_type_id' => 4,
            ),
            95 => 
            array (
                'id' => 96,
                'recipe_id' => 50,
                'meal_type_id' => 5,
            ),
            96 => 
            array (
                'id' => 97,
                'recipe_id' => 51,
                'meal_type_id' => 2,
            ),
            97 => 
            array (
                'id' => 98,
                'recipe_id' => 51,
                'meal_type_id' => 5,
            ),
            98 => 
            array (
                'id' => 99,
                'recipe_id' => 51,
                'meal_type_id' => 4,
            ),
            99 => 
            array (
                'id' => 100,
                'recipe_id' => 52,
                'meal_type_id' => 3,
            ),
            100 => 
            array (
                'id' => 101,
                'recipe_id' => 52,
                'meal_type_id' => 6,
            ),
            101 => 
            array (
                'id' => 102,
                'recipe_id' => 53,
                'meal_type_id' => 2,
            ),
            102 => 
            array (
                'id' => 103,
                'recipe_id' => 53,
                'meal_type_id' => 4,
            ),
            103 => 
            array (
                'id' => 104,
                'recipe_id' => 54,
                'meal_type_id' => 6,
            ),
            104 => 
            array (
                'id' => 105,
                'recipe_id' => 54,
                'meal_type_id' => 3,
            ),
            105 => 
            array (
                'id' => 106,
                'recipe_id' => 55,
                'meal_type_id' => 6,
            ),
            106 => 
            array (
                'id' => 107,
                'recipe_id' => 55,
                'meal_type_id' => 3,
            ),
            107 => 
            array (
                'id' => 108,
                'recipe_id' => 56,
                'meal_type_id' => 6,
            ),
            108 => 
            array (
                'id' => 109,
                'recipe_id' => 56,
                'meal_type_id' => 3,
            ),
            109 => 
            array (
                'id' => 110,
                'recipe_id' => 57,
                'meal_type_id' => 6,
            ),
            110 => 
            array (
                'id' => 111,
                'recipe_id' => 57,
                'meal_type_id' => 3,
            ),
            111 => 
            array (
                'id' => 112,
                'recipe_id' => 58,
                'meal_type_id' => 6,
            ),
            112 => 
            array (
                'id' => 113,
                'recipe_id' => 58,
                'meal_type_id' => 3,
            ),
            113 => 
            array (
                'id' => 114,
                'recipe_id' => 59,
                'meal_type_id' => 6,
            ),
            114 => 
            array (
                'id' => 115,
                'recipe_id' => 59,
                'meal_type_id' => 3,
            ),
            115 => 
            array (
                'id' => 116,
                'recipe_id' => 60,
                'meal_type_id' => 6,
            ),
            116 => 
            array (
                'id' => 117,
                'recipe_id' => 60,
                'meal_type_id' => 3,
            ),
            117 => 
            array (
                'id' => 118,
                'recipe_id' => 61,
                'meal_type_id' => 6,
            ),
            118 => 
            array (
                'id' => 119,
                'recipe_id' => 61,
                'meal_type_id' => 3,
            ),
            119 => 
            array (
                'id' => 120,
                'recipe_id' => 62,
                'meal_type_id' => 2,
            ),
            120 => 
            array (
                'id' => 121,
                'recipe_id' => 62,
                'meal_type_id' => 6,
            ),
            121 => 
            array (
                'id' => 122,
                'recipe_id' => 62,
                'meal_type_id' => 3,
            ),
            122 => 
            array (
                'id' => 123,
                'recipe_id' => 62,
                'meal_type_id' => 5,
            ),
            123 => 
            array (
                'id' => 124,
                'recipe_id' => 62,
                'meal_type_id' => 4,
            ),
            124 => 
            array (
                'id' => 125,
                'recipe_id' => 63,
                'meal_type_id' => 2,
            ),
            125 => 
            array (
                'id' => 126,
                'recipe_id' => 63,
                'meal_type_id' => 5,
            ),
            126 => 
            array (
                'id' => 127,
                'recipe_id' => 63,
                'meal_type_id' => 4,
            ),
            127 => 
            array (
                'id' => 128,
                'recipe_id' => 65,
                'meal_type_id' => 5,
            ),
            128 => 
            array (
                'id' => 129,
                'recipe_id' => 65,
                'meal_type_id' => 6,
            ),
            129 => 
            array (
                'id' => 130,
                'recipe_id' => 66,
                'meal_type_id' => 5,
            ),
            130 => 
            array (
                'id' => 131,
                'recipe_id' => 67,
                'meal_type_id' => 6,
            ),
            131 => 
            array (
                'id' => 132,
                'recipe_id' => 67,
                'meal_type_id' => 3,
            ),
            132 => 
            array (
                'id' => 133,
                'recipe_id' => 67,
                'meal_type_id' => 5,
            ),
            133 => 
            array (
                'id' => 134,
                'recipe_id' => 68,
                'meal_type_id' => 5,
            ),
            134 => 
            array (
                'id' => 135,
                'recipe_id' => 69,
                'meal_type_id' => 3,
            ),
            135 => 
            array (
                'id' => 136,
                'recipe_id' => 69,
                'meal_type_id' => 6,
            ),
            136 => 
            array (
                'id' => 137,
                'recipe_id' => 71,
                'meal_type_id' => 6,
            ),
            137 => 
            array (
                'id' => 138,
                'recipe_id' => 71,
                'meal_type_id' => 3,
            ),
            138 => 
            array (
                'id' => 139,
                'recipe_id' => 71,
                'meal_type_id' => 5,
            ),
            139 => 
            array (
                'id' => 140,
                'recipe_id' => 72,
                'meal_type_id' => 2,
            ),
            140 => 
            array (
                'id' => 141,
                'recipe_id' => 72,
                'meal_type_id' => 4,
            ),
            141 => 
            array (
                'id' => 142,
                'recipe_id' => 72,
                'meal_type_id' => 5,
            ),
            142 => 
            array (
                'id' => 143,
                'recipe_id' => 73,
                'meal_type_id' => 5,
            ),
            143 => 
            array (
                'id' => 144,
                'recipe_id' => 73,
                'meal_type_id' => 3,
            ),
            144 => 
            array (
                'id' => 145,
                'recipe_id' => 74,
                'meal_type_id' => 5,
            ),
            145 => 
            array (
                'id' => 146,
                'recipe_id' => 74,
                'meal_type_id' => 6,
            ),
            146 => 
            array (
                'id' => 147,
                'recipe_id' => 75,
                'meal_type_id' => 5,
            ),
            147 => 
            array (
                'id' => 148,
                'recipe_id' => 76,
                'meal_type_id' => 3,
            ),
            148 => 
            array (
                'id' => 149,
                'recipe_id' => 76,
                'meal_type_id' => 6,
            ),
            149 => 
            array (
                'id' => 150,
                'recipe_id' => 77,
                'meal_type_id' => 2,
            ),
            150 => 
            array (
                'id' => 151,
                'recipe_id' => 77,
                'meal_type_id' => 4,
            ),
            151 => 
            array (
                'id' => 152,
                'recipe_id' => 78,
                'meal_type_id' => 2,
            ),
            152 => 
            array (
                'id' => 153,
                'recipe_id' => 78,
                'meal_type_id' => 4,
            ),
            153 => 
            array (
                'id' => 154,
                'recipe_id' => 79,
                'meal_type_id' => 6,
            ),
            154 => 
            array (
                'id' => 155,
                'recipe_id' => 79,
                'meal_type_id' => 3,
            ),
            155 => 
            array (
                'id' => 156,
                'recipe_id' => 80,
                'meal_type_id' => 2,
            ),
            156 => 
            array (
                'id' => 157,
                'recipe_id' => 80,
                'meal_type_id' => 4,
            ),
            157 => 
            array (
                'id' => 158,
                'recipe_id' => 81,
                'meal_type_id' => 2,
            ),
            158 => 
            array (
                'id' => 159,
                'recipe_id' => 81,
                'meal_type_id' => 4,
            ),
            159 => 
            array (
                'id' => 160,
                'recipe_id' => 82,
                'meal_type_id' => 2,
            ),
            160 => 
            array (
                'id' => 161,
                'recipe_id' => 83,
                'meal_type_id' => 2,
            ),
            161 => 
            array (
                'id' => 162,
                'recipe_id' => 84,
                'meal_type_id' => 2,
            ),
            162 => 
            array (
                'id' => 163,
                'recipe_id' => 85,
                'meal_type_id' => 6,
            ),
            163 => 
            array (
                'id' => 164,
                'recipe_id' => 85,
                'meal_type_id' => 3,
            ),
            164 => 
            array (
                'id' => 165,
                'recipe_id' => 86,
                'meal_type_id' => 3,
            ),
            165 => 
            array (
                'id' => 166,
                'recipe_id' => 86,
                'meal_type_id' => 6,
            ),
            166 => 
            array (
                'id' => 167,
                'recipe_id' => 87,
                'meal_type_id' => 2,
            ),
            167 => 
            array (
                'id' => 168,
                'recipe_id' => 88,
                'meal_type_id' => 6,
            ),
            168 => 
            array (
                'id' => 169,
                'recipe_id' => 88,
                'meal_type_id' => 3,
            ),
            169 => 
            array (
                'id' => 170,
                'recipe_id' => 89,
                'meal_type_id' => 2,
            ),
            170 => 
            array (
                'id' => 171,
                'recipe_id' => 90,
                'meal_type_id' => 3,
            ),
            171 => 
            array (
                'id' => 172,
                'recipe_id' => 90,
                'meal_type_id' => 6,
            ),
            172 => 
            array (
                'id' => 173,
                'recipe_id' => 91,
                'meal_type_id' => 6,
            ),
            173 => 
            array (
                'id' => 174,
                'recipe_id' => 91,
                'meal_type_id' => 3,
            ),
            174 => 
            array (
                'id' => 175,
                'recipe_id' => 92,
                'meal_type_id' => 6,
            ),
            175 => 
            array (
                'id' => 176,
                'recipe_id' => 92,
                'meal_type_id' => 3,
            ),
            176 => 
            array (
                'id' => 177,
                'recipe_id' => 92,
                'meal_type_id' => 5,
            ),
            177 => 
            array (
                'id' => 178,
                'recipe_id' => 93,
                'meal_type_id' => 6,
            ),
            178 => 
            array (
                'id' => 179,
                'recipe_id' => 93,
                'meal_type_id' => 3,
            ),
            179 => 
            array (
                'id' => 180,
                'recipe_id' => 94,
                'meal_type_id' => 3,
            ),
            180 => 
            array (
                'id' => 181,
                'recipe_id' => 94,
                'meal_type_id' => 6,
            ),
            181 => 
            array (
                'id' => 182,
                'recipe_id' => 95,
                'meal_type_id' => 6,
            ),
            182 => 
            array (
                'id' => 183,
                'recipe_id' => 95,
                'meal_type_id' => 5,
            ),
            183 => 
            array (
                'id' => 184,
                'recipe_id' => 95,
                'meal_type_id' => 2,
            ),
            184 => 
            array (
                'id' => 185,
                'recipe_id' => 95,
                'meal_type_id' => 4,
            ),
            185 => 
            array (
                'id' => 186,
                'recipe_id' => 96,
                'meal_type_id' => 3,
            ),
            186 => 
            array (
                'id' => 187,
                'recipe_id' => 96,
                'meal_type_id' => 6,
            ),
            187 => 
            array (
                'id' => 188,
                'recipe_id' => 96,
                'meal_type_id' => 5,
            ),
            188 => 
            array (
                'id' => 189,
                'recipe_id' => 97,
                'meal_type_id' => 6,
            ),
            189 => 
            array (
                'id' => 190,
                'recipe_id' => 97,
                'meal_type_id' => 3,
            ),
            190 => 
            array (
                'id' => 191,
                'recipe_id' => 98,
                'meal_type_id' => 2,
            ),
            191 => 
            array (
                'id' => 192,
                'recipe_id' => 98,
                'meal_type_id' => 4,
            ),
            192 => 
            array (
                'id' => 193,
                'recipe_id' => 99,
                'meal_type_id' => 2,
            ),
            193 => 
            array (
                'id' => 194,
                'recipe_id' => 99,
                'meal_type_id' => 4,
            ),
            194 => 
            array (
                'id' => 195,
                'recipe_id' => 100,
                'meal_type_id' => 2,
            ),
            195 => 
            array (
                'id' => 196,
                'recipe_id' => 100,
                'meal_type_id' => 4,
            ),
            196 => 
            array (
                'id' => 197,
                'recipe_id' => 101,
                'meal_type_id' => 2,
            ),
            197 => 
            array (
                'id' => 198,
                'recipe_id' => 101,
                'meal_type_id' => 4,
            ),
            198 => 
            array (
                'id' => 199,
                'recipe_id' => 102,
                'meal_type_id' => 3,
            ),
            199 => 
            array (
                'id' => 200,
                'recipe_id' => 102,
                'meal_type_id' => 6,
            ),
            200 => 
            array (
                'id' => 201,
                'recipe_id' => 103,
                'meal_type_id' => 2,
            ),
            201 => 
            array (
                'id' => 202,
                'recipe_id' => 103,
                'meal_type_id' => 4,
            ),
            202 => 
            array (
                'id' => 203,
                'recipe_id' => 104,
                'meal_type_id' => 2,
            ),
            203 => 
            array (
                'id' => 204,
                'recipe_id' => 104,
                'meal_type_id' => 4,
            ),
            204 => 
            array (
                'id' => 205,
                'recipe_id' => 105,
                'meal_type_id' => 2,
            ),
            205 => 
            array (
                'id' => 206,
                'recipe_id' => 105,
                'meal_type_id' => 4,
            ),
            206 => 
            array (
                'id' => 207,
                'recipe_id' => 106,
                'meal_type_id' => 3,
            ),
            207 => 
            array (
                'id' => 208,
                'recipe_id' => 106,
                'meal_type_id' => 6,
            ),
            208 => 
            array (
                'id' => 209,
                'recipe_id' => 107,
                'meal_type_id' => 2,
            ),
            209 => 
            array (
                'id' => 210,
                'recipe_id' => 107,
                'meal_type_id' => 4,
            ),
            210 => 
            array (
                'id' => 211,
                'recipe_id' => 108,
                'meal_type_id' => 6,
            ),
            211 => 
            array (
                'id' => 212,
                'recipe_id' => 108,
                'meal_type_id' => 3,
            ),
            212 => 
            array (
                'id' => 213,
                'recipe_id' => 109,
                'meal_type_id' => 3,
            ),
            213 => 
            array (
                'id' => 214,
                'recipe_id' => 109,
                'meal_type_id' => 6,
            ),
            214 => 
            array (
                'id' => 215,
                'recipe_id' => 110,
                'meal_type_id' => 6,
            ),
            215 => 
            array (
                'id' => 216,
                'recipe_id' => 110,
                'meal_type_id' => 3,
            ),
            216 => 
            array (
                'id' => 217,
                'recipe_id' => 111,
                'meal_type_id' => 2,
            ),
            217 => 
            array (
                'id' => 218,
                'recipe_id' => 111,
                'meal_type_id' => 6,
            ),
            218 => 
            array (
                'id' => 219,
                'recipe_id' => 111,
                'meal_type_id' => 3,
            ),
            219 => 
            array (
                'id' => 220,
                'recipe_id' => 111,
                'meal_type_id' => 5,
            ),
            220 => 
            array (
                'id' => 221,
                'recipe_id' => 111,
                'meal_type_id' => 4,
            ),
            221 => 
            array (
                'id' => 222,
                'recipe_id' => 112,
                'meal_type_id' => 6,
            ),
            222 => 
            array (
                'id' => 223,
                'recipe_id' => 112,
                'meal_type_id' => 3,
            ),
            223 => 
            array (
                'id' => 224,
                'recipe_id' => 113,
                'meal_type_id' => 2,
            ),
            224 => 
            array (
                'id' => 225,
                'recipe_id' => 113,
                'meal_type_id' => 4,
            ),
            225 => 
            array (
                'id' => 226,
                'recipe_id' => 114,
                'meal_type_id' => 6,
            ),
            226 => 
            array (
                'id' => 227,
                'recipe_id' => 114,
                'meal_type_id' => 3,
            ),
            227 => 
            array (
                'id' => 228,
                'recipe_id' => 115,
                'meal_type_id' => 2,
            ),
            228 => 
            array (
                'id' => 229,
                'recipe_id' => 115,
                'meal_type_id' => 5,
            ),
            229 => 
            array (
                'id' => 230,
                'recipe_id' => 115,
                'meal_type_id' => 4,
            ),
            230 => 
            array (
                'id' => 231,
                'recipe_id' => 116,
                'meal_type_id' => 2,
            ),
            231 => 
            array (
                'id' => 232,
                'recipe_id' => 116,
                'meal_type_id' => 4,
            ),
            232 => 
            array (
                'id' => 233,
                'recipe_id' => 117,
                'meal_type_id' => 2,
            ),
            233 => 
            array (
                'id' => 234,
                'recipe_id' => 118,
                'meal_type_id' => 6,
            ),
            234 => 
            array (
                'id' => 235,
                'recipe_id' => 118,
                'meal_type_id' => 3,
            ),
            235 => 
            array (
                'id' => 236,
                'recipe_id' => 120,
                'meal_type_id' => 6,
            ),
            236 => 
            array (
                'id' => 237,
                'recipe_id' => 120,
                'meal_type_id' => 3,
            ),
            237 => 
            array (
                'id' => 238,
                'recipe_id' => 121,
                'meal_type_id' => 5,
            ),
            238 => 
            array (
                'id' => 239,
                'recipe_id' => 122,
                'meal_type_id' => 6,
            ),
            239 => 
            array (
                'id' => 240,
                'recipe_id' => 122,
                'meal_type_id' => 3,
            ),
            240 => 
            array (
                'id' => 241,
                'recipe_id' => 123,
                'meal_type_id' => 6,
            ),
            241 => 
            array (
                'id' => 242,
                'recipe_id' => 123,
                'meal_type_id' => 3,
            ),
            242 => 
            array (
                'id' => 243,
                'recipe_id' => 124,
                'meal_type_id' => 6,
            ),
            243 => 
            array (
                'id' => 244,
                'recipe_id' => 124,
                'meal_type_id' => 3,
            ),
            244 => 
            array (
                'id' => 245,
                'recipe_id' => 125,
                'meal_type_id' => 3,
            ),
            245 => 
            array (
                'id' => 246,
                'recipe_id' => 125,
                'meal_type_id' => 6,
            ),
            246 => 
            array (
                'id' => 247,
                'recipe_id' => 126,
                'meal_type_id' => 3,
            ),
            247 => 
            array (
                'id' => 248,
                'recipe_id' => 126,
                'meal_type_id' => 6,
            ),
            248 => 
            array (
                'id' => 249,
                'recipe_id' => 127,
                'meal_type_id' => 6,
            ),
            249 => 
            array (
                'id' => 250,
                'recipe_id' => 127,
                'meal_type_id' => 3,
            ),
            250 => 
            array (
                'id' => 251,
                'recipe_id' => 128,
                'meal_type_id' => 3,
            ),
            251 => 
            array (
                'id' => 252,
                'recipe_id' => 128,
                'meal_type_id' => 6,
            ),
            252 => 
            array (
                'id' => 253,
                'recipe_id' => 129,
                'meal_type_id' => 3,
            ),
            253 => 
            array (
                'id' => 254,
                'recipe_id' => 129,
                'meal_type_id' => 6,
            ),
            254 => 
            array (
                'id' => 255,
                'recipe_id' => 130,
                'meal_type_id' => 6,
            ),
            255 => 
            array (
                'id' => 256,
                'recipe_id' => 130,
                'meal_type_id' => 3,
            ),
            256 => 
            array (
                'id' => 257,
                'recipe_id' => 131,
                'meal_type_id' => 3,
            ),
            257 => 
            array (
                'id' => 258,
                'recipe_id' => 131,
                'meal_type_id' => 6,
            ),
            258 => 
            array (
                'id' => 259,
                'recipe_id' => 132,
                'meal_type_id' => 2,
            ),
            259 => 
            array (
                'id' => 260,
                'recipe_id' => 133,
                'meal_type_id' => 2,
            ),
            260 => 
            array (
                'id' => 261,
                'recipe_id' => 133,
                'meal_type_id' => 6,
            ),
            261 => 
            array (
                'id' => 262,
                'recipe_id' => 133,
                'meal_type_id' => 3,
            ),
            262 => 
            array (
                'id' => 263,
                'recipe_id' => 133,
                'meal_type_id' => 5,
            ),
            263 => 
            array (
                'id' => 264,
                'recipe_id' => 133,
                'meal_type_id' => 4,
            ),
            264 => 
            array (
                'id' => 265,
                'recipe_id' => 134,
                'meal_type_id' => 2,
            ),
            265 => 
            array (
                'id' => 266,
                'recipe_id' => 134,
                'meal_type_id' => 5,
            ),
            266 => 
            array (
                'id' => 267,
                'recipe_id' => 134,
                'meal_type_id' => 4,
            ),
            267 => 
            array (
                'id' => 268,
                'recipe_id' => 135,
                'meal_type_id' => 3,
            ),
            268 => 
            array (
                'id' => 269,
                'recipe_id' => 135,
                'meal_type_id' => 6,
            ),
            269 => 
            array (
                'id' => 270,
                'recipe_id' => 136,
                'meal_type_id' => 6,
            ),
            270 => 
            array (
                'id' => 271,
                'recipe_id' => 136,
                'meal_type_id' => 3,
            ),
            271 => 
            array (
                'id' => 272,
                'recipe_id' => 137,
                'meal_type_id' => 5,
            ),
            272 => 
            array (
                'id' => 273,
                'recipe_id' => 138,
                'meal_type_id' => 6,
            ),
            273 => 
            array (
                'id' => 274,
                'recipe_id' => 138,
                'meal_type_id' => 5,
            ),
            274 => 
            array (
                'id' => 275,
                'recipe_id' => 138,
                'meal_type_id' => 3,
            ),
            275 => 
            array (
                'id' => 276,
                'recipe_id' => 138,
                'meal_type_id' => 2,
            ),
            276 => 
            array (
                'id' => 277,
                'recipe_id' => 138,
                'meal_type_id' => 4,
            ),
            277 => 
            array (
                'id' => 278,
                'recipe_id' => 139,
                'meal_type_id' => 2,
            ),
            278 => 
            array (
                'id' => 279,
                'recipe_id' => 139,
                'meal_type_id' => 6,
            ),
            279 => 
            array (
                'id' => 280,
                'recipe_id' => 139,
                'meal_type_id' => 3,
            ),
            280 => 
            array (
                'id' => 281,
                'recipe_id' => 139,
                'meal_type_id' => 5,
            ),
            281 => 
            array (
                'id' => 282,
                'recipe_id' => 139,
                'meal_type_id' => 4,
            ),
            282 => 
            array (
                'id' => 283,
                'recipe_id' => 140,
                'meal_type_id' => 6,
            ),
            283 => 
            array (
                'id' => 284,
                'recipe_id' => 140,
                'meal_type_id' => 3,
            ),
            284 => 
            array (
                'id' => 285,
                'recipe_id' => 141,
                'meal_type_id' => 5,
            ),
            285 => 
            array (
                'id' => 286,
                'recipe_id' => 141,
                'meal_type_id' => 4,
            ),
            286 => 
            array (
                'id' => 287,
                'recipe_id' => 142,
                'meal_type_id' => 2,
            ),
            287 => 
            array (
                'id' => 288,
                'recipe_id' => 142,
                'meal_type_id' => 4,
            ),
            288 => 
            array (
                'id' => 289,
                'recipe_id' => 143,
                'meal_type_id' => 2,
            ),
            289 => 
            array (
                'id' => 290,
                'recipe_id' => 144,
                'meal_type_id' => 3,
            ),
            290 => 
            array (
                'id' => 291,
                'recipe_id' => 144,
                'meal_type_id' => 6,
            ),
            291 => 
            array (
                'id' => 292,
                'recipe_id' => 145,
                'meal_type_id' => 6,
            ),
            292 => 
            array (
                'id' => 293,
                'recipe_id' => 145,
                'meal_type_id' => 3,
            ),
            293 => 
            array (
                'id' => 294,
                'recipe_id' => 146,
                'meal_type_id' => 6,
            ),
            294 => 
            array (
                'id' => 295,
                'recipe_id' => 146,
                'meal_type_id' => 3,
            ),
            295 => 
            array (
                'id' => 296,
                'recipe_id' => 147,
                'meal_type_id' => 3,
            ),
            296 => 
            array (
                'id' => 297,
                'recipe_id' => 147,
                'meal_type_id' => 6,
            ),
            297 => 
            array (
                'id' => 298,
                'recipe_id' => 148,
                'meal_type_id' => 2,
            ),
            298 => 
            array (
                'id' => 299,
                'recipe_id' => 149,
                'meal_type_id' => 2,
            ),
            299 => 
            array (
                'id' => 300,
                'recipe_id' => 149,
                'meal_type_id' => 6,
            ),
            300 => 
            array (
                'id' => 301,
                'recipe_id' => 149,
                'meal_type_id' => 3,
            ),
            301 => 
            array (
                'id' => 302,
                'recipe_id' => 150,
                'meal_type_id' => 6,
            ),
            302 => 
            array (
                'id' => 303,
                'recipe_id' => 150,
                'meal_type_id' => 3,
            ),
            303 => 
            array (
                'id' => 304,
                'recipe_id' => 151,
                'meal_type_id' => 3,
            ),
            304 => 
            array (
                'id' => 305,
                'recipe_id' => 151,
                'meal_type_id' => 6,
            ),
            305 => 
            array (
                'id' => 306,
                'recipe_id' => 152,
                'meal_type_id' => 5,
            ),
            306 => 
            array (
                'id' => 307,
                'recipe_id' => 152,
                'meal_type_id' => 4,
            ),
            307 => 
            array (
                'id' => 308,
                'recipe_id' => 153,
                'meal_type_id' => 2,
            ),
            308 => 
            array (
                'id' => 309,
                'recipe_id' => 153,
                'meal_type_id' => 5,
            ),
            309 => 
            array (
                'id' => 310,
                'recipe_id' => 153,
                'meal_type_id' => 4,
            ),
            310 => 
            array (
                'id' => 311,
                'recipe_id' => 154,
                'meal_type_id' => 2,
            ),
            311 => 
            array (
                'id' => 312,
                'recipe_id' => 155,
                'meal_type_id' => 5,
            ),
            312 => 
            array (
                'id' => 313,
                'recipe_id' => 155,
                'meal_type_id' => 2,
            ),
            313 => 
            array (
                'id' => 314,
                'recipe_id' => 155,
                'meal_type_id' => 6,
            ),
            314 => 
            array (
                'id' => 315,
                'recipe_id' => 155,
                'meal_type_id' => 3,
            ),
            315 => 
            array (
                'id' => 316,
                'recipe_id' => 156,
                'meal_type_id' => 3,
            ),
            316 => 
            array (
                'id' => 317,
                'recipe_id' => 156,
                'meal_type_id' => 6,
            ),
            317 => 
            array (
                'id' => 318,
                'recipe_id' => 157,
                'meal_type_id' => 6,
            ),
            318 => 
            array (
                'id' => 319,
                'recipe_id' => 157,
                'meal_type_id' => 5,
            ),
            319 => 
            array (
                'id' => 320,
                'recipe_id' => 158,
                'meal_type_id' => 2,
            ),
            320 => 
            array (
                'id' => 321,
                'recipe_id' => 158,
                'meal_type_id' => 3,
            ),
            321 => 
            array (
                'id' => 322,
                'recipe_id' => 159,
                'meal_type_id' => 2,
            ),
            322 => 
            array (
                'id' => 323,
                'recipe_id' => 159,
                'meal_type_id' => 4,
            ),
            323 => 
            array (
                'id' => 324,
                'recipe_id' => 160,
                'meal_type_id' => 3,
            ),
            324 => 
            array (
                'id' => 325,
                'recipe_id' => 160,
                'meal_type_id' => 6,
            ),
            325 => 
            array (
                'id' => 326,
                'recipe_id' => 161,
                'meal_type_id' => 2,
            ),
            326 => 
            array (
                'id' => 327,
                'recipe_id' => 161,
                'meal_type_id' => 4,
            ),
            327 => 
            array (
                'id' => 328,
                'recipe_id' => 162,
                'meal_type_id' => 6,
            ),
            328 => 
            array (
                'id' => 329,
                'recipe_id' => 162,
                'meal_type_id' => 3,
            ),
            329 => 
            array (
                'id' => 330,
                'recipe_id' => 163,
                'meal_type_id' => 6,
            ),
            330 => 
            array (
                'id' => 331,
                'recipe_id' => 163,
                'meal_type_id' => 3,
            ),
            331 => 
            array (
                'id' => 332,
                'recipe_id' => 164,
                'meal_type_id' => 6,
            ),
            332 => 
            array (
                'id' => 333,
                'recipe_id' => 164,
                'meal_type_id' => 5,
            ),
            333 => 
            array (
                'id' => 334,
                'recipe_id' => 165,
                'meal_type_id' => 2,
            ),
            334 => 
            array (
                'id' => 335,
                'recipe_id' => 165,
                'meal_type_id' => 3,
            ),
            335 => 
            array (
                'id' => 336,
                'recipe_id' => 165,
                'meal_type_id' => 6,
            ),
            336 => 
            array (
                'id' => 337,
                'recipe_id' => 165,
                'meal_type_id' => 5,
            ),
            337 => 
            array (
                'id' => 338,
                'recipe_id' => 165,
                'meal_type_id' => 4,
            ),
            338 => 
            array (
                'id' => 339,
                'recipe_id' => 166,
                'meal_type_id' => 6,
            ),
            339 => 
            array (
                'id' => 340,
                'recipe_id' => 166,
                'meal_type_id' => 3,
            ),
            340 => 
            array (
                'id' => 341,
                'recipe_id' => 167,
                'meal_type_id' => 6,
            ),
            341 => 
            array (
                'id' => 342,
                'recipe_id' => 167,
                'meal_type_id' => 3,
            ),
            342 => 
            array (
                'id' => 343,
                'recipe_id' => 168,
                'meal_type_id' => 3,
            ),
            343 => 
            array (
                'id' => 344,
                'recipe_id' => 169,
                'meal_type_id' => 3,
            ),
            344 => 
            array (
                'id' => 345,
                'recipe_id' => 169,
                'meal_type_id' => 6,
            ),
            345 => 
            array (
                'id' => 346,
                'recipe_id' => 170,
                'meal_type_id' => 6,
            ),
            346 => 
            array (
                'id' => 347,
                'recipe_id' => 170,
                'meal_type_id' => 3,
            ),
            347 => 
            array (
                'id' => 348,
                'recipe_id' => 171,
                'meal_type_id' => 5,
            ),
            348 => 
            array (
                'id' => 349,
                'recipe_id' => 171,
                'meal_type_id' => 3,
            ),
            349 => 
            array (
                'id' => 350,
                'recipe_id' => 171,
                'meal_type_id' => 6,
            ),
            350 => 
            array (
                'id' => 351,
                'recipe_id' => 172,
                'meal_type_id' => 3,
            ),
            351 => 
            array (
                'id' => 352,
                'recipe_id' => 172,
                'meal_type_id' => 6,
            ),
            352 => 
            array (
                'id' => 353,
                'recipe_id' => 173,
                'meal_type_id' => 2,
            ),
            353 => 
            array (
                'id' => 354,
                'recipe_id' => 174,
                'meal_type_id' => 6,
            ),
            354 => 
            array (
                'id' => 355,
                'recipe_id' => 174,
                'meal_type_id' => 3,
            ),
            355 => 
            array (
                'id' => 356,
                'recipe_id' => 176,
                'meal_type_id' => 6,
            ),
            356 => 
            array (
                'id' => 357,
                'recipe_id' => 176,
                'meal_type_id' => 3,
            ),
            357 => 
            array (
                'id' => 358,
                'recipe_id' => 177,
                'meal_type_id' => 2,
            ),
            358 => 
            array (
                'id' => 359,
                'recipe_id' => 177,
                'meal_type_id' => 4,
            ),
            359 => 
            array (
                'id' => 360,
                'recipe_id' => 178,
                'meal_type_id' => 2,
            ),
            360 => 
            array (
                'id' => 361,
                'recipe_id' => 178,
                'meal_type_id' => 4,
            ),
            361 => 
            array (
                'id' => 362,
                'recipe_id' => 179,
                'meal_type_id' => 6,
            ),
            362 => 
            array (
                'id' => 363,
                'recipe_id' => 179,
                'meal_type_id' => 3,
            ),
            363 => 
            array (
                'id' => 364,
                'recipe_id' => 180,
                'meal_type_id' => 3,
            ),
            364 => 
            array (
                'id' => 365,
                'recipe_id' => 180,
                'meal_type_id' => 6,
            ),
            365 => 
            array (
                'id' => 366,
                'recipe_id' => 181,
                'meal_type_id' => 6,
            ),
            366 => 
            array (
                'id' => 367,
                'recipe_id' => 181,
                'meal_type_id' => 3,
            ),
            367 => 
            array (
                'id' => 368,
                'recipe_id' => 182,
                'meal_type_id' => 6,
            ),
            368 => 
            array (
                'id' => 369,
                'recipe_id' => 182,
                'meal_type_id' => 3,
            ),
            369 => 
            array (
                'id' => 370,
                'recipe_id' => 183,
                'meal_type_id' => 6,
            ),
            370 => 
            array (
                'id' => 371,
                'recipe_id' => 183,
                'meal_type_id' => 3,
            ),
            371 => 
            array (
                'id' => 372,
                'recipe_id' => 184,
                'meal_type_id' => 3,
            ),
            372 => 
            array (
                'id' => 373,
                'recipe_id' => 184,
                'meal_type_id' => 6,
            ),
            373 => 
            array (
                'id' => 374,
                'recipe_id' => 186,
                'meal_type_id' => 6,
            ),
            374 => 
            array (
                'id' => 375,
                'recipe_id' => 186,
                'meal_type_id' => 3,
            ),
            375 => 
            array (
                'id' => 376,
                'recipe_id' => 187,
                'meal_type_id' => 6,
            ),
            376 => 
            array (
                'id' => 377,
                'recipe_id' => 187,
                'meal_type_id' => 3,
            ),
            377 => 
            array (
                'id' => 378,
                'recipe_id' => 188,
                'meal_type_id' => 6,
            ),
            378 => 
            array (
                'id' => 379,
                'recipe_id' => 188,
                'meal_type_id' => 3,
            ),
            379 => 
            array (
                'id' => 380,
                'recipe_id' => 189,
                'meal_type_id' => 3,
            ),
            380 => 
            array (
                'id' => 381,
                'recipe_id' => 189,
                'meal_type_id' => 6,
            ),
            381 => 
            array (
                'id' => 382,
                'recipe_id' => 190,
                'meal_type_id' => 6,
            ),
            382 => 
            array (
                'id' => 383,
                'recipe_id' => 190,
                'meal_type_id' => 3,
            ),
            383 => 
            array (
                'id' => 384,
                'recipe_id' => 191,
                'meal_type_id' => 5,
            ),
            384 => 
            array (
                'id' => 385,
                'recipe_id' => 191,
                'meal_type_id' => 6,
            ),
            385 => 
            array (
                'id' => 386,
                'recipe_id' => 192,
                'meal_type_id' => 3,
            ),
            386 => 
            array (
                'id' => 387,
                'recipe_id' => 192,
                'meal_type_id' => 6,
            ),
            387 => 
            array (
                'id' => 388,
                'recipe_id' => 193,
                'meal_type_id' => 2,
            ),
            388 => 
            array (
                'id' => 389,
                'recipe_id' => 193,
                'meal_type_id' => 6,
            ),
            389 => 
            array (
                'id' => 390,
                'recipe_id' => 193,
                'meal_type_id' => 5,
            ),
            390 => 
            array (
                'id' => 391,
                'recipe_id' => 193,
                'meal_type_id' => 4,
            ),
            391 => 
            array (
                'id' => 392,
                'recipe_id' => 194,
                'meal_type_id' => 2,
            ),
            392 => 
            array (
                'id' => 393,
                'recipe_id' => 194,
                'meal_type_id' => 5,
            ),
            393 => 
            array (
                'id' => 394,
                'recipe_id' => 194,
                'meal_type_id' => 4,
            ),
            394 => 
            array (
                'id' => 395,
                'recipe_id' => 195,
                'meal_type_id' => 2,
            ),
            395 => 
            array (
                'id' => 396,
                'recipe_id' => 196,
                'meal_type_id' => 3,
            ),
            396 => 
            array (
                'id' => 397,
                'recipe_id' => 196,
                'meal_type_id' => 6,
            ),
            397 => 
            array (
                'id' => 398,
                'recipe_id' => 197,
                'meal_type_id' => 3,
            ),
            398 => 
            array (
                'id' => 399,
                'recipe_id' => 197,
                'meal_type_id' => 6,
            ),
            399 => 
            array (
                'id' => 400,
                'recipe_id' => 198,
                'meal_type_id' => 3,
            ),
            400 => 
            array (
                'id' => 401,
                'recipe_id' => 198,
                'meal_type_id' => 6,
            ),
            401 => 
            array (
                'id' => 402,
                'recipe_id' => 199,
                'meal_type_id' => 6,
            ),
            402 => 
            array (
                'id' => 403,
                'recipe_id' => 199,
                'meal_type_id' => 3,
            ),
            403 => 
            array (
                'id' => 404,
                'recipe_id' => 200,
                'meal_type_id' => 3,
            ),
            404 => 
            array (
                'id' => 405,
                'recipe_id' => 200,
                'meal_type_id' => 6,
            ),
            405 => 
            array (
                'id' => 406,
                'recipe_id' => 201,
                'meal_type_id' => 6,
            ),
            406 => 
            array (
                'id' => 407,
                'recipe_id' => 201,
                'meal_type_id' => 3,
            ),
            407 => 
            array (
                'id' => 408,
                'recipe_id' => 202,
                'meal_type_id' => 6,
            ),
            408 => 
            array (
                'id' => 409,
                'recipe_id' => 202,
                'meal_type_id' => 3,
            ),
            409 => 
            array (
                'id' => 410,
                'recipe_id' => 203,
                'meal_type_id' => 2,
            ),
            410 => 
            array (
                'id' => 411,
                'recipe_id' => 203,
                'meal_type_id' => 6,
            ),
            411 => 
            array (
                'id' => 412,
                'recipe_id' => 203,
                'meal_type_id' => 5,
            ),
            412 => 
            array (
                'id' => 413,
                'recipe_id' => 203,
                'meal_type_id' => 4,
            ),
            413 => 
            array (
                'id' => 414,
                'recipe_id' => 204,
                'meal_type_id' => 5,
            ),
            414 => 
            array (
                'id' => 415,
                'recipe_id' => 204,
                'meal_type_id' => 6,
            ),
            415 => 
            array (
                'id' => 416,
                'recipe_id' => 205,
                'meal_type_id' => 3,
            ),
            416 => 
            array (
                'id' => 417,
                'recipe_id' => 205,
                'meal_type_id' => 6,
            ),
            417 => 
            array (
                'id' => 418,
                'recipe_id' => 206,
                'meal_type_id' => 3,
            ),
            418 => 
            array (
                'id' => 419,
                'recipe_id' => 206,
                'meal_type_id' => 6,
            ),
            419 => 
            array (
                'id' => 420,
                'recipe_id' => 207,
                'meal_type_id' => 5,
            ),
            420 => 
            array (
                'id' => 421,
                'recipe_id' => 207,
                'meal_type_id' => 4,
            ),
            421 => 
            array (
                'id' => 422,
                'recipe_id' => 207,
                'meal_type_id' => 2,
            ),
            422 => 
            array (
                'id' => 423,
                'recipe_id' => 207,
                'meal_type_id' => 6,
            ),
            423 => 
            array (
                'id' => 424,
                'recipe_id' => 208,
                'meal_type_id' => 6,
            ),
            424 => 
            array (
                'id' => 425,
                'recipe_id' => 208,
                'meal_type_id' => 2,
            ),
            425 => 
            array (
                'id' => 426,
                'recipe_id' => 208,
                'meal_type_id' => 3,
            ),
            426 => 
            array (
                'id' => 427,
                'recipe_id' => 208,
                'meal_type_id' => 5,
            ),
            427 => 
            array (
                'id' => 428,
                'recipe_id' => 208,
                'meal_type_id' => 4,
            ),
            428 => 
            array (
                'id' => 429,
                'recipe_id' => 209,
                'meal_type_id' => 6,
            ),
            429 => 
            array (
                'id' => 430,
                'recipe_id' => 209,
                'meal_type_id' => 3,
            ),
            430 => 
            array (
                'id' => 431,
                'recipe_id' => 210,
                'meal_type_id' => 6,
            ),
            431 => 
            array (
                'id' => 432,
                'recipe_id' => 210,
                'meal_type_id' => 3,
            ),
            432 => 
            array (
                'id' => 433,
                'recipe_id' => 211,
                'meal_type_id' => 5,
            ),
            433 => 
            array (
                'id' => 434,
                'recipe_id' => 212,
                'meal_type_id' => 3,
            ),
            434 => 
            array (
                'id' => 435,
                'recipe_id' => 212,
                'meal_type_id' => 6,
            ),
            435 => 
            array (
                'id' => 436,
                'recipe_id' => 213,
                'meal_type_id' => 3,
            ),
            436 => 
            array (
                'id' => 437,
                'recipe_id' => 213,
                'meal_type_id' => 6,
            ),
            437 => 
            array (
                'id' => 438,
                'recipe_id' => 214,
                'meal_type_id' => 6,
            ),
            438 => 
            array (
                'id' => 439,
                'recipe_id' => 214,
                'meal_type_id' => 3,
            ),
            439 => 
            array (
                'id' => 440,
                'recipe_id' => 215,
                'meal_type_id' => 6,
            ),
            440 => 
            array (
                'id' => 441,
                'recipe_id' => 215,
                'meal_type_id' => 3,
            ),
            441 => 
            array (
                'id' => 442,
                'recipe_id' => 217,
                'meal_type_id' => 3,
            ),
            442 => 
            array (
                'id' => 443,
                'recipe_id' => 217,
                'meal_type_id' => 6,
            ),
            443 => 
            array (
                'id' => 444,
                'recipe_id' => 218,
                'meal_type_id' => 6,
            ),
            444 => 
            array (
                'id' => 445,
                'recipe_id' => 218,
                'meal_type_id' => 3,
            ),
            445 => 
            array (
                'id' => 446,
                'recipe_id' => 218,
                'meal_type_id' => 5,
            ),
            446 => 
            array (
                'id' => 447,
                'recipe_id' => 219,
                'meal_type_id' => 6,
            ),
            447 => 
            array (
                'id' => 448,
                'recipe_id' => 219,
                'meal_type_id' => 3,
            ),
            448 => 
            array (
                'id' => 449,
                'recipe_id' => 219,
                'meal_type_id' => 5,
            ),
            449 => 
            array (
                'id' => 450,
                'recipe_id' => 220,
                'meal_type_id' => 6,
            ),
            450 => 
            array (
                'id' => 451,
                'recipe_id' => 220,
                'meal_type_id' => 3,
            ),
            451 => 
            array (
                'id' => 452,
                'recipe_id' => 221,
                'meal_type_id' => 2,
            ),
            452 => 
            array (
                'id' => 453,
                'recipe_id' => 221,
                'meal_type_id' => 4,
            ),
            453 => 
            array (
                'id' => 454,
                'recipe_id' => 222,
                'meal_type_id' => 5,
            ),
            454 => 
            array (
                'id' => 455,
                'recipe_id' => 222,
                'meal_type_id' => 4,
            ),
            455 => 
            array (
                'id' => 456,
                'recipe_id' => 222,
                'meal_type_id' => 6,
            ),
            456 => 
            array (
                'id' => 457,
                'recipe_id' => 222,
                'meal_type_id' => 2,
            ),
            457 => 
            array (
                'id' => 458,
                'recipe_id' => 222,
                'meal_type_id' => 3,
            ),
            458 => 
            array (
                'id' => 459,
                'recipe_id' => 223,
                'meal_type_id' => 6,
            ),
            459 => 
            array (
                'id' => 460,
                'recipe_id' => 223,
                'meal_type_id' => 3,
            ),
            460 => 
            array (
                'id' => 461,
                'recipe_id' => 224,
                'meal_type_id' => 6,
            ),
            461 => 
            array (
                'id' => 462,
                'recipe_id' => 224,
                'meal_type_id' => 3,
            ),
            462 => 
            array (
                'id' => 463,
                'recipe_id' => 225,
                'meal_type_id' => 2,
            ),
            463 => 
            array (
                'id' => 464,
                'recipe_id' => 226,
                'meal_type_id' => 5,
            ),
            464 => 
            array (
                'id' => 465,
                'recipe_id' => 227,
                'meal_type_id' => 6,
            ),
            465 => 
            array (
                'id' => 466,
                'recipe_id' => 227,
                'meal_type_id' => 3,
            ),
            466 => 
            array (
                'id' => 467,
                'recipe_id' => 228,
                'meal_type_id' => 6,
            ),
            467 => 
            array (
                'id' => 468,
                'recipe_id' => 228,
                'meal_type_id' => 5,
            ),
            468 => 
            array (
                'id' => 469,
                'recipe_id' => 229,
                'meal_type_id' => 1,
            ),
            469 => 
            array (
                'id' => 470,
                'recipe_id' => 230,
                'meal_type_id' => 6,
            ),
            470 => 
            array (
                'id' => 471,
                'recipe_id' => 230,
                'meal_type_id' => 3,
            ),
        ));
        
        
    }
}