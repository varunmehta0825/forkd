<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Beverage',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Bread',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Canned / Jarred Goods',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Dairy',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Dry / Baking Goods',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Frozen',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Grains / Pasta',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Health Shake',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Herbs & Spices',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Meat',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Misc.',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Nuts / Seeds',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Oil / Vinegar',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Produce',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Seafood',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Syrups / Sauces',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Vegan',
            ),
        ));
        
        
    }
}