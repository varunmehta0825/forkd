<?php

use Illuminate\Database\Seeder;

class RecipeRecipeTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_recipe_type')->delete();
        
        \DB::table('recipe_recipe_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'recipe_type_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 1,
                'recipe_type_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 2,
                'recipe_type_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 3,
                'recipe_type_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 3,
                'recipe_type_id' => 3,
            ),
            5 => 
            array (
                'id' => 6,
                'recipe_id' => 3,
                'recipe_type_id' => 4,
            ),
            6 => 
            array (
                'id' => 7,
                'recipe_id' => 4,
                'recipe_type_id' => 2,
            ),
            7 => 
            array (
                'id' => 8,
                'recipe_id' => 4,
                'recipe_type_id' => 4,
            ),
            8 => 
            array (
                'id' => 9,
                'recipe_id' => 4,
                'recipe_type_id' => 3,
            ),
            9 => 
            array (
                'id' => 10,
                'recipe_id' => 5,
                'recipe_type_id' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'recipe_id' => 5,
                'recipe_type_id' => 2,
            ),
            11 => 
            array (
                'id' => 12,
                'recipe_id' => 5,
                'recipe_type_id' => 3,
            ),
            12 => 
            array (
                'id' => 13,
                'recipe_id' => 5,
                'recipe_type_id' => 4,
            ),
            13 => 
            array (
                'id' => 14,
                'recipe_id' => 6,
                'recipe_type_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'recipe_id' => 6,
                'recipe_type_id' => 2,
            ),
            15 => 
            array (
                'id' => 16,
                'recipe_id' => 6,
                'recipe_type_id' => 4,
            ),
            16 => 
            array (
                'id' => 17,
                'recipe_id' => 8,
                'recipe_type_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'recipe_id' => 8,
                'recipe_type_id' => 2,
            ),
            18 => 
            array (
                'id' => 19,
                'recipe_id' => 9,
                'recipe_type_id' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'recipe_id' => 9,
                'recipe_type_id' => 2,
            ),
            20 => 
            array (
                'id' => 21,
                'recipe_id' => 10,
                'recipe_type_id' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'recipe_id' => 11,
                'recipe_type_id' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'recipe_id' => 11,
                'recipe_type_id' => 2,
            ),
            23 => 
            array (
                'id' => 24,
                'recipe_id' => 12,
                'recipe_type_id' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'recipe_id' => 13,
                'recipe_type_id' => 2,
            ),
            25 => 
            array (
                'id' => 26,
                'recipe_id' => 13,
                'recipe_type_id' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'recipe_id' => 13,
                'recipe_type_id' => 4,
            ),
            27 => 
            array (
                'id' => 28,
                'recipe_id' => 13,
                'recipe_type_id' => 3,
            ),
            28 => 
            array (
                'id' => 29,
                'recipe_id' => 14,
                'recipe_type_id' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'recipe_id' => 14,
                'recipe_type_id' => 3,
            ),
            30 => 
            array (
                'id' => 31,
                'recipe_id' => 14,
                'recipe_type_id' => 4,
            ),
            31 => 
            array (
                'id' => 32,
                'recipe_id' => 14,
                'recipe_type_id' => 2,
            ),
            32 => 
            array (
                'id' => 33,
                'recipe_id' => 15,
                'recipe_type_id' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'recipe_id' => 15,
                'recipe_type_id' => 3,
            ),
            34 => 
            array (
                'id' => 35,
                'recipe_id' => 15,
                'recipe_type_id' => 4,
            ),
            35 => 
            array (
                'id' => 36,
                'recipe_id' => 15,
                'recipe_type_id' => 2,
            ),
            36 => 
            array (
                'id' => 37,
                'recipe_id' => 16,
                'recipe_type_id' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'recipe_id' => 17,
                'recipe_type_id' => 3,
            ),
            38 => 
            array (
                'id' => 39,
                'recipe_id' => 17,
                'recipe_type_id' => 4,
            ),
            39 => 
            array (
                'id' => 40,
                'recipe_id' => 18,
                'recipe_type_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'recipe_id' => 19,
                'recipe_type_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'recipe_id' => 19,
                'recipe_type_id' => 2,
            ),
            42 => 
            array (
                'id' => 43,
                'recipe_id' => 19,
                'recipe_type_id' => 3,
            ),
            43 => 
            array (
                'id' => 44,
                'recipe_id' => 19,
                'recipe_type_id' => 4,
            ),
            44 => 
            array (
                'id' => 45,
                'recipe_id' => 20,
                'recipe_type_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'recipe_id' => 20,
                'recipe_type_id' => 2,
            ),
            46 => 
            array (
                'id' => 47,
                'recipe_id' => 20,
                'recipe_type_id' => 3,
            ),
            47 => 
            array (
                'id' => 48,
                'recipe_id' => 20,
                'recipe_type_id' => 4,
            ),
            48 => 
            array (
                'id' => 49,
                'recipe_id' => 21,
                'recipe_type_id' => 2,
            ),
            49 => 
            array (
                'id' => 50,
                'recipe_id' => 21,
                'recipe_type_id' => 3,
            ),
            50 => 
            array (
                'id' => 51,
                'recipe_id' => 21,
                'recipe_type_id' => 4,
            ),
            51 => 
            array (
                'id' => 52,
                'recipe_id' => 21,
                'recipe_type_id' => 1,
            ),
            52 => 
            array (
                'id' => 53,
                'recipe_id' => 23,
                'recipe_type_id' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'recipe_id' => 24,
                'recipe_type_id' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'recipe_id' => 25,
                'recipe_type_id' => 2,
            ),
            55 => 
            array (
                'id' => 56,
                'recipe_id' => 25,
                'recipe_type_id' => 3,
            ),
            56 => 
            array (
                'id' => 57,
                'recipe_id' => 25,
                'recipe_type_id' => 4,
            ),
            57 => 
            array (
                'id' => 58,
                'recipe_id' => 26,
                'recipe_type_id' => 2,
            ),
            58 => 
            array (
                'id' => 59,
                'recipe_id' => 26,
                'recipe_type_id' => 4,
            ),
            59 => 
            array (
                'id' => 60,
                'recipe_id' => 26,
                'recipe_type_id' => 3,
            ),
            60 => 
            array (
                'id' => 61,
                'recipe_id' => 27,
                'recipe_type_id' => 1,
            ),
            61 => 
            array (
                'id' => 62,
                'recipe_id' => 27,
                'recipe_type_id' => 2,
            ),
            62 => 
            array (
                'id' => 63,
                'recipe_id' => 27,
                'recipe_type_id' => 3,
            ),
            63 => 
            array (
                'id' => 64,
                'recipe_id' => 27,
                'recipe_type_id' => 4,
            ),
            64 => 
            array (
                'id' => 65,
                'recipe_id' => 28,
                'recipe_type_id' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'recipe_id' => 29,
                'recipe_type_id' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'recipe_id' => 31,
                'recipe_type_id' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'recipe_id' => 31,
                'recipe_type_id' => 3,
            ),
            68 => 
            array (
                'id' => 69,
                'recipe_id' => 31,
                'recipe_type_id' => 4,
            ),
            69 => 
            array (
                'id' => 70,
                'recipe_id' => 32,
                'recipe_type_id' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'recipe_id' => 32,
                'recipe_type_id' => 4,
            ),
            71 => 
            array (
                'id' => 72,
                'recipe_id' => 32,
                'recipe_type_id' => 3,
            ),
            72 => 
            array (
                'id' => 73,
                'recipe_id' => 32,
                'recipe_type_id' => 2,
            ),
            73 => 
            array (
                'id' => 74,
                'recipe_id' => 33,
                'recipe_type_id' => 1,
            ),
            74 => 
            array (
                'id' => 75,
                'recipe_id' => 34,
                'recipe_type_id' => 1,
            ),
            75 => 
            array (
                'id' => 76,
                'recipe_id' => 34,
                'recipe_type_id' => 3,
            ),
            76 => 
            array (
                'id' => 77,
                'recipe_id' => 34,
                'recipe_type_id' => 4,
            ),
            77 => 
            array (
                'id' => 78,
                'recipe_id' => 34,
                'recipe_type_id' => 2,
            ),
            78 => 
            array (
                'id' => 79,
                'recipe_id' => 35,
                'recipe_type_id' => 1,
            ),
            79 => 
            array (
                'id' => 80,
                'recipe_id' => 35,
                'recipe_type_id' => 3,
            ),
            80 => 
            array (
                'id' => 81,
                'recipe_id' => 35,
                'recipe_type_id' => 4,
            ),
            81 => 
            array (
                'id' => 82,
                'recipe_id' => 35,
                'recipe_type_id' => 2,
            ),
            82 => 
            array (
                'id' => 83,
                'recipe_id' => 36,
                'recipe_type_id' => 1,
            ),
            83 => 
            array (
                'id' => 84,
                'recipe_id' => 36,
                'recipe_type_id' => 2,
            ),
            84 => 
            array (
                'id' => 85,
                'recipe_id' => 36,
                'recipe_type_id' => 4,
            ),
            85 => 
            array (
                'id' => 86,
                'recipe_id' => 37,
                'recipe_type_id' => 1,
            ),
            86 => 
            array (
                'id' => 87,
                'recipe_id' => 38,
                'recipe_type_id' => 1,
            ),
            87 => 
            array (
                'id' => 88,
                'recipe_id' => 39,
                'recipe_type_id' => 1,
            ),
            88 => 
            array (
                'id' => 89,
                'recipe_id' => 42,
                'recipe_type_id' => 1,
            ),
            89 => 
            array (
                'id' => 90,
                'recipe_id' => 43,
                'recipe_type_id' => 1,
            ),
            90 => 
            array (
                'id' => 91,
                'recipe_id' => 44,
                'recipe_type_id' => 1,
            ),
            91 => 
            array (
                'id' => 92,
                'recipe_id' => 45,
                'recipe_type_id' => 1,
            ),
            92 => 
            array (
                'id' => 93,
                'recipe_id' => 46,
                'recipe_type_id' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'recipe_id' => 47,
                'recipe_type_id' => 1,
            ),
            94 => 
            array (
                'id' => 95,
                'recipe_id' => 47,
                'recipe_type_id' => 3,
            ),
            95 => 
            array (
                'id' => 96,
                'recipe_id' => 47,
                'recipe_type_id' => 4,
            ),
            96 => 
            array (
                'id' => 97,
                'recipe_id' => 47,
                'recipe_type_id' => 2,
            ),
            97 => 
            array (
                'id' => 98,
                'recipe_id' => 48,
                'recipe_type_id' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'recipe_id' => 48,
                'recipe_type_id' => 2,
            ),
            99 => 
            array (
                'id' => 100,
                'recipe_id' => 48,
                'recipe_type_id' => 3,
            ),
            100 => 
            array (
                'id' => 101,
                'recipe_id' => 48,
                'recipe_type_id' => 4,
            ),
            101 => 
            array (
                'id' => 102,
                'recipe_id' => 49,
                'recipe_type_id' => 3,
            ),
            102 => 
            array (
                'id' => 103,
                'recipe_id' => 49,
                'recipe_type_id' => 4,
            ),
            103 => 
            array (
                'id' => 104,
                'recipe_id' => 49,
                'recipe_type_id' => 2,
            ),
            104 => 
            array (
                'id' => 105,
                'recipe_id' => 50,
                'recipe_type_id' => 1,
            ),
            105 => 
            array (
                'id' => 106,
                'recipe_id' => 50,
                'recipe_type_id' => 2,
            ),
            106 => 
            array (
                'id' => 107,
                'recipe_id' => 50,
                'recipe_type_id' => 4,
            ),
            107 => 
            array (
                'id' => 108,
                'recipe_id' => 52,
                'recipe_type_id' => 1,
            ),
            108 => 
            array (
                'id' => 109,
                'recipe_id' => 53,
                'recipe_type_id' => 2,
            ),
            109 => 
            array (
                'id' => 110,
                'recipe_id' => 53,
                'recipe_type_id' => 3,
            ),
            110 => 
            array (
                'id' => 111,
                'recipe_id' => 53,
                'recipe_type_id' => 4,
            ),
            111 => 
            array (
                'id' => 112,
                'recipe_id' => 54,
                'recipe_type_id' => 1,
            ),
            112 => 
            array (
                'id' => 113,
                'recipe_id' => 54,
                'recipe_type_id' => 2,
            ),
            113 => 
            array (
                'id' => 114,
                'recipe_id' => 57,
                'recipe_type_id' => 1,
            ),
            114 => 
            array (
                'id' => 115,
                'recipe_id' => 58,
                'recipe_type_id' => 1,
            ),
            115 => 
            array (
                'id' => 116,
                'recipe_id' => 58,
                'recipe_type_id' => 6,
            ),
            116 => 
            array (
                'id' => 117,
                'recipe_id' => 58,
                'recipe_type_id' => 3,
            ),
            117 => 
            array (
                'id' => 118,
                'recipe_id' => 58,
                'recipe_type_id' => 4,
            ),
            118 => 
            array (
                'id' => 119,
                'recipe_id' => 59,
                'recipe_type_id' => 3,
            ),
            119 => 
            array (
                'id' => 120,
                'recipe_id' => 59,
                'recipe_type_id' => 4,
            ),
            120 => 
            array (
                'id' => 121,
                'recipe_id' => 61,
                'recipe_type_id' => 3,
            ),
            121 => 
            array (
                'id' => 122,
                'recipe_id' => 61,
                'recipe_type_id' => 4,
            ),
            122 => 
            array (
                'id' => 123,
                'recipe_id' => 62,
                'recipe_type_id' => 1,
            ),
            123 => 
            array (
                'id' => 124,
                'recipe_id' => 62,
                'recipe_type_id' => 3,
            ),
            124 => 
            array (
                'id' => 125,
                'recipe_id' => 62,
                'recipe_type_id' => 4,
            ),
            125 => 
            array (
                'id' => 126,
                'recipe_id' => 62,
                'recipe_type_id' => 2,
            ),
            126 => 
            array (
                'id' => 127,
                'recipe_id' => 63,
                'recipe_type_id' => 1,
            ),
            127 => 
            array (
                'id' => 128,
                'recipe_id' => 63,
                'recipe_type_id' => 2,
            ),
            128 => 
            array (
                'id' => 129,
                'recipe_id' => 63,
                'recipe_type_id' => 4,
            ),
            129 => 
            array (
                'id' => 130,
                'recipe_id' => 65,
                'recipe_type_id' => 1,
            ),
            130 => 
            array (
                'id' => 131,
                'recipe_id' => 65,
                'recipe_type_id' => 3,
            ),
            131 => 
            array (
                'id' => 132,
                'recipe_id' => 65,
                'recipe_type_id' => 4,
            ),
            132 => 
            array (
                'id' => 133,
                'recipe_id' => 65,
                'recipe_type_id' => 2,
            ),
            133 => 
            array (
                'id' => 134,
                'recipe_id' => 66,
                'recipe_type_id' => 1,
            ),
            134 => 
            array (
                'id' => 135,
                'recipe_id' => 66,
                'recipe_type_id' => 2,
            ),
            135 => 
            array (
                'id' => 136,
                'recipe_id' => 66,
                'recipe_type_id' => 4,
            ),
            136 => 
            array (
                'id' => 137,
                'recipe_id' => 66,
                'recipe_type_id' => 3,
            ),
            137 => 
            array (
                'id' => 138,
                'recipe_id' => 67,
                'recipe_type_id' => 1,
            ),
            138 => 
            array (
                'id' => 139,
                'recipe_id' => 67,
                'recipe_type_id' => 2,
            ),
            139 => 
            array (
                'id' => 140,
                'recipe_id' => 68,
                'recipe_type_id' => 1,
            ),
            140 => 
            array (
                'id' => 141,
                'recipe_id' => 68,
                'recipe_type_id' => 2,
            ),
            141 => 
            array (
                'id' => 142,
                'recipe_id' => 68,
                'recipe_type_id' => 4,
            ),
            142 => 
            array (
                'id' => 143,
                'recipe_id' => 68,
                'recipe_type_id' => 3,
            ),
            143 => 
            array (
                'id' => 144,
                'recipe_id' => 69,
                'recipe_type_id' => 1,
            ),
            144 => 
            array (
                'id' => 145,
                'recipe_id' => 71,
                'recipe_type_id' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'recipe_id' => 72,
                'recipe_type_id' => 1,
            ),
            146 => 
            array (
                'id' => 147,
                'recipe_id' => 72,
                'recipe_type_id' => 2,
            ),
            147 => 
            array (
                'id' => 148,
                'recipe_id' => 73,
                'recipe_type_id' => 1,
            ),
            148 => 
            array (
                'id' => 149,
                'recipe_id' => 73,
                'recipe_type_id' => 2,
            ),
            149 => 
            array (
                'id' => 150,
                'recipe_id' => 73,
                'recipe_type_id' => 3,
            ),
            150 => 
            array (
                'id' => 151,
                'recipe_id' => 73,
                'recipe_type_id' => 4,
            ),
            151 => 
            array (
                'id' => 152,
                'recipe_id' => 74,
                'recipe_type_id' => 1,
            ),
            152 => 
            array (
                'id' => 153,
                'recipe_id' => 74,
                'recipe_type_id' => 2,
            ),
            153 => 
            array (
                'id' => 154,
                'recipe_id' => 74,
                'recipe_type_id' => 3,
            ),
            154 => 
            array (
                'id' => 155,
                'recipe_id' => 74,
                'recipe_type_id' => 4,
            ),
            155 => 
            array (
                'id' => 156,
                'recipe_id' => 75,
                'recipe_type_id' => 1,
            ),
            156 => 
            array (
                'id' => 157,
                'recipe_id' => 75,
                'recipe_type_id' => 2,
            ),
            157 => 
            array (
                'id' => 158,
                'recipe_id' => 75,
                'recipe_type_id' => 3,
            ),
            158 => 
            array (
                'id' => 159,
                'recipe_id' => 75,
                'recipe_type_id' => 4,
            ),
            159 => 
            array (
                'id' => 160,
                'recipe_id' => 77,
                'recipe_type_id' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'recipe_id' => 78,
                'recipe_type_id' => 1,
            ),
            161 => 
            array (
                'id' => 162,
                'recipe_id' => 80,
                'recipe_type_id' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'recipe_id' => 81,
                'recipe_type_id' => 1,
            ),
            163 => 
            array (
                'id' => 164,
                'recipe_id' => 82,
                'recipe_type_id' => 1,
            ),
            164 => 
            array (
                'id' => 165,
                'recipe_id' => 83,
                'recipe_type_id' => 1,
            ),
            165 => 
            array (
                'id' => 166,
                'recipe_id' => 84,
                'recipe_type_id' => 1,
            ),
            166 => 
            array (
                'id' => 167,
                'recipe_id' => 85,
                'recipe_type_id' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'recipe_id' => 85,
                'recipe_type_id' => 2,
            ),
            168 => 
            array (
                'id' => 169,
                'recipe_id' => 86,
                'recipe_type_id' => 1,
            ),
            169 => 
            array (
                'id' => 170,
                'recipe_id' => 88,
                'recipe_type_id' => 1,
            ),
            170 => 
            array (
                'id' => 171,
                'recipe_id' => 88,
                'recipe_type_id' => 2,
            ),
            171 => 
            array (
                'id' => 172,
                'recipe_id' => 89,
                'recipe_type_id' => 1,
            ),
            172 => 
            array (
                'id' => 173,
                'recipe_id' => 90,
                'recipe_type_id' => 1,
            ),
            173 => 
            array (
                'id' => 174,
                'recipe_id' => 91,
                'recipe_type_id' => 6,
            ),
            174 => 
            array (
                'id' => 175,
                'recipe_id' => 93,
                'recipe_type_id' => 1,
            ),
            175 => 
            array (
                'id' => 176,
                'recipe_id' => 95,
                'recipe_type_id' => 1,
            ),
            176 => 
            array (
                'id' => 177,
                'recipe_id' => 95,
                'recipe_type_id' => 2,
            ),
            177 => 
            array (
                'id' => 178,
                'recipe_id' => 95,
                'recipe_type_id' => 3,
            ),
            178 => 
            array (
                'id' => 179,
                'recipe_id' => 95,
                'recipe_type_id' => 4,
            ),
            179 => 
            array (
                'id' => 180,
                'recipe_id' => 97,
                'recipe_type_id' => 1,
            ),
            180 => 
            array (
                'id' => 181,
                'recipe_id' => 98,
                'recipe_type_id' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'recipe_id' => 98,
                'recipe_type_id' => 2,
            ),
            182 => 
            array (
                'id' => 183,
                'recipe_id' => 98,
                'recipe_type_id' => 4,
            ),
            183 => 
            array (
                'id' => 184,
                'recipe_id' => 98,
                'recipe_type_id' => 3,
            ),
            184 => 
            array (
                'id' => 185,
                'recipe_id' => 99,
                'recipe_type_id' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'recipe_id' => 99,
                'recipe_type_id' => 2,
            ),
            186 => 
            array (
                'id' => 187,
                'recipe_id' => 99,
                'recipe_type_id' => 4,
            ),
            187 => 
            array (
                'id' => 188,
                'recipe_id' => 99,
                'recipe_type_id' => 3,
            ),
            188 => 
            array (
                'id' => 189,
                'recipe_id' => 100,
                'recipe_type_id' => 1,
            ),
            189 => 
            array (
                'id' => 190,
                'recipe_id' => 100,
                'recipe_type_id' => 2,
            ),
            190 => 
            array (
                'id' => 191,
                'recipe_id' => 100,
                'recipe_type_id' => 3,
            ),
            191 => 
            array (
                'id' => 192,
                'recipe_id' => 100,
                'recipe_type_id' => 4,
            ),
            192 => 
            array (
                'id' => 193,
                'recipe_id' => 101,
                'recipe_type_id' => 1,
            ),
            193 => 
            array (
                'id' => 194,
                'recipe_id' => 101,
                'recipe_type_id' => 2,
            ),
            194 => 
            array (
                'id' => 195,
                'recipe_id' => 101,
                'recipe_type_id' => 3,
            ),
            195 => 
            array (
                'id' => 196,
                'recipe_id' => 101,
                'recipe_type_id' => 4,
            ),
            196 => 
            array (
                'id' => 197,
                'recipe_id' => 102,
                'recipe_type_id' => 1,
            ),
            197 => 
            array (
                'id' => 198,
                'recipe_id' => 102,
                'recipe_type_id' => 2,
            ),
            198 => 
            array (
                'id' => 199,
                'recipe_id' => 103,
                'recipe_type_id' => 1,
            ),
            199 => 
            array (
                'id' => 200,
                'recipe_id' => 103,
                'recipe_type_id' => 2,
            ),
            200 => 
            array (
                'id' => 201,
                'recipe_id' => 103,
                'recipe_type_id' => 3,
            ),
            201 => 
            array (
                'id' => 202,
                'recipe_id' => 103,
                'recipe_type_id' => 4,
            ),
            202 => 
            array (
                'id' => 203,
                'recipe_id' => 104,
                'recipe_type_id' => 1,
            ),
            203 => 
            array (
                'id' => 204,
                'recipe_id' => 104,
                'recipe_type_id' => 2,
            ),
            204 => 
            array (
                'id' => 205,
                'recipe_id' => 104,
                'recipe_type_id' => 3,
            ),
            205 => 
            array (
                'id' => 206,
                'recipe_id' => 104,
                'recipe_type_id' => 4,
            ),
            206 => 
            array (
                'id' => 207,
                'recipe_id' => 105,
                'recipe_type_id' => 1,
            ),
            207 => 
            array (
                'id' => 208,
                'recipe_id' => 105,
                'recipe_type_id' => 2,
            ),
            208 => 
            array (
                'id' => 209,
                'recipe_id' => 105,
                'recipe_type_id' => 3,
            ),
            209 => 
            array (
                'id' => 210,
                'recipe_id' => 105,
                'recipe_type_id' => 4,
            ),
            210 => 
            array (
                'id' => 211,
                'recipe_id' => 106,
                'recipe_type_id' => 1,
            ),
            211 => 
            array (
                'id' => 212,
                'recipe_id' => 107,
                'recipe_type_id' => 1,
            ),
            212 => 
            array (
                'id' => 213,
                'recipe_id' => 107,
                'recipe_type_id' => 2,
            ),
            213 => 
            array (
                'id' => 214,
                'recipe_id' => 107,
                'recipe_type_id' => 3,
            ),
            214 => 
            array (
                'id' => 215,
                'recipe_id' => 107,
                'recipe_type_id' => 4,
            ),
            215 => 
            array (
                'id' => 216,
                'recipe_id' => 108,
                'recipe_type_id' => 1,
            ),
            216 => 
            array (
                'id' => 217,
                'recipe_id' => 109,
                'recipe_type_id' => 1,
            ),
            217 => 
            array (
                'id' => 218,
                'recipe_id' => 110,
                'recipe_type_id' => 1,
            ),
            218 => 
            array (
                'id' => 219,
                'recipe_id' => 111,
                'recipe_type_id' => 1,
            ),
            219 => 
            array (
                'id' => 220,
                'recipe_id' => 111,
                'recipe_type_id' => 2,
            ),
            220 => 
            array (
                'id' => 221,
                'recipe_id' => 112,
                'recipe_type_id' => 1,
            ),
            221 => 
            array (
                'id' => 222,
                'recipe_id' => 113,
                'recipe_type_id' => 1,
            ),
            222 => 
            array (
                'id' => 223,
                'recipe_id' => 113,
                'recipe_type_id' => 4,
            ),
            223 => 
            array (
                'id' => 224,
                'recipe_id' => 114,
                'recipe_type_id' => 1,
            ),
            224 => 
            array (
                'id' => 225,
                'recipe_id' => 115,
                'recipe_type_id' => 1,
            ),
            225 => 
            array (
                'id' => 226,
                'recipe_id' => 115,
                'recipe_type_id' => 2,
            ),
            226 => 
            array (
                'id' => 227,
                'recipe_id' => 115,
                'recipe_type_id' => 3,
            ),
            227 => 
            array (
                'id' => 228,
                'recipe_id' => 115,
                'recipe_type_id' => 4,
            ),
            228 => 
            array (
                'id' => 229,
                'recipe_id' => 116,
                'recipe_type_id' => 1,
            ),
            229 => 
            array (
                'id' => 230,
                'recipe_id' => 117,
                'recipe_type_id' => 1,
            ),
            230 => 
            array (
                'id' => 231,
                'recipe_id' => 118,
                'recipe_type_id' => 1,
            ),
            231 => 
            array (
                'id' => 232,
                'recipe_id' => 119,
                'recipe_type_id' => 2,
            ),
            232 => 
            array (
                'id' => 233,
                'recipe_id' => 119,
                'recipe_type_id' => 1,
            ),
            233 => 
            array (
                'id' => 234,
                'recipe_id' => 121,
                'recipe_type_id' => 1,
            ),
            234 => 
            array (
                'id' => 235,
                'recipe_id' => 121,
                'recipe_type_id' => 2,
            ),
            235 => 
            array (
                'id' => 236,
                'recipe_id' => 121,
                'recipe_type_id' => 3,
            ),
            236 => 
            array (
                'id' => 237,
                'recipe_id' => 121,
                'recipe_type_id' => 4,
            ),
            237 => 
            array (
                'id' => 238,
                'recipe_id' => 123,
                'recipe_type_id' => 6,
            ),
            238 => 
            array (
                'id' => 239,
                'recipe_id' => 124,
                'recipe_type_id' => 1,
            ),
            239 => 
            array (
                'id' => 240,
                'recipe_id' => 126,
                'recipe_type_id' => 1,
            ),
            240 => 
            array (
                'id' => 241,
                'recipe_id' => 127,
                'recipe_type_id' => 1,
            ),
            241 => 
            array (
                'id' => 242,
                'recipe_id' => 127,
                'recipe_type_id' => 2,
            ),
            242 => 
            array (
                'id' => 243,
                'recipe_id' => 128,
                'recipe_type_id' => 1,
            ),
            243 => 
            array (
                'id' => 244,
                'recipe_id' => 129,
                'recipe_type_id' => 1,
            ),
            244 => 
            array (
                'id' => 245,
                'recipe_id' => 129,
                'recipe_type_id' => 2,
            ),
            245 => 
            array (
                'id' => 246,
                'recipe_id' => 131,
                'recipe_type_id' => 1,
            ),
            246 => 
            array (
                'id' => 247,
                'recipe_id' => 133,
                'recipe_type_id' => 1,
            ),
            247 => 
            array (
                'id' => 248,
                'recipe_id' => 133,
                'recipe_type_id' => 2,
            ),
            248 => 
            array (
                'id' => 249,
                'recipe_id' => 133,
                'recipe_type_id' => 3,
            ),
            249 => 
            array (
                'id' => 250,
                'recipe_id' => 133,
                'recipe_type_id' => 4,
            ),
            250 => 
            array (
                'id' => 251,
                'recipe_id' => 134,
                'recipe_type_id' => 1,
            ),
            251 => 
            array (
                'id' => 252,
                'recipe_id' => 134,
                'recipe_type_id' => 2,
            ),
            252 => 
            array (
                'id' => 253,
                'recipe_id' => 134,
                'recipe_type_id' => 3,
            ),
            253 => 
            array (
                'id' => 254,
                'recipe_id' => 134,
                'recipe_type_id' => 4,
            ),
            254 => 
            array (
                'id' => 255,
                'recipe_id' => 135,
                'recipe_type_id' => 1,
            ),
            255 => 
            array (
                'id' => 256,
                'recipe_id' => 138,
                'recipe_type_id' => 1,
            ),
            256 => 
            array (
                'id' => 257,
                'recipe_id' => 138,
                'recipe_type_id' => 2,
            ),
            257 => 
            array (
                'id' => 258,
                'recipe_id' => 138,
                'recipe_type_id' => 3,
            ),
            258 => 
            array (
                'id' => 259,
                'recipe_id' => 138,
                'recipe_type_id' => 4,
            ),
            259 => 
            array (
                'id' => 260,
                'recipe_id' => 139,
                'recipe_type_id' => 1,
            ),
            260 => 
            array (
                'id' => 261,
                'recipe_id' => 139,
                'recipe_type_id' => 2,
            ),
            261 => 
            array (
                'id' => 262,
                'recipe_id' => 139,
                'recipe_type_id' => 3,
            ),
            262 => 
            array (
                'id' => 263,
                'recipe_id' => 139,
                'recipe_type_id' => 4,
            ),
            263 => 
            array (
                'id' => 264,
                'recipe_id' => 140,
                'recipe_type_id' => 1,
            ),
            264 => 
            array (
                'id' => 265,
                'recipe_id' => 141,
                'recipe_type_id' => 2,
            ),
            265 => 
            array (
                'id' => 266,
                'recipe_id' => 141,
                'recipe_type_id' => 3,
            ),
            266 => 
            array (
                'id' => 267,
                'recipe_id' => 141,
                'recipe_type_id' => 4,
            ),
            267 => 
            array (
                'id' => 268,
                'recipe_id' => 141,
                'recipe_type_id' => 1,
            ),
            268 => 
            array (
                'id' => 269,
                'recipe_id' => 142,
                'recipe_type_id' => 1,
            ),
            269 => 
            array (
                'id' => 270,
                'recipe_id' => 142,
                'recipe_type_id' => 2,
            ),
            270 => 
            array (
                'id' => 271,
                'recipe_id' => 142,
                'recipe_type_id' => 3,
            ),
            271 => 
            array (
                'id' => 272,
                'recipe_id' => 142,
                'recipe_type_id' => 4,
            ),
            272 => 
            array (
                'id' => 273,
                'recipe_id' => 143,
                'recipe_type_id' => 1,
            ),
            273 => 
            array (
                'id' => 274,
                'recipe_id' => 144,
                'recipe_type_id' => 1,
            ),
            274 => 
            array (
                'id' => 275,
                'recipe_id' => 145,
                'recipe_type_id' => 1,
            ),
            275 => 
            array (
                'id' => 276,
                'recipe_id' => 146,
                'recipe_type_id' => 1,
            ),
            276 => 
            array (
                'id' => 277,
                'recipe_id' => 148,
                'recipe_type_id' => 1,
            ),
            277 => 
            array (
                'id' => 278,
                'recipe_id' => 150,
                'recipe_type_id' => 1,
            ),
            278 => 
            array (
                'id' => 279,
                'recipe_id' => 151,
                'recipe_type_id' => 1,
            ),
            279 => 
            array (
                'id' => 280,
                'recipe_id' => 152,
                'recipe_type_id' => 1,
            ),
            280 => 
            array (
                'id' => 281,
                'recipe_id' => 152,
                'recipe_type_id' => 2,
            ),
            281 => 
            array (
                'id' => 282,
                'recipe_id' => 152,
                'recipe_type_id' => 3,
            ),
            282 => 
            array (
                'id' => 283,
                'recipe_id' => 152,
                'recipe_type_id' => 4,
            ),
            283 => 
            array (
                'id' => 284,
                'recipe_id' => 153,
                'recipe_type_id' => 1,
            ),
            284 => 
            array (
                'id' => 285,
                'recipe_id' => 153,
                'recipe_type_id' => 3,
            ),
            285 => 
            array (
                'id' => 286,
                'recipe_id' => 153,
                'recipe_type_id' => 4,
            ),
            286 => 
            array (
                'id' => 287,
                'recipe_id' => 154,
                'recipe_type_id' => 1,
            ),
            287 => 
            array (
                'id' => 288,
                'recipe_id' => 155,
                'recipe_type_id' => 1,
            ),
            288 => 
            array (
                'id' => 289,
                'recipe_id' => 155,
                'recipe_type_id' => 2,
            ),
            289 => 
            array (
                'id' => 290,
                'recipe_id' => 156,
                'recipe_type_id' => 1,
            ),
            290 => 
            array (
                'id' => 291,
                'recipe_id' => 157,
                'recipe_type_id' => 1,
            ),
            291 => 
            array (
                'id' => 292,
                'recipe_id' => 157,
                'recipe_type_id' => 3,
            ),
            292 => 
            array (
                'id' => 293,
                'recipe_id' => 157,
                'recipe_type_id' => 4,
            ),
            293 => 
            array (
                'id' => 294,
                'recipe_id' => 158,
                'recipe_type_id' => 1,
            ),
            294 => 
            array (
                'id' => 295,
                'recipe_id' => 159,
                'recipe_type_id' => 1,
            ),
            295 => 
            array (
                'id' => 296,
                'recipe_id' => 159,
                'recipe_type_id' => 2,
            ),
            296 => 
            array (
                'id' => 297,
                'recipe_id' => 159,
                'recipe_type_id' => 3,
            ),
            297 => 
            array (
                'id' => 298,
                'recipe_id' => 159,
                'recipe_type_id' => 4,
            ),
            298 => 
            array (
                'id' => 299,
                'recipe_id' => 160,
                'recipe_type_id' => 6,
            ),
            299 => 
            array (
                'id' => 300,
                'recipe_id' => 161,
                'recipe_type_id' => 2,
            ),
            300 => 
            array (
                'id' => 301,
                'recipe_id' => 161,
                'recipe_type_id' => 4,
            ),
            301 => 
            array (
                'id' => 302,
                'recipe_id' => 161,
                'recipe_type_id' => 3,
            ),
            302 => 
            array (
                'id' => 303,
                'recipe_id' => 162,
                'recipe_type_id' => 1,
            ),
            303 => 
            array (
                'id' => 304,
                'recipe_id' => 163,
                'recipe_type_id' => 1,
            ),
            304 => 
            array (
                'id' => 305,
                'recipe_id' => 164,
                'recipe_type_id' => 1,
            ),
            305 => 
            array (
                'id' => 306,
                'recipe_id' => 164,
                'recipe_type_id' => 2,
            ),
            306 => 
            array (
                'id' => 307,
                'recipe_id' => 164,
                'recipe_type_id' => 3,
            ),
            307 => 
            array (
                'id' => 308,
                'recipe_id' => 164,
                'recipe_type_id' => 4,
            ),
            308 => 
            array (
                'id' => 309,
                'recipe_id' => 165,
                'recipe_type_id' => 1,
            ),
            309 => 
            array (
                'id' => 310,
                'recipe_id' => 165,
                'recipe_type_id' => 2,
            ),
            310 => 
            array (
                'id' => 311,
                'recipe_id' => 165,
                'recipe_type_id' => 3,
            ),
            311 => 
            array (
                'id' => 312,
                'recipe_id' => 165,
                'recipe_type_id' => 4,
            ),
            312 => 
            array (
                'id' => 313,
                'recipe_id' => 166,
                'recipe_type_id' => 1,
            ),
            313 => 
            array (
                'id' => 314,
                'recipe_id' => 171,
                'recipe_type_id' => 1,
            ),
            314 => 
            array (
                'id' => 315,
                'recipe_id' => 171,
                'recipe_type_id' => 3,
            ),
            315 => 
            array (
                'id' => 316,
                'recipe_id' => 171,
                'recipe_type_id' => 4,
            ),
            316 => 
            array (
                'id' => 317,
                'recipe_id' => 173,
                'recipe_type_id' => 1,
            ),
            317 => 
            array (
                'id' => 318,
                'recipe_id' => 174,
                'recipe_type_id' => 1,
            ),
            318 => 
            array (
                'id' => 319,
                'recipe_id' => 176,
                'recipe_type_id' => 1,
            ),
            319 => 
            array (
                'id' => 320,
                'recipe_id' => 177,
                'recipe_type_id' => 1,
            ),
            320 => 
            array (
                'id' => 321,
                'recipe_id' => 177,
                'recipe_type_id' => 2,
            ),
            321 => 
            array (
                'id' => 322,
                'recipe_id' => 177,
                'recipe_type_id' => 3,
            ),
            322 => 
            array (
                'id' => 323,
                'recipe_id' => 177,
                'recipe_type_id' => 4,
            ),
            323 => 
            array (
                'id' => 324,
                'recipe_id' => 178,
                'recipe_type_id' => 1,
            ),
            324 => 
            array (
                'id' => 325,
                'recipe_id' => 178,
                'recipe_type_id' => 2,
            ),
            325 => 
            array (
                'id' => 326,
                'recipe_id' => 178,
                'recipe_type_id' => 3,
            ),
            326 => 
            array (
                'id' => 327,
                'recipe_id' => 178,
                'recipe_type_id' => 4,
            ),
            327 => 
            array (
                'id' => 328,
                'recipe_id' => 179,
                'recipe_type_id' => 1,
            ),
            328 => 
            array (
                'id' => 329,
                'recipe_id' => 182,
                'recipe_type_id' => 1,
            ),
            329 => 
            array (
                'id' => 330,
                'recipe_id' => 183,
                'recipe_type_id' => 1,
            ),
            330 => 
            array (
                'id' => 331,
                'recipe_id' => 184,
                'recipe_type_id' => 1,
            ),
            331 => 
            array (
                'id' => 332,
                'recipe_id' => 185,
                'recipe_type_id' => 1,
            ),
            332 => 
            array (
                'id' => 333,
                'recipe_id' => 186,
                'recipe_type_id' => 1,
            ),
            333 => 
            array (
                'id' => 334,
                'recipe_id' => 187,
                'recipe_type_id' => 1,
            ),
            334 => 
            array (
                'id' => 335,
                'recipe_id' => 188,
                'recipe_type_id' => 1,
            ),
            335 => 
            array (
                'id' => 336,
                'recipe_id' => 189,
                'recipe_type_id' => 1,
            ),
            336 => 
            array (
                'id' => 337,
                'recipe_id' => 190,
                'recipe_type_id' => 1,
            ),
            337 => 
            array (
                'id' => 338,
                'recipe_id' => 191,
                'recipe_type_id' => 1,
            ),
            338 => 
            array (
                'id' => 339,
                'recipe_id' => 191,
                'recipe_type_id' => 2,
            ),
            339 => 
            array (
                'id' => 340,
                'recipe_id' => 191,
                'recipe_type_id' => 3,
            ),
            340 => 
            array (
                'id' => 341,
                'recipe_id' => 191,
                'recipe_type_id' => 4,
            ),
            341 => 
            array (
                'id' => 342,
                'recipe_id' => 192,
                'recipe_type_id' => 6,
            ),
            342 => 
            array (
                'id' => 343,
                'recipe_id' => 193,
                'recipe_type_id' => 1,
            ),
            343 => 
            array (
                'id' => 344,
                'recipe_id' => 193,
                'recipe_type_id' => 2,
            ),
            344 => 
            array (
                'id' => 345,
                'recipe_id' => 193,
                'recipe_type_id' => 3,
            ),
            345 => 
            array (
                'id' => 346,
                'recipe_id' => 193,
                'recipe_type_id' => 4,
            ),
            346 => 
            array (
                'id' => 347,
                'recipe_id' => 194,
                'recipe_type_id' => 1,
            ),
            347 => 
            array (
                'id' => 348,
                'recipe_id' => 194,
                'recipe_type_id' => 2,
            ),
            348 => 
            array (
                'id' => 349,
                'recipe_id' => 194,
                'recipe_type_id' => 3,
            ),
            349 => 
            array (
                'id' => 350,
                'recipe_id' => 194,
                'recipe_type_id' => 4,
            ),
            350 => 
            array (
                'id' => 351,
                'recipe_id' => 195,
                'recipe_type_id' => 1,
            ),
            351 => 
            array (
                'id' => 352,
                'recipe_id' => 196,
                'recipe_type_id' => 2,
            ),
            352 => 
            array (
                'id' => 353,
                'recipe_id' => 198,
                'recipe_type_id' => 1,
            ),
            353 => 
            array (
                'id' => 354,
                'recipe_id' => 199,
                'recipe_type_id' => 6,
            ),
            354 => 
            array (
                'id' => 355,
                'recipe_id' => 201,
                'recipe_type_id' => 1,
            ),
            355 => 
            array (
                'id' => 356,
                'recipe_id' => 202,
                'recipe_type_id' => 1,
            ),
            356 => 
            array (
                'id' => 357,
                'recipe_id' => 203,
                'recipe_type_id' => 1,
            ),
            357 => 
            array (
                'id' => 358,
                'recipe_id' => 203,
                'recipe_type_id' => 2,
            ),
            358 => 
            array (
                'id' => 359,
                'recipe_id' => 203,
                'recipe_type_id' => 3,
            ),
            359 => 
            array (
                'id' => 360,
                'recipe_id' => 203,
                'recipe_type_id' => 4,
            ),
            360 => 
            array (
                'id' => 361,
                'recipe_id' => 204,
                'recipe_type_id' => 1,
            ),
            361 => 
            array (
                'id' => 362,
                'recipe_id' => 204,
                'recipe_type_id' => 2,
            ),
            362 => 
            array (
                'id' => 363,
                'recipe_id' => 204,
                'recipe_type_id' => 4,
            ),
            363 => 
            array (
                'id' => 364,
                'recipe_id' => 204,
                'recipe_type_id' => 3,
            ),
            364 => 
            array (
                'id' => 365,
                'recipe_id' => 205,
                'recipe_type_id' => 1,
            ),
            365 => 
            array (
                'id' => 366,
                'recipe_id' => 206,
                'recipe_type_id' => 1,
            ),
            366 => 
            array (
                'id' => 367,
                'recipe_id' => 207,
                'recipe_type_id' => 1,
            ),
            367 => 
            array (
                'id' => 368,
                'recipe_id' => 207,
                'recipe_type_id' => 2,
            ),
            368 => 
            array (
                'id' => 369,
                'recipe_id' => 207,
                'recipe_type_id' => 3,
            ),
            369 => 
            array (
                'id' => 370,
                'recipe_id' => 207,
                'recipe_type_id' => 4,
            ),
            370 => 
            array (
                'id' => 371,
                'recipe_id' => 208,
                'recipe_type_id' => 1,
            ),
            371 => 
            array (
                'id' => 372,
                'recipe_id' => 208,
                'recipe_type_id' => 3,
            ),
            372 => 
            array (
                'id' => 373,
                'recipe_id' => 208,
                'recipe_type_id' => 4,
            ),
            373 => 
            array (
                'id' => 374,
                'recipe_id' => 208,
                'recipe_type_id' => 2,
            ),
            374 => 
            array (
                'id' => 375,
                'recipe_id' => 209,
                'recipe_type_id' => 6,
            ),
            375 => 
            array (
                'id' => 376,
                'recipe_id' => 211,
                'recipe_type_id' => 3,
            ),
            376 => 
            array (
                'id' => 377,
                'recipe_id' => 211,
                'recipe_type_id' => 4,
            ),
            377 => 
            array (
                'id' => 378,
                'recipe_id' => 212,
                'recipe_type_id' => 1,
            ),
            378 => 
            array (
                'id' => 379,
                'recipe_id' => 213,
                'recipe_type_id' => 1,
            ),
            379 => 
            array (
                'id' => 380,
                'recipe_id' => 214,
                'recipe_type_id' => 1,
            ),
            380 => 
            array (
                'id' => 381,
                'recipe_id' => 215,
                'recipe_type_id' => 1,
            ),
            381 => 
            array (
                'id' => 382,
                'recipe_id' => 216,
                'recipe_type_id' => 1,
            ),
            382 => 
            array (
                'id' => 383,
                'recipe_id' => 217,
                'recipe_type_id' => 1,
            ),
            383 => 
            array (
                'id' => 384,
                'recipe_id' => 218,
                'recipe_type_id' => 1,
            ),
            384 => 
            array (
                'id' => 385,
                'recipe_id' => 219,
                'recipe_type_id' => 1,
            ),
            385 => 
            array (
                'id' => 386,
                'recipe_id' => 219,
                'recipe_type_id' => 3,
            ),
            386 => 
            array (
                'id' => 387,
                'recipe_id' => 219,
                'recipe_type_id' => 4,
            ),
            387 => 
            array (
                'id' => 388,
                'recipe_id' => 220,
                'recipe_type_id' => 1,
            ),
            388 => 
            array (
                'id' => 389,
                'recipe_id' => 221,
                'recipe_type_id' => 1,
            ),
            389 => 
            array (
                'id' => 390,
                'recipe_id' => 221,
                'recipe_type_id' => 2,
            ),
            390 => 
            array (
                'id' => 391,
                'recipe_id' => 222,
                'recipe_type_id' => 1,
            ),
            391 => 
            array (
                'id' => 392,
                'recipe_id' => 222,
                'recipe_type_id' => 2,
            ),
            392 => 
            array (
                'id' => 393,
                'recipe_id' => 222,
                'recipe_type_id' => 3,
            ),
            393 => 
            array (
                'id' => 394,
                'recipe_id' => 222,
                'recipe_type_id' => 4,
            ),
            394 => 
            array (
                'id' => 395,
                'recipe_id' => 223,
                'recipe_type_id' => 1,
            ),
            395 => 
            array (
                'id' => 396,
                'recipe_id' => 223,
                'recipe_type_id' => 2,
            ),
            396 => 
            array (
                'id' => 397,
                'recipe_id' => 224,
                'recipe_type_id' => 6,
            ),
            397 => 
            array (
                'id' => 398,
                'recipe_id' => 225,
                'recipe_type_id' => 1,
            ),
            398 => 
            array (
                'id' => 399,
                'recipe_id' => 226,
                'recipe_type_id' => 4,
            ),
            399 => 
            array (
                'id' => 400,
                'recipe_id' => 226,
                'recipe_type_id' => 2,
            ),
            400 => 
            array (
                'id' => 401,
                'recipe_id' => 226,
                'recipe_type_id' => 1,
            ),
            401 => 
            array (
                'id' => 402,
                'recipe_id' => 227,
                'recipe_type_id' => 6,
            ),
            402 => 
            array (
                'id' => 403,
                'recipe_id' => 228,
                'recipe_type_id' => 1,
            ),
            403 => 
            array (
                'id' => 404,
                'recipe_id' => 228,
                'recipe_type_id' => 3,
            ),
            404 => 
            array (
                'id' => 405,
                'recipe_id' => 228,
                'recipe_type_id' => 4,
            ),
            405 => 
            array (
                'id' => 406,
                'recipe_id' => 229,
                'recipe_type_id' => 1,
            ),
            406 => 
            array (
                'id' => 407,
                'recipe_id' => 229,
                'recipe_type_id' => 2,
            ),
            407 => 
            array (
                'id' => 408,
                'recipe_id' => 230,
                'recipe_type_id' => 1,
            ),
        ));
        
        
    }
}