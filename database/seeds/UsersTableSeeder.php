<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Hercule',
                'email' => 'herculea@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Z4eufhPXF0HSo1xuHD14ceyUtxKreVhTDKhwvrZE4YSSvNH2.qj9C',
                'age' => NULL,
                'gender' => NULL,
                'avatar' => 'avatar.png',
                'active' => 1,
                'confirmed' => '1',
                'stripe_id' => NULL,
                'card_brand' => NULL,
                'card_last_four' => NULL,
                'trial_ends_at' => NULL,
                'measurement' => 'us',
                'remember_token' => NULL,
                'last_active' => '2019-01-25 08:59:52',
                'deleted_at' => NULL,
                'created_at' => '2019-01-25 08:59:52',
                'updated_at' => '2019-01-25 08:59:52',
            ),
        ));


    }
}
