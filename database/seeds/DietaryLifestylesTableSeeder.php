<?php

use Illuminate\Database\Seeder;

class DietaryLifestylesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('dietary_lifestyles')->delete();

        \DB::table('dietary_lifestyles')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Dairy Free',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Gluten Free',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Pescatarian',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Vegan',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Vegetarian',
            ),
        ));


    }
}
