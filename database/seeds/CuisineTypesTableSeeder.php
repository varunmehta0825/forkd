<?php

use Illuminate\Database\Seeder;

class CuisineTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('cuisine_types')->delete();

        \DB::table('cuisine_types')->insert(array (
            0 =>
            array (
                'id' => 1,
                'Name' => 'American',
            ),
            1 =>
            array (
                'id' => 2,
                'Name' => 'Asian',
            ),
            2 =>
            array (
                'id' => 3,
                'Name' => 'Cuban',
            ),
            3 =>
            array (
                'id' => 4,
                'Name' => 'French',
            ),
            4 =>
            array (
                'id' => 5,
                'Name' => 'German',
            ),
            5 =>
            array (
                'id' => 6,
                'Name' => 'Greek',
            ),
            6 =>
            array (
                'id' => 7,
                'Name' => 'Italian',
            ),
            7 =>
            array (
                'id' => 8,
                'Name' => 'Japanese',
            ),
            8 =>
            array (
                'id' => 9,
                'Name' => 'Mediterranean',
            ),
            9 =>
            array (
                'id' => 10,
                'Name' => 'Mexican',
            ),
            10 =>
            array (
                'id' => 11,
                'Name' => 'Thai',
            ),
        ));


    }
}
