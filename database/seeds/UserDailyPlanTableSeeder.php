<?php

use Illuminate\Database\Seeder;

class UserDailyPlanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_daily_plan')->delete();
        
        \DB::table('user_daily_plan')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_weekly_plan_id' => 1,
                'day' => 'Monday',
            ),
            1 => 
            array (
                'id' => 2,
                'user_weekly_plan_id' => 1,
                'day' => 'Tuesday',
            ),
            2 => 
            array (
                'id' => 3,
                'user_weekly_plan_id' => 1,
                'day' => 'Wednesday',
            ),
            3 => 
            array (
                'id' => 4,
                'user_weekly_plan_id' => 1,
                'day' => 'Thursday',
            ),
            4 => 
            array (
                'id' => 5,
                'user_weekly_plan_id' => 1,
                'day' => 'Friday',
            ),
            5 => 
            array (
                'id' => 6,
                'user_weekly_plan_id' => 1,
                'day' => 'Saturday',
            ),
            6 => 
            array (
                'id' => 7,
                'user_weekly_plan_id' => 1,
                'day' => 'Sunday',
            ),
        ));
        
        
    }
}