<?php

use Illuminate\Database\Seeder;

class MealTemplatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('meal_templates')->delete();
        
        \DB::table('meal_templates')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Repeat All Meals Every Day',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Only Dinners Change',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Alternate M, W, F SU & T, TH SA',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Leftover Dinner For Lunch',
            ),
        ));
        
        
    }
}