<?php

use Illuminate\Database\Seeder;

class OauthRefreshTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_refresh_tokens')->delete();
        
        \DB::table('oauth_refresh_tokens')->insert(array (
            0 => 
            array (
                'id' => '808bc5d3fdd0b3889a583cf317fe24f9182fb2e33c069472dc7d66d70afbbaf7b862db34d58e7701',
                'access_token_id' => 'b2c4f446a1985dc23235409729d06eb35afa4afc58e724b8bf58d142d1e7552b87dfba779c1f7e8c',
                'revoked' => 0,
                'expires_at' => '2020-02-03 19:43:20',
            ),
        ));
        
        
    }
}