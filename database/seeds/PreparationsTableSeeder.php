<?php

use Illuminate\Database\Seeder;

class PreparationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('preparations')->delete();
        
        \DB::table('preparations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '.25',
            ),
            1 => 
            array (
                'id' => 2,
            'name' => '(2 cans)',
            ),
            2 => 
            array (
                'id' => 3,
            'name' => '(2, 6 oz. fillets)',
            ),
            3 => 
            array (
                'id' => 4,
            'name' => '(2, 14.5 oz cans)',
            ),
            4 => 
            array (
                'id' => 5,
            'name' => '(4, 6 oz. fillets)',
            ),
            5 => 
            array (
                'id' => 6,
            'name' => '(about 1 small bunch)',
            ),
            6 => 
            array (
                'id' => 7,
            'name' => '(approx. 42 grams, check label for serving size)',
            ),
            7 => 
            array (
                'id' => 8,
            'name' => '(can substitute white wine vinegar) ',
            ),
            8 => 
            array (
                'id' => 9,
            'name' => '(can substitute with balsamic vinegar)',
            ),
            9 => 
            array (
                'id' => 10,
            'name' => '(medium)',
            ),
            10 => 
            array (
                'id' => 11,
            'name' => '(optional)',
            ),
            11 => 
            array (
                'id' => 12,
            'name' => '(remove seeds for less heat)',
            ),
            12 => 
            array (
                'id' => 13,
            'name' => '(spicy Italian)',
            ),
            13 => 
            array (
                'id' => 14,
            'name' => '(stew or stir fry cut)',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => '1 fillet',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => '3',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'brewed',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'chopped',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'cooked',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'cooked and chopped',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'cooked, shredded',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'cubed',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'cut into strips',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'diced',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'divided',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'fried or poached',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'frozen',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'ground',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'halved',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'halved and seeded',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'hard-boiled',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'mashed',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'melted',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'minced',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'peeled and diced',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'plus more for baking dish',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'roasted',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'room temperature',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'shredded',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'sliced',
            ),
            40 => 
            array (
                'id' => 41,
            'name' => 'spiralized (zoodles)',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'thinly sliced',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'trimmed',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'whole',
            ),
        ));
        
        
    }
}