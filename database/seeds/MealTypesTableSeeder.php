<?php

use Illuminate\Database\Seeder;

class MealTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('meal_types')->delete();
        
        \DB::table('meal_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Alcohol',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Breakfast',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Dinner',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Snack 1',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Snack 2',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Lunch',
            ),
        ));
        
        
    }
}