<?php

use Illuminate\Database\Seeder;

class RecipeCuisineTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recipe_cuisine_type')->delete();
        
        \DB::table('recipe_cuisine_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'recipe_id' => 1,
                'cuisine_type_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'recipe_id' => 2,
                'cuisine_type_id' => 10,
            ),
            2 => 
            array (
                'id' => 3,
                'recipe_id' => 3,
                'cuisine_type_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'recipe_id' => 4,
                'cuisine_type_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'recipe_id' => 5,
                'cuisine_type_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'recipe_id' => 6,
                'cuisine_type_id' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'recipe_id' => 7,
                'cuisine_type_id' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'recipe_id' => 8,
                'cuisine_type_id' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'recipe_id' => 9,
                'cuisine_type_id' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'recipe_id' => 10,
                'cuisine_type_id' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'recipe_id' => 11,
                'cuisine_type_id' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'recipe_id' => 12,
                'cuisine_type_id' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'recipe_id' => 13,
                'cuisine_type_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'recipe_id' => 14,
                'cuisine_type_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'recipe_id' => 15,
                'cuisine_type_id' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'recipe_id' => 16,
                'cuisine_type_id' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'recipe_id' => 17,
                'cuisine_type_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'recipe_id' => 18,
                'cuisine_type_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'recipe_id' => 19,
                'cuisine_type_id' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'recipe_id' => 20,
                'cuisine_type_id' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'recipe_id' => 21,
                'cuisine_type_id' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'recipe_id' => 22,
                'cuisine_type_id' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'recipe_id' => 23,
                'cuisine_type_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'recipe_id' => 23,
                'cuisine_type_id' => 10,
            ),
            24 => 
            array (
                'id' => 25,
                'recipe_id' => 24,
                'cuisine_type_id' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'recipe_id' => 25,
                'cuisine_type_id' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'recipe_id' => 26,
                'cuisine_type_id' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'recipe_id' => 27,
                'cuisine_type_id' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'recipe_id' => 28,
                'cuisine_type_id' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'recipe_id' => 28,
                'cuisine_type_id' => 10,
            ),
            30 => 
            array (
                'id' => 31,
                'recipe_id' => 29,
                'cuisine_type_id' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'recipe_id' => 30,
                'cuisine_type_id' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'recipe_id' => 30,
                'cuisine_type_id' => 2,
            ),
            33 => 
            array (
                'id' => 34,
                'recipe_id' => 31,
                'cuisine_type_id' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'recipe_id' => 32,
                'cuisine_type_id' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'recipe_id' => 33,
                'cuisine_type_id' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'recipe_id' => 34,
                'cuisine_type_id' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'recipe_id' => 35,
                'cuisine_type_id' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'recipe_id' => 36,
                'cuisine_type_id' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'recipe_id' => 37,
                'cuisine_type_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'recipe_id' => 38,
                'cuisine_type_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'recipe_id' => 39,
                'cuisine_type_id' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'recipe_id' => 40,
                'cuisine_type_id' => 11,
            ),
            43 => 
            array (
                'id' => 44,
                'recipe_id' => 41,
                'cuisine_type_id' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'recipe_id' => 42,
                'cuisine_type_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'recipe_id' => 43,
                'cuisine_type_id' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'recipe_id' => 44,
                'cuisine_type_id' => 1,
            ),
            47 => 
            array (
                'id' => 48,
                'recipe_id' => 45,
                'cuisine_type_id' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'recipe_id' => 46,
                'cuisine_type_id' => 1,
            ),
            49 => 
            array (
                'id' => 50,
                'recipe_id' => 47,
                'cuisine_type_id' => 1,
            ),
            50 => 
            array (
                'id' => 51,
                'recipe_id' => 48,
                'cuisine_type_id' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'recipe_id' => 49,
                'cuisine_type_id' => 1,
            ),
            52 => 
            array (
                'id' => 53,
                'recipe_id' => 50,
                'cuisine_type_id' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'recipe_id' => 51,
                'cuisine_type_id' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'recipe_id' => 52,
                'cuisine_type_id' => 11,
            ),
            55 => 
            array (
                'id' => 56,
                'recipe_id' => 53,
                'cuisine_type_id' => 1,
            ),
            56 => 
            array (
                'id' => 57,
                'recipe_id' => 54,
                'cuisine_type_id' => 1,
            ),
            57 => 
            array (
                'id' => 58,
                'recipe_id' => 56,
                'cuisine_type_id' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'recipe_id' => 56,
                'cuisine_type_id' => 10,
            ),
            59 => 
            array (
                'id' => 60,
                'recipe_id' => 58,
                'cuisine_type_id' => 1,
            ),
            60 => 
            array (
                'id' => 61,
                'recipe_id' => 59,
                'cuisine_type_id' => 1,
            ),
            61 => 
            array (
                'id' => 62,
                'recipe_id' => 60,
                'cuisine_type_id' => 1,
            ),
            62 => 
            array (
                'id' => 63,
                'recipe_id' => 61,
                'cuisine_type_id' => 1,
            ),
            63 => 
            array (
                'id' => 64,
                'recipe_id' => 62,
                'cuisine_type_id' => 10,
            ),
            64 => 
            array (
                'id' => 65,
                'recipe_id' => 62,
                'cuisine_type_id' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'recipe_id' => 63,
                'cuisine_type_id' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'recipe_id' => 65,
                'cuisine_type_id' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'recipe_id' => 66,
                'cuisine_type_id' => 1,
            ),
            68 => 
            array (
                'id' => 69,
                'recipe_id' => 67,
                'cuisine_type_id' => 1,
            ),
            69 => 
            array (
                'id' => 70,
                'recipe_id' => 68,
                'cuisine_type_id' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'recipe_id' => 68,
                'cuisine_type_id' => 10,
            ),
            71 => 
            array (
                'id' => 72,
                'recipe_id' => 69,
                'cuisine_type_id' => 11,
            ),
            72 => 
            array (
                'id' => 73,
                'recipe_id' => 71,
                'cuisine_type_id' => 10,
            ),
            73 => 
            array (
                'id' => 74,
                'recipe_id' => 72,
                'cuisine_type_id' => 1,
            ),
            74 => 
            array (
                'id' => 75,
                'recipe_id' => 73,
                'cuisine_type_id' => 1,
            ),
            75 => 
            array (
                'id' => 76,
                'recipe_id' => 74,
                'cuisine_type_id' => 1,
            ),
            76 => 
            array (
                'id' => 77,
                'recipe_id' => 75,
                'cuisine_type_id' => 1,
            ),
            77 => 
            array (
                'id' => 78,
                'recipe_id' => 76,
                'cuisine_type_id' => 1,
            ),
            78 => 
            array (
                'id' => 79,
                'recipe_id' => 77,
                'cuisine_type_id' => 1,
            ),
            79 => 
            array (
                'id' => 80,
                'recipe_id' => 78,
                'cuisine_type_id' => 1,
            ),
            80 => 
            array (
                'id' => 81,
                'recipe_id' => 79,
                'cuisine_type_id' => 1,
            ),
            81 => 
            array (
                'id' => 82,
                'recipe_id' => 80,
                'cuisine_type_id' => 1,
            ),
            82 => 
            array (
                'id' => 83,
                'recipe_id' => 81,
                'cuisine_type_id' => 1,
            ),
            83 => 
            array (
                'id' => 84,
                'recipe_id' => 82,
                'cuisine_type_id' => 1,
            ),
            84 => 
            array (
                'id' => 85,
                'recipe_id' => 83,
                'cuisine_type_id' => 1,
            ),
            85 => 
            array (
                'id' => 86,
                'recipe_id' => 84,
                'cuisine_type_id' => 1,
            ),
            86 => 
            array (
                'id' => 87,
                'recipe_id' => 85,
                'cuisine_type_id' => 1,
            ),
            87 => 
            array (
                'id' => 88,
                'recipe_id' => 86,
                'cuisine_type_id' => 1,
            ),
            88 => 
            array (
                'id' => 89,
                'recipe_id' => 87,
                'cuisine_type_id' => 1,
            ),
            89 => 
            array (
                'id' => 90,
                'recipe_id' => 88,
                'cuisine_type_id' => 1,
            ),
            90 => 
            array (
                'id' => 91,
                'recipe_id' => 89,
                'cuisine_type_id' => 1,
            ),
            91 => 
            array (
                'id' => 92,
                'recipe_id' => 90,
                'cuisine_type_id' => 1,
            ),
            92 => 
            array (
                'id' => 93,
                'recipe_id' => 91,
                'cuisine_type_id' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'recipe_id' => 92,
                'cuisine_type_id' => 1,
            ),
            94 => 
            array (
                'id' => 95,
                'recipe_id' => 93,
                'cuisine_type_id' => 1,
            ),
            95 => 
            array (
                'id' => 96,
                'recipe_id' => 94,
                'cuisine_type_id' => 1,
            ),
            96 => 
            array (
                'id' => 97,
                'recipe_id' => 94,
                'cuisine_type_id' => 2,
            ),
            97 => 
            array (
                'id' => 98,
                'recipe_id' => 95,
                'cuisine_type_id' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'recipe_id' => 96,
                'cuisine_type_id' => 6,
            ),
            99 => 
            array (
                'id' => 100,
                'recipe_id' => 97,
                'cuisine_type_id' => 1,
            ),
            100 => 
            array (
                'id' => 101,
                'recipe_id' => 98,
                'cuisine_type_id' => 1,
            ),
            101 => 
            array (
                'id' => 102,
                'recipe_id' => 99,
                'cuisine_type_id' => 1,
            ),
            102 => 
            array (
                'id' => 103,
                'recipe_id' => 100,
                'cuisine_type_id' => 1,
            ),
            103 => 
            array (
                'id' => 104,
                'recipe_id' => 101,
                'cuisine_type_id' => 1,
            ),
            104 => 
            array (
                'id' => 105,
                'recipe_id' => 102,
                'cuisine_type_id' => 1,
            ),
            105 => 
            array (
                'id' => 106,
                'recipe_id' => 103,
                'cuisine_type_id' => 1,
            ),
            106 => 
            array (
                'id' => 107,
                'recipe_id' => 104,
                'cuisine_type_id' => 1,
            ),
            107 => 
            array (
                'id' => 108,
                'recipe_id' => 105,
                'cuisine_type_id' => 1,
            ),
            108 => 
            array (
                'id' => 109,
                'recipe_id' => 106,
                'cuisine_type_id' => 1,
            ),
            109 => 
            array (
                'id' => 110,
                'recipe_id' => 107,
                'cuisine_type_id' => 1,
            ),
            110 => 
            array (
                'id' => 111,
                'recipe_id' => 108,
                'cuisine_type_id' => 1,
            ),
            111 => 
            array (
                'id' => 112,
                'recipe_id' => 109,
                'cuisine_type_id' => 1,
            ),
            112 => 
            array (
                'id' => 113,
                'recipe_id' => 110,
                'cuisine_type_id' => 1,
            ),
            113 => 
            array (
                'id' => 114,
                'recipe_id' => 111,
                'cuisine_type_id' => 1,
            ),
            114 => 
            array (
                'id' => 115,
                'recipe_id' => 112,
                'cuisine_type_id' => 1,
            ),
            115 => 
            array (
                'id' => 116,
                'recipe_id' => 113,
                'cuisine_type_id' => 1,
            ),
            116 => 
            array (
                'id' => 117,
                'recipe_id' => 114,
                'cuisine_type_id' => 1,
            ),
            117 => 
            array (
                'id' => 118,
                'recipe_id' => 115,
                'cuisine_type_id' => 1,
            ),
            118 => 
            array (
                'id' => 119,
                'recipe_id' => 116,
                'cuisine_type_id' => 1,
            ),
            119 => 
            array (
                'id' => 120,
                'recipe_id' => 117,
                'cuisine_type_id' => 1,
            ),
            120 => 
            array (
                'id' => 121,
                'recipe_id' => 118,
                'cuisine_type_id' => 1,
            ),
            121 => 
            array (
                'id' => 122,
                'recipe_id' => 119,
                'cuisine_type_id' => 1,
            ),
            122 => 
            array (
                'id' => 123,
                'recipe_id' => 120,
                'cuisine_type_id' => 1,
            ),
            123 => 
            array (
                'id' => 124,
                'recipe_id' => 121,
                'cuisine_type_id' => 6,
            ),
            124 => 
            array (
                'id' => 125,
                'recipe_id' => 122,
                'cuisine_type_id' => 1,
            ),
            125 => 
            array (
                'id' => 126,
                'recipe_id' => 123,
                'cuisine_type_id' => 1,
            ),
            126 => 
            array (
                'id' => 127,
                'recipe_id' => 124,
                'cuisine_type_id' => 1,
            ),
            127 => 
            array (
                'id' => 128,
                'recipe_id' => 125,
                'cuisine_type_id' => 1,
            ),
            128 => 
            array (
                'id' => 129,
                'recipe_id' => 125,
                'cuisine_type_id' => 6,
            ),
            129 => 
            array (
                'id' => 130,
                'recipe_id' => 126,
                'cuisine_type_id' => 1,
            ),
            130 => 
            array (
                'id' => 131,
                'recipe_id' => 127,
                'cuisine_type_id' => 1,
            ),
            131 => 
            array (
                'id' => 132,
                'recipe_id' => 128,
                'cuisine_type_id' => 1,
            ),
            132 => 
            array (
                'id' => 133,
                'recipe_id' => 128,
                'cuisine_type_id' => 6,
            ),
            133 => 
            array (
                'id' => 134,
                'recipe_id' => 129,
                'cuisine_type_id' => 1,
            ),
            134 => 
            array (
                'id' => 135,
                'recipe_id' => 129,
                'cuisine_type_id' => 8,
            ),
            135 => 
            array (
                'id' => 136,
                'recipe_id' => 130,
                'cuisine_type_id' => 1,
            ),
            136 => 
            array (
                'id' => 137,
                'recipe_id' => 131,
                'cuisine_type_id' => 1,
            ),
            137 => 
            array (
                'id' => 138,
                'recipe_id' => 133,
                'cuisine_type_id' => 1,
            ),
            138 => 
            array (
                'id' => 139,
                'recipe_id' => 134,
                'cuisine_type_id' => 1,
            ),
            139 => 
            array (
                'id' => 140,
                'recipe_id' => 135,
                'cuisine_type_id' => 10,
            ),
            140 => 
            array (
                'id' => 141,
                'recipe_id' => 136,
                'cuisine_type_id' => 1,
            ),
            141 => 
            array (
                'id' => 142,
                'recipe_id' => 136,
                'cuisine_type_id' => 10,
            ),
            142 => 
            array (
                'id' => 143,
                'recipe_id' => 137,
                'cuisine_type_id' => 10,
            ),
            143 => 
            array (
                'id' => 144,
                'recipe_id' => 137,
                'cuisine_type_id' => 1,
            ),
            144 => 
            array (
                'id' => 145,
                'recipe_id' => 138,
                'cuisine_type_id' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'recipe_id' => 139,
                'cuisine_type_id' => 1,
            ),
            146 => 
            array (
                'id' => 147,
                'recipe_id' => 141,
                'cuisine_type_id' => 1,
            ),
            147 => 
            array (
                'id' => 148,
                'recipe_id' => 142,
                'cuisine_type_id' => 1,
            ),
            148 => 
            array (
                'id' => 149,
                'recipe_id' => 143,
                'cuisine_type_id' => 1,
            ),
            149 => 
            array (
                'id' => 150,
                'recipe_id' => 144,
                'cuisine_type_id' => 1,
            ),
            150 => 
            array (
                'id' => 151,
                'recipe_id' => 145,
                'cuisine_type_id' => 1,
            ),
            151 => 
            array (
                'id' => 152,
                'recipe_id' => 146,
                'cuisine_type_id' => 1,
            ),
            152 => 
            array (
                'id' => 153,
                'recipe_id' => 147,
                'cuisine_type_id' => 1,
            ),
            153 => 
            array (
                'id' => 154,
                'recipe_id' => 148,
                'cuisine_type_id' => 7,
            ),
            154 => 
            array (
                'id' => 155,
                'recipe_id' => 149,
                'cuisine_type_id' => 1,
            ),
            155 => 
            array (
                'id' => 156,
                'recipe_id' => 150,
                'cuisine_type_id' => 1,
            ),
            156 => 
            array (
                'id' => 157,
                'recipe_id' => 151,
                'cuisine_type_id' => 1,
            ),
            157 => 
            array (
                'id' => 158,
                'recipe_id' => 152,
                'cuisine_type_id' => 1,
            ),
            158 => 
            array (
                'id' => 159,
                'recipe_id' => 153,
                'cuisine_type_id' => 1,
            ),
            159 => 
            array (
                'id' => 160,
                'recipe_id' => 154,
                'cuisine_type_id' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'recipe_id' => 155,
                'cuisine_type_id' => 10,
            ),
            161 => 
            array (
                'id' => 162,
                'recipe_id' => 156,
                'cuisine_type_id' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'recipe_id' => 157,
                'cuisine_type_id' => 6,
            ),
            163 => 
            array (
                'id' => 164,
                'recipe_id' => 157,
                'cuisine_type_id' => 1,
            ),
            164 => 
            array (
                'id' => 165,
                'recipe_id' => 158,
                'cuisine_type_id' => 1,
            ),
            165 => 
            array (
                'id' => 166,
                'recipe_id' => 159,
                'cuisine_type_id' => 1,
            ),
            166 => 
            array (
                'id' => 167,
                'recipe_id' => 160,
                'cuisine_type_id' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'recipe_id' => 161,
                'cuisine_type_id' => 1,
            ),
            168 => 
            array (
                'id' => 169,
                'recipe_id' => 163,
                'cuisine_type_id' => 1,
            ),
            169 => 
            array (
                'id' => 170,
                'recipe_id' => 163,
                'cuisine_type_id' => 10,
            ),
            170 => 
            array (
                'id' => 171,
                'recipe_id' => 164,
                'cuisine_type_id' => 1,
            ),
            171 => 
            array (
                'id' => 172,
                'recipe_id' => 165,
                'cuisine_type_id' => 1,
            ),
            172 => 
            array (
                'id' => 173,
                'recipe_id' => 166,
                'cuisine_type_id' => 1,
            ),
            173 => 
            array (
                'id' => 174,
                'recipe_id' => 167,
                'cuisine_type_id' => 1,
            ),
            174 => 
            array (
                'id' => 175,
                'recipe_id' => 168,
                'cuisine_type_id' => 1,
            ),
            175 => 
            array (
                'id' => 176,
                'recipe_id' => 169,
                'cuisine_type_id' => 1,
            ),
            176 => 
            array (
                'id' => 177,
                'recipe_id' => 170,
                'cuisine_type_id' => 1,
            ),
            177 => 
            array (
                'id' => 178,
                'recipe_id' => 171,
                'cuisine_type_id' => 1,
            ),
            178 => 
            array (
                'id' => 179,
                'recipe_id' => 172,
                'cuisine_type_id' => 1,
            ),
            179 => 
            array (
                'id' => 180,
                'recipe_id' => 173,
                'cuisine_type_id' => 1,
            ),
            180 => 
            array (
                'id' => 181,
                'recipe_id' => 174,
                'cuisine_type_id' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'recipe_id' => 176,
                'cuisine_type_id' => 2,
            ),
            182 => 
            array (
                'id' => 183,
                'recipe_id' => 176,
                'cuisine_type_id' => 1,
            ),
            183 => 
            array (
                'id' => 184,
                'recipe_id' => 177,
                'cuisine_type_id' => 1,
            ),
            184 => 
            array (
                'id' => 185,
                'recipe_id' => 178,
                'cuisine_type_id' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'recipe_id' => 179,
                'cuisine_type_id' => 1,
            ),
            186 => 
            array (
                'id' => 187,
                'recipe_id' => 180,
                'cuisine_type_id' => 1,
            ),
            187 => 
            array (
                'id' => 188,
                'recipe_id' => 180,
                'cuisine_type_id' => 6,
            ),
            188 => 
            array (
                'id' => 189,
                'recipe_id' => 181,
                'cuisine_type_id' => 1,
            ),
            189 => 
            array (
                'id' => 190,
                'recipe_id' => 182,
                'cuisine_type_id' => 1,
            ),
            190 => 
            array (
                'id' => 191,
                'recipe_id' => 182,
                'cuisine_type_id' => 2,
            ),
            191 => 
            array (
                'id' => 192,
                'recipe_id' => 183,
                'cuisine_type_id' => 1,
            ),
            192 => 
            array (
                'id' => 193,
                'recipe_id' => 184,
                'cuisine_type_id' => 1,
            ),
            193 => 
            array (
                'id' => 194,
                'recipe_id' => 184,
                'cuisine_type_id' => 2,
            ),
            194 => 
            array (
                'id' => 195,
                'recipe_id' => 185,
                'cuisine_type_id' => 7,
            ),
            195 => 
            array (
                'id' => 196,
                'recipe_id' => 186,
                'cuisine_type_id' => 1,
            ),
            196 => 
            array (
                'id' => 197,
                'recipe_id' => 187,
                'cuisine_type_id' => 1,
            ),
            197 => 
            array (
                'id' => 198,
                'recipe_id' => 188,
                'cuisine_type_id' => 1,
            ),
            198 => 
            array (
                'id' => 199,
                'recipe_id' => 189,
                'cuisine_type_id' => 1,
            ),
            199 => 
            array (
                'id' => 200,
                'recipe_id' => 190,
                'cuisine_type_id' => 1,
            ),
            200 => 
            array (
                'id' => 201,
                'recipe_id' => 191,
                'cuisine_type_id' => 1,
            ),
            201 => 
            array (
                'id' => 202,
                'recipe_id' => 192,
                'cuisine_type_id' => 1,
            ),
            202 => 
            array (
                'id' => 203,
                'recipe_id' => 193,
                'cuisine_type_id' => 1,
            ),
            203 => 
            array (
                'id' => 204,
                'recipe_id' => 194,
                'cuisine_type_id' => 1,
            ),
            204 => 
            array (
                'id' => 205,
                'recipe_id' => 195,
                'cuisine_type_id' => 1,
            ),
            205 => 
            array (
                'id' => 206,
                'recipe_id' => 196,
                'cuisine_type_id' => 1,
            ),
            206 => 
            array (
                'id' => 207,
                'recipe_id' => 197,
                'cuisine_type_id' => 1,
            ),
            207 => 
            array (
                'id' => 208,
                'recipe_id' => 197,
                'cuisine_type_id' => 10,
            ),
            208 => 
            array (
                'id' => 209,
                'recipe_id' => 198,
                'cuisine_type_id' => 1,
            ),
            209 => 
            array (
                'id' => 210,
                'recipe_id' => 199,
                'cuisine_type_id' => 1,
            ),
            210 => 
            array (
                'id' => 211,
                'recipe_id' => 199,
                'cuisine_type_id' => 11,
            ),
            211 => 
            array (
                'id' => 212,
                'recipe_id' => 200,
                'cuisine_type_id' => 1,
            ),
            212 => 
            array (
                'id' => 213,
                'recipe_id' => 201,
                'cuisine_type_id' => 1,
            ),
            213 => 
            array (
                'id' => 214,
                'recipe_id' => 203,
                'cuisine_type_id' => 1,
            ),
            214 => 
            array (
                'id' => 215,
                'recipe_id' => 204,
                'cuisine_type_id' => 1,
            ),
            215 => 
            array (
                'id' => 216,
                'recipe_id' => 205,
                'cuisine_type_id' => 1,
            ),
            216 => 
            array (
                'id' => 217,
                'recipe_id' => 206,
                'cuisine_type_id' => 1,
            ),
            217 => 
            array (
                'id' => 218,
                'recipe_id' => 207,
                'cuisine_type_id' => 1,
            ),
            218 => 
            array (
                'id' => 219,
                'recipe_id' => 208,
                'cuisine_type_id' => 1,
            ),
            219 => 
            array (
                'id' => 220,
                'recipe_id' => 209,
                'cuisine_type_id' => 1,
            ),
            220 => 
            array (
                'id' => 221,
                'recipe_id' => 210,
                'cuisine_type_id' => 1,
            ),
            221 => 
            array (
                'id' => 222,
                'recipe_id' => 211,
                'cuisine_type_id' => 1,
            ),
            222 => 
            array (
                'id' => 223,
                'recipe_id' => 212,
                'cuisine_type_id' => 1,
            ),
            223 => 
            array (
                'id' => 224,
                'recipe_id' => 213,
                'cuisine_type_id' => 1,
            ),
            224 => 
            array (
                'id' => 225,
                'recipe_id' => 214,
                'cuisine_type_id' => 10,
            ),
            225 => 
            array (
                'id' => 226,
                'recipe_id' => 214,
                'cuisine_type_id' => 1,
            ),
            226 => 
            array (
                'id' => 227,
                'recipe_id' => 215,
                'cuisine_type_id' => 10,
            ),
            227 => 
            array (
                'id' => 228,
                'recipe_id' => 216,
                'cuisine_type_id' => 10,
            ),
            228 => 
            array (
                'id' => 229,
                'recipe_id' => 217,
                'cuisine_type_id' => 1,
            ),
            229 => 
            array (
                'id' => 230,
                'recipe_id' => 217,
                'cuisine_type_id' => 10,
            ),
            230 => 
            array (
                'id' => 231,
                'recipe_id' => 218,
                'cuisine_type_id' => 10,
            ),
            231 => 
            array (
                'id' => 232,
                'recipe_id' => 219,
                'cuisine_type_id' => 1,
            ),
            232 => 
            array (
                'id' => 233,
                'recipe_id' => 220,
                'cuisine_type_id' => 1,
            ),
            233 => 
            array (
                'id' => 234,
                'recipe_id' => 220,
                'cuisine_type_id' => 11,
            ),
            234 => 
            array (
                'id' => 235,
                'recipe_id' => 221,
                'cuisine_type_id' => 1,
            ),
            235 => 
            array (
                'id' => 236,
                'recipe_id' => 222,
                'cuisine_type_id' => 1,
            ),
            236 => 
            array (
                'id' => 237,
                'recipe_id' => 223,
                'cuisine_type_id' => 1,
            ),
            237 => 
            array (
                'id' => 238,
                'recipe_id' => 224,
                'cuisine_type_id' => 1,
            ),
            238 => 
            array (
                'id' => 239,
                'recipe_id' => 225,
                'cuisine_type_id' => 1,
            ),
            239 => 
            array (
                'id' => 240,
                'recipe_id' => 226,
                'cuisine_type_id' => 1,
            ),
            240 => 
            array (
                'id' => 241,
                'recipe_id' => 227,
                'cuisine_type_id' => 1,
            ),
            241 => 
            array (
                'id' => 242,
                'recipe_id' => 228,
                'cuisine_type_id' => 1,
            ),
            242 => 
            array (
                'id' => 243,
                'recipe_id' => 228,
                'cuisine_type_id' => 6,
            ),
            243 => 
            array (
                'id' => 244,
                'recipe_id' => 230,
                'cuisine_type_id' => 1,
            ),
        ));
        
        
    }
}