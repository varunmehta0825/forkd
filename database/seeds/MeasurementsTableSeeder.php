<?php

use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('measurements')->delete();
        
        \DB::table('measurements')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'cloves',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'cup',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'fl. oz.',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'g',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'lb.',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'oz.',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'patty',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'scoop',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'slices',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'tbsp.',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'tsp.',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'whole',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'leaves',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'link',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'to taste',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'pinch',
            ),
        ));
        
        
    }
}