<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OauthClientsTableSeeder::class);
        $this->call(OauthPersonalAccessClientsTableSeeder::class);
        $this->call(OauthRefreshTokensTableSeeder::class);
        $this->call(OauthAccessTokensTableSeeder::class);
        $this->call(CalorieBracketsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MeasurementsTableSeeder::class);
        // $this->call(IngredientsTableSeeder::class);
        $this->call(MealTypesTableSeeder::class);
        $this->call(DietaryLifestylesTableSeeder::class);
        $this->call(RecipeTypesTableSeeder::class);
        $this->call(CuisineTypesTableSeeder::class);
        $this->call(PreparationsTableSeeder::class);
        // $this->call(RecipesTableSeeder::class);
        // $this->call(RecipeIngredientsTableSeeder::class);
        // $this->call(RelatedRecipesTableSeeder::class);
        // $this->call(RecipeRecipeTypeTableSeeder::class);
        // $this->call(RecipeMealTypeTableSeeder::class);
        // $this->call(RecipeDietaryLifestyleTableSeeder::class);
        // $this->call(RecipeCuisineTypeTableSeeder::class);
        // $this->call(UserWeeklyPlanTableSeeder::class);
        // $this->call(UserDailyPlanTableSeeder::class);
        // $this->call(UserMealsPlanTableSeeder::class);
        $this->call(MealTemplatesTableSeeder::class);
    }
}
