<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_clients')->delete();
        
        \DB::table('oauth_clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'Simple Fork\\\'d Personal Access Client',
                'secret' => '5IsiUAXhPgYzgkNb4GX5ex06VyIxrsxcZtmCA0ND',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2019-02-03 19:43:08',
                'updated_at' => '2019-02-03 19:43:08',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => NULL,
                'name' => 'Simple Fork\\\'d Password Grant Client',
                'secret' => 'yrQ819ZJ2yriHBhs9Wz8cTZbKQCi6aHlIfSCR9dI',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => '2019-02-03 19:43:08',
                'updated_at' => '2019-02-03 19:43:08',
            ),
        ));
        
        
    }
}