<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealsPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meals_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_daily_plan_id')->unsigned();
            $table->integer('meal_type_id')->unsigned();
            $table->integer('recipe_id')->unsigned();

            $table->foreign('user_daily_plan_id')->references('id')->on('user_daily_plan')->onDelete('cascade');
            $table->foreign('meal_type_id')->references('id')->on('meal_types')->onDelete('cascade');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meals_plan');
    }
}
