<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ingredient_id');
            $table->integer('category_id')->nullable()->unsigned();
            $table->integer('measurement_id')->nullable()->unsigned();
            $table->float('base_qty')->default(0);
            $table->string('name')->default(0);
            $table->integer('calories')->nullable();
            $table->float('red_containers')->default(0);
            $table->float('green_containers')->default(0);
            $table->float('purple_containers')->default(0);
            $table->float('yellow_containers')->default(0);
            $table->float('blue_containers')->default(0);
            $table->float('orange_containers')->default(0);
            $table->float('spoon_containers')->default(0);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('measurement_id')->references('id')->on('measurements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
