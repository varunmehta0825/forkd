<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('recipe_id');
            $table->string('name');
            $table->string('attachment')->nullable();
            $table->text('preparation')->nullable();
            $table->integer('cook_time_m')->nullable();
            $table->integer('ready_time_m')->nullable();
            $table->integer('prep_time_m')->nullable();
            $table->integer('servings')->nullable();
            $table->string('secondary_ingredients_title')->nullable();
            $table->float('prep_time_h')->nullable();
            $table->float('cook_time_h')->nullable();
            $table->float('ready_time_h')->nullable();
            $table->string('display_prep_time', 40)->nullable();
            $table->string('display_cook_time', 40)->nullable();
            $table->string('display_ready_time', 40)->nullable();
            $table->boolean('proofed')->nullable();
            $table->string('count_per_serving')->nullable();
            $table->boolean('tested')->nullable();
            $table->float('red_containers')->default(0);
            $table->float('green_containers')->default(0);
            $table->float('purple_containers')->default(0);
            $table->float('yellow_containers')->default(0);
            $table->float('blue_containers')->default(0);
            $table->float('orange_containers')->default(0);
            $table->float('spoon_containers')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
