<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCalorieBrackets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calorie_brackets', function (Blueprint $table) {
            $table->integer('red');
            $table->integer('green');
            $table->integer('purple');
            $table->integer('yellow');
            $table->integer('blue');
            $table->integer('orange');
            $table->integer('grey');
            $table->integer('n_red');
            $table->integer('n_green');
            $table->integer('n_purple');
            $table->integer('n_yellow');
            $table->integer('n_blue');
            $table->integer('n_orange');
            $table->integer('n_grey');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calorie_brackets', function (Blueprint $table) {
            //
        });
    }
}
