<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDailyPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_daily_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_weekly_plan_id')->unsigned();
            $table->string('day', 15);

            $table->foreign('user_weekly_plan_id')->references('id')->on('user_weekly_plan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_daily_plan');
    }
}
