<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeDietaryLifestyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_dietary_lifestyle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')->unsigned();
            $table->integer('dietary_lifestyle_id')->unsigned();

            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('dietary_lifestyle_id')->references('id')->on('dietary_lifestyles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_dietary_lifestyle');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('dietary_lifestyles');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
