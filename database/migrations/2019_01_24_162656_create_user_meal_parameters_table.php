<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meal_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('calorie_bracket_id')->unsigned()->nullable();
            $table->boolean('nursing')->default(0);
            $table->integer('dietary_lifestyle_id')->unsigned()->nullable();
            $table->integer('meal_template_id')->unsigned()->nullable();
            $table->integer('passed_steps')->default(2);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('calorie_bracket_id')->references('id')->on('calorie_brackets')->onDelete('cascade');
            $table->foreign('dietary_lifestyle_id')->references('id')->on('dietary_lifestyles')->onDelete('cascade');
            $table->foreign('meal_template_id')->references('id')->on('meal_templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meal_parameters');
    }
}
