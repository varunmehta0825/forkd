<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMealCustomizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meal_customization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->boolean('cheat_day')->defaul(0);
            $table->boolean('eating_out')->defaul(0);
            $table->boolean('travel')->defaul(0);
            $table->boolean('shake')->defaul(0);
            $table->boolean('alcohol_intake')->defaul(0);
            $table->boolean('ingredients')->defaul(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_meal_customization');
    }
}
