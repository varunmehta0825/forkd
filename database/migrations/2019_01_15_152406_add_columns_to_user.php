<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('age')->after('password')->nullable();
            $table->string('gender', 10)->after('age')->nullable();
            $table->string('avatar', 255)->after('gender')->default('avatar.png');
            $table->boolean('active')->after('avatar')->default(true);
            $table->string('measurement')->after('active')->default('US');
            $table->timestamp('last_active')->default(DB::raw('CURRENT_TIMESTAMP'))->after('remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('user', function (Blueprint $table) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            Schema::drop('calorie_brackets');
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        // });
    }
}
