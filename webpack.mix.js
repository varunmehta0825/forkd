const mix = require('laravel-mix');
// require('laravel-mix-svg-sprite');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .webpackConfig({
       plugins: [
         new SVGSpritemapPlugin(
           'resources/icons/*.svg',{
               output: {
                  filename : 'svg/icons.svg',
               },
               sprite: {
                  prefix: false,
                  generate: {
                      title: false
                  }
               }
           })
        ]
    });
