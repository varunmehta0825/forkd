<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Auth
Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::get('user/confirm/{token}', 'Api\AuthController@verifyUser');
Route::post('password/create', 'Api\AuthController@create');
Route::post('password/reset', 'Api\AuthController@reset');

Route::middleware('auth:api')->group(function () {
    // User account
    Route::get('user/{id?}',                      'Api\UserController@show');

    // Subscribe
    Route::post('subscribe',                      'Api\CheckoutController@subscribe');

    Route::post('resend-confirmation',            'Api\AuthController@resendConfirmation');

    Route::middleware(['is_confirmed', 'last_active'])->group(function () {
        Route::namespace('Api')->group(function () {
            // Auth
            Route::post('logout',                           'AuthController@logout');

            // Create meal plan
            Route::get('calorie-brackets',                  'GetAttributesController@calories');
            Route::post('step/2',                           'StoreAttributesController@step2');
            Route::post('step/3',                           'StoreAttributesController@step3');
            Route::get('search-ingredients',                'StoreAttributesController@searchIngredients');
            Route::post('step/4',                           'StoreAttributesController@step4');
            Route::post('step/5',                           'StoreAttributesController@step5');
            Route::get('excluded-recipes',                  'StoreAttributesController@excludedRecipes');
            Route::post('plan/create',                      'MealPlanController@createPlan');
            Route::post('meal-plan/store/{week?}',          'MealPlanController@store');
            Route::get('meal-plan/show/{week?}',            'MealPlanController@show');

            // Recipe
            Route::get('recipe/{recipe_id}',                             'RecipeController@show');
            Route::get('swap/{week_id}/{meal_id}/{recipe_id}',           'RecipeController@swap');

            // Customize meal plan
            Route::post('select-steps',                     'MealCustomization@selectCustomizationSteps');
            Route::get('get-steps',                         'MealCustomization@getCustomizationSteps');
            Route::post('customize/shake',                  'MealCustomization@customizeShake');
            Route::get('get-previous-settings',             'MealCustomization@getPreviousSettings');

            // Create PDF
            Route::get('pdf/{week?}',                       'RecipeController@createPDF');

            // Shopping list
            Route::get('shopping-list',                     'ShoppingListController@show');

            // Calorie Calculator
            Route::post('calorie-calculator',                'CalorieCalculatorController@index');

            // My account
            Route::post('change-password',                   'AuthController@changePassword');
            Route::post('change-avatar',                     'AuthController@changeAvatar');
            Route::get('nutritions-profile',                 'NutritionProfileController@show');
            Route::post('nutritions-profile',                'NutritionProfileController@update');

        });
    });
});

// TEMPORARY API'S. IMPORT DATA FROM AIRTABLE
Route::get('/ingredients', 'Api\AirtableController@getIngredients');
Route::get('/recipes', 'Api\AirtableController@getRecipes');
Route::get('/recipe-ingredients', 'Api\AirtableController@getRecipeIngredients');
Route::get('/all-recipes', 'Api\AirtableController@getAllRecipes');
